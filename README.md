# Neverwinter Nights Dark Sun [NWNDS] Issues & Information

This repo deals with project tracking, bugfixing & information for the NWN Dark Sun persistent world.

## Features

DMFI + ACP v41 + HCR2 + CEP 2.69 + PRC8

[20 Base Classes & 52 Prestige Classes](https://gitea.raptio.us/Jaysyn/NWNDS/wiki/NWN-Dark-Sun-Availible-Class-List)

15 new Athasian 3.5e classes from the Official Dark Sun Campaign Setting, with more to come.

[25 Athasian Player races including 6 dynamic kreen models](https://gitea.raptio.us/Jaysyn/NWNDS/wiki/NWN-Dark-Sun-Available-Race-List#base-races)

[10 Athasian Prestige races.  Be the Belgoi!](https://gitea.raptio.us/Jaysyn/NWNDS/wiki/NWN-Dark-Sun-Availible-Class-List#prestige-classes)

[15 Athasian animal companions, with more planned.](https://gitea.raptio.us/Jaysyn/NWNDS/wiki/NWN-Dark-Sun-Available-Animal-Companions)

60+ new 3.5e spells from the Dark Sun Campaign Setting, wih more to come.

Dark Sun Mechanics: Defiling Razes, Favored Terrain, Gladitorial Performaces, Arcane Terrain Modifiers and more.

Custom Mount system.  Mounted combat from an erdlu?  Yes you can!

70+ Athasian poisons integrated into the the PRC's poisoncrafting system.

Custom Overworld Map System with over 240k square kilometers of Athasian terrain to explore.

EL TAFOD Randomized Encounter System - Encounter Level, Terrain, Activity cycle, Frequency, Organization and Difficulty is considered for each random encounter generated.

### Contributions & Thanks

Athas.org

Acidchalk, Azimn, Draygoth, Tjured, Tonden - Models & tilesets

Dizzit_Gardan - Quests, scripting, tilesets & models.

Ebonfowl - Scripting

Draceran & Dromin - Wordsmithing

Cervantes - Athas Desert Exterior Tileset & portraits

Merricksdad - Granitelands 2, Red Desert & Deep Caverns Tilesets

LordofWorms - Jungle Mountain Tileset

Pasilli (R.I.P) & CTP - CTP Babylon Tileset

Mark Ip - Aztec Exterior Redux Tilset

ILmaTeR / ReAver - CODI/Daggerdale Swamp Tileset 

Lord Rosenkantz - Hut Interiors v1.3 

Zunath - Buildable Persistent Player Housing System

HCR2 - Hunger, Thirst & Fatigue

69MEH69 - Module Builders Henchman Kit

Quevy - Trap Strength Icons

Lucky Day & Ana - Tumbleweed Creature

### Music
Francis Bonin - "Desert Sun Monk"


### Athas Reborn / Dark Sun Online Contributors

#### The PRC8 Team

#### The CEP2 Team

#### Athas Reborn & Dark Sun Online contributors:
Acidchalk, Andrak the Mad, Antoneagle, Aserath, Dante, Danule, Deek, Dom, Dreaderick, Dred, Drohgan, Elanthis Seldavius, Hellfire, Ihsaan, Jaysyn, Josten, Kenquinn, Klonapin, Kyriana Agrivar, Lorack, Magnus, Master Pain, Natana, NiteCap, Noob Punchingbag, Ranulf Majere, Rashod81100, Shadara, Sirna, Squirrel, Sutered, The Deadheart, The Inquisitor, The Obsidian Oracle, Tik, Trump, Wayne & Wrains