

//:: Check & fix Belgoi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_BELGOI  && CheckBelgoiSkin(oPC) != TRUE)
	{
		RndBelgoiSkin(oPC);
	}
	
//:: Check & fix Belgoi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_BELGOI  && CheckBelgoiHair(oPC) != TRUE)
	{
		RndBelgoiHair(oPC);
	}
	
//:: Checks for valid Belgoi skin color channel
	int CheckBelgoiSkin(object oPC)
	{
		//:: Get the skin color channel
		int nSkinColor 	= GetColorChannel(oPC, COLOR_CHANNEL_SKIN);
		
		//:: Check if the skin color channel is within any of the specified ranges
		return (nSkinColor >= 21 && nSkinColor <= 22) ||
			   (nSkinColor >= 48 && nSkinColor <= 50) ||
			   (nSkinColor >= 78 && nSkinColor <= 79) ||
			   (nSkinColor >= 82 && nSkinColor <= 83) ||
			   (nSkinColor >= 136 && nSkinColor <= 139) ||
			   (nSkinColor == 165) ||
			   (nSkinColor == 169);
	}
	
//:: Checks for valid Belgoi hair color channel
	int CheckBelgoiHair(object oPC)
	{
		//:: Get the hair color channel
		int nHairColor 	= GetColorChannel(oPC, COLOR_CHANNEL_HAIR);
		
		//:: Check if the hair color channel is within any of the specified ranges
		return (nHairColor >= 16 && nHairColor <= 35) ||
			   (nHairColor == 44) ||
			   (nHairColor == 46) ||
			   (nHairColor == 54) ||
			   (nHairColor == 56) ||
			   (nHairColor == 57) ||
			   (nHairColor == 70) ||
			   (nHairColor >= 77 && nHairColor <= 79) ||
			   (nHairColor >= 82 && nHairColor <= 84) ||
			   (nHairColor >= 128 && nHairColor <= 150) ||
			   (nHairColor >= 163 && nHairColor <= 171);
	}
	
//:: Function to randomly set oPC's skin color channel for Belgoi
	void RndBelgoiSkin(object oPC)
	{
		// Get a random number
		int randomNum = Random(14);

		// Map the random number to a skin color value
		int randomSkinColor;

			 if (randomNum >= 0 && randomNum <= 1) randomSkinColor = Random(2) + 21; // Range 21-22
		else if (randomNum >= 2 && randomNum <= 4) randomSkinColor = Random(3) + 48; // Range 48-50
		else if (randomNum >= 5 && randomNum <= 6) randomSkinColor = Random(2) + 78; // Range 78-79
		else if (randomNum >= 7 && randomNum <= 8) randomSkinColor = Random(2) + 82; // Range 82-83
		else if (randomNum >= 9 && randomNum <= 12) randomSkinColor = Random(4) + 136; // Range 136-139
		else if (randomNum == 13) randomSkinColor = 165; // Single value
		else if (randomNum == 14) randomSkinColor = 169; // Single value
		else randomSkinColor = 0; // Default case

		// Set the random skin color value for oPC
		SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
	}
	
//:: Function to randomly set oPC's hair color channel for Belgoi
	void RndBelgoiHair(object oPC)
	{
		// Get a random number
		int randomNum = Random(9);

		// Map the random number to a hair color value
		int randomHairColor;

			 if (randomNum == 0) randomHairColor = Random(2) + 22; // Range 22-23
		else if (randomNum == 1) randomHairColor = 27; // Single value
		else if (randomNum == 2) randomHairColor = 31; // Single value
		else if (randomNum == 3) randomHairColor = 35; // Single value
		else if (randomNum == 4) randomHairColor = 63; // Single value
		else if (randomNum == 5) randomHairColor = 115; // Single value
		else if (randomNum == 6) randomHairColor = 135; // Single value
		else if (randomNum == 7) randomHairColor = 167; // Single value

		// Set the random hair color value for oPC
		SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
	}
	
	
	
	/*
	*	Allowed color channel ranges
	*
	*	Skin
	*	21-22
	*	48-50
	*	78-79
	*	82-83
	*	136-139
	*	165
	*	169
	*
	*	Hair
	*	22-23
	*	27
	*	31
	*	35
	*	63
	*	115
	*	135
	*	167
	*/
	
21-22
48-50
78-79
82-83
136-139
165
169

22-23
27
31
35
63
115
135
167