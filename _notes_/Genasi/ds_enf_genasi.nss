//:: ds_enf_genasi.nss
/*	
*	Checks that PC Genasis are using an appropriate
*	skin & hair color for their racialtype & set
*	them to an appropriate color if not.
*
*/

#include "prc_inc_racial"

int CheckAirGenasiSkin(object oPC);
int CheckAirGenasiHair(object oPC);
void RndAirGenasiSkin(object oPC);
void RndAirGenasiHair(object oPC);
int CheckEarthGenasiSkin(object oPC);
int CheckEarthGenasiHair(object oPC);
void RndEarthGenasiSkin(object oPC);
void RndEarthGenasiHair(object oPC);
int CheckFireGenasiSkin(object oPC);
int CheckFireGenasiHair(object oPC);
void RndFireGenasiSkin(object oPC);
void RndFireGenasiHair(object oPC);
int CheckWaterGenasiSkin(object oPC);
int CheckWaterGenasiHair(object oPC);
void RndWaterGenasiSkin(object oPC);
void RndWaterGenasiHair(object oPC);
int CheckMagmaGenasiSkin(object oPC);
int CheckMagmaGenasiHair(object oPC);
void RndMagmaGenasiSkin(object oPC);
void RndMagmaGenasiHair(object oPC);
int CheckRainGenasiSkin(object oPC);
int CheckRainGenasiHair(object oPC);
void RndRainGenasiSkin(object oPC);
void RndRainGenasiHair(object oPC);
int CheckSiltGenasiSkin(object oPC);
int CheckSiltGenasiHair(object oPC);
void RndSiltGenasiSkin(object oPC);
void RndSiltGenasiHair(object oPC);
int CheckSunGenasiSkin(object oPC);
int CheckSunGenasiHair(object oPC);
void RndSunGenasiSkin(object oPC);
void RndSunGenasiHair(object oPC);

void main ()
{
	object oPC = OBJECT_SELF;
	
//:: Check & fix Air Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_AIR_GEN  && CheckAirGenasiSkin(oPC) != TRUE)
	{
		RndAirGenasiSkin(oPC);
	}
	
//:: Check & fix Air Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_AIR_GEN  && CheckAirGenasiHair(oPC) != TRUE)
	{
		RndAirGenasiHair(oPC);
	}

//:: Check & fix Earth Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_EARTH_GEN  && CheckEarthGenasiSkin(oPC) != TRUE)
	{
		RndEarthGenasiSkin(oPC);
	}
	
//:: Check & fix Earth Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_EARTH_GEN  && CheckEarthGenasiHair(oPC) != TRUE)
	{
		RndEarthGenasiHair(oPC);
	}
	
//:: Check & fix Fire Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_FIRE_GEN  && CheckFireGenasiSkin(oPC) != TRUE)
	{
		RndFireGenasiSkin(oPC);
	}
	
//:: Check & fix Fire Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_FIRE_GEN  && CheckFireGenasiHair(oPC) != TRUE)
	{
		RndFireGenasiHair(oPC);
	}	
	
//:: Check & fix Water Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_WATER_GEN  && CheckWaterGenasiSkin(oPC) != TRUE)
	{
		RndWaterGenasiSkin(oPC);
	}
	
//:: Check & fix Water Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_WATER_GEN  && CheckWaterGenasiHair(oPC) != TRUE)
	{
		RndWaterGenasiHair(oPC);
	}		
	
//:: Check & fix Magma Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_MAGMA_GEN  && CheckMagmaGenasiSkin(oPC) != TRUE)
	{
		RndMagmaGenasiSkin(oPC);
	}
	
//:: Check & fix Magma Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_MAGMA_GEN  && CheckMagmaGenasiHair(oPC) != TRUE)
	{
		RndMagmaGenasiHair(oPC);
	}		
		
//:: Check & fix Rain Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_RAIN_GEN  && CheckRainGenasiSkin(oPC) != TRUE)
	{
		RndRainGenasiSkin(oPC);
	}
	
//:: Check & fix Rain Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_RAIN_GEN  && CheckRainGenasiHair(oPC) != TRUE)
	{
		RndRainGenasiHair(oPC);
	}	

//:: Check & fix Silt Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SILT_GEN  && CheckSiltGenasiSkin(oPC) != TRUE)
	{
		RndSiltGenasiSkin(oPC);
	}
	
//:: Check & fix Silt Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SILT_GEN  && CheckSiltGenasiHair(oPC) != TRUE)
	{
		RndSiltGenasiHair(oPC);
	}

//:: Check & fix Sun Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SUN_GEN  && CheckSunGenasiSkin(oPC) != TRUE)
	{
		RndSunGenasiSkin(oPC);
	}
	
//:: Check & fix Sun Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SUN_GEN  && CheckSunGenasiHair(oPC) != TRUE)
	{
		RndSunGenasiHair(oPC);
	}

}

//:: Checks for valid Air Genasi skin color channel
int CheckAirGenasiSkin(object oPC)
{
	//:: Get the skin color channel
    int nSkinColor 	= GetColor(oPC, COLOR_CHANNEL_SKIN);
	
	//:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 20 && nSkinColor <= 23) ||
           (nSkinColor == 40) ||
           (nSkinColor >= 50 && nSkinColor <= 51) ||
           (nSkinColor >= 132 && nSkinColor <= 133) ||
           (nSkinColor >= 136 && nSkinColor <= 137) ||
           (nSkinColor >= 140 && nSkinColor <= 141) ||
           (nSkinColor >= 148 && nSkinColor <= 149) ||
           (nSkinColor == 164) ||
           (nSkinColor == 166);
}

//:: Checks for valid Air Genasi hair color channel
int CheckAirGenasiHair(object oPC)
{
	//:: Get the hair color channel
    int nHairColor 	= GetColor(oPC, COLOR_CHANNEL_HAIR);
	
	//:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 35) ||
           (nHairColor == 44) ||
           (nHairColor == 46) ||
           (nHairColor == 54) ||
           (nHairColor == 56) ||
           (nHairColor == 57) ||
           (nHairColor == 70) ||
           (nHairColor >= 77 && nHairColor <= 79) ||
           (nHairColor >= 82 && nHairColor <= 84) ||
           (nHairColor >= 128 && nHairColor <= 150) ||
           (nHairColor >= 163 && nHairColor <= 171);
}

// Function to randomly set oPC's skin color channel for Air Genasi
void RndAirGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(9);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum == 0) randomSkinColor = 20 + Random(4); // Range 20-23
    else if (randomNum == 1) randomSkinColor = 40;
    else if (randomNum == 2) randomSkinColor = 50 + Random(2); // Range 50-51
    else if (randomNum == 3) randomSkinColor = 132 + Random(2); // Range 132-133
    else if (randomNum == 4) randomSkinColor = 136 + Random(2); // Range 136-137
    else if (randomNum == 5) randomSkinColor = 140 + Random(2); // Range 140-141
    else if (randomNum == 6) randomSkinColor = 148 + Random(2); // Range 148-149
    else if (randomNum == 7) randomSkinColor = 164;
    else if (randomNum == 8) randomSkinColor = 166;

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Air Genasi
void RndAirGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(11);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum == 0) randomHairColor = Random(20) + 16; // Range 16-35
    else if (randomNum == 1) randomHairColor = 44; // Single value
    else if (randomNum == 2) randomHairColor = 46; // Single value
    else if (randomNum == 3) randomHairColor = 54; // Single value
    else if (randomNum == 4) randomHairColor = 56; // Single value
    else if (randomNum == 5) randomHairColor = 57; // Single value
    else if (randomNum == 6) randomHairColor = 70; // Single value
    else if (randomNum == 7) randomHairColor = Random(3) + 77; // Range 77-79
    else if (randomNum == 8) randomHairColor = Random(3) + 82; // Range 82-84
    else if (randomNum == 9) randomHairColor = Random(23) + 128; // Range 128-150
    else if (randomNum == 10) randomHairColor = Random(9) + 163; // Range 163-171

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Earth Genasi skin color channel
int CheckEarthGenasiSkin(object oPC)
{
	//:: Get the skin color channel
    int nSkinColor 	= GetColor(oPC, COLOR_CHANNEL_SKIN);
	
	//:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 3 && nSkinColor <= 7) ||
           (nSkinColor == 11) ||
           (nSkinColor >= 14 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 21 && nSkinColor <= 23) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor == 35) ||
           (nSkinColor >= 37 && nSkinColor <= 39) ||
           (nSkinColor >= 42 && nSkinColor <= 43) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor == 63) ||
           (nSkinColor >= 74 && nSkinColor <= 79) ||
           (nSkinColor >= 80 && nSkinColor <= 87) ||
           (nSkinColor >= 103 && nSkinColor <= 127) ||
           (nSkinColor >= 130 && nSkinColor <= 135) ||
           (nSkinColor >= 155 && nSkinColor <= 157) ||
           (nSkinColor >= 167 && nSkinColor <= 174);
}

//:: Checks for valid Earth Genasi hair color channel
int CheckEarthGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 0 && nHairColor <= 3) ||
           (nHairColor == 7) ||
           (nHairColor >= 13 && nHairColor <= 15) ||
           (nHairColor == 19) ||
           (nHairColor == 23) ||
           (nHairColor >= 27 && nHairColor <= 31) ||
           (nHairColor == 35) ||
           (nHairColor >= 37 && nHairColor <= 39) ||
           (nHairColor == 41) ||
           (nHairColor == 43) ||
           (nHairColor == 47) ||
           (nHairColor == 49) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor == 63) ||
           (nHairColor >= 69 && nHairColor <= 71) ||
           (nHairColor >= 74 && nHairColor <= 79) ||
           (nHairColor >= 80 && nHairColor <= 86) ||
           (nHairColor >= 96 && nHairColor <= 135) ||
           (nHairColor >= 148 && nHairColor <= 159) ||
           (nHairColor >= 165 && nHairColor <= 174);
}

// Function to randomly set oPC's skin color channel for Earth Genasi
void RndEarthGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(18);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum == 0) randomSkinColor = Random(5) + 3; // Range 3-7
    else if (randomNum == 1) randomSkinColor = 11; // Range 11
    else if (randomNum == 2) randomSkinColor = Random(2) + 14; // Range 14-15
    else if (randomNum == 3) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum == 4) randomSkinColor = Random(3) + 21; // Range 21-23
    else if (randomNum == 5) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum == 6) randomSkinColor = 35; // Range 35
    else if (randomNum == 7) randomSkinColor = Random(3) + 37; // Range 37-39
    else if (randomNum == 8) randomSkinColor = Random(2) + 42; // Range 42-43
    else if (randomNum == 9) randomSkinColor = 57; // Range 57
    else if (randomNum == 10) randomSkinColor = 60; // Range 60
    else if (randomNum == 11) randomSkinColor = 63; // Range 63
    else if (randomNum == 12) randomSkinColor = Random(6) + 74; // Range 74-79
    else if (randomNum == 13) randomSkinColor = Random(8) + 80; // Range 80-87
    else if (randomNum == 14) randomSkinColor = Random(25) + 103; // Range 103-127
    else if (randomNum == 15) randomSkinColor = Random(6) + 130; // Range 130-135
    else if (randomNum == 16) randomSkinColor = Random(3) + 155; // Range 155-157
    else if (randomNum == 17) randomSkinColor = Random(8) + 167; // Range 167-174

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Earth Genasi
void RndEarthGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(99);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 3) randomHairColor = Random(4); // Range 0-3
    else if (randomNum == 4) randomHairColor = 7;
    else if (randomNum >= 5 && randomNum <= 7) randomHairColor = Random(3) + 13; // Range 13-15
    else if (randomNum == 8) randomHairColor = 19;
    else if (randomNum == 9) randomHairColor = 23;
    else if (randomNum >= 10 && randomNum <= 14) randomHairColor = Random(5) + 27; // Range 27-31
    else if (randomNum == 15) randomHairColor = 35;
    else if (randomNum >= 16 && randomNum <= 18) randomHairColor = Random(3) + 37; // Range 37-39
    else if (randomNum == 19) randomHairColor = 41; 
    else if (randomNum == 20) randomHairColor = 43; 
    else if (randomNum == 21) randomHairColor = 47;
    else if (randomNum == 22) randomHairColor = 49;
    else if (randomNum == 23) randomHairColor = 57;
    else if (randomNum == 24) randomHairColor = 60;
    else if (randomNum == 25) randomHairColor = 63;
    else if (randomNum >= 26 && randomNum <= 28) randomHairColor = Random(3) + 69; // Range 69-71
    else if (randomNum >= 29 && randomNum <= 33) randomHairColor = Random(5) + 74; // Range 74-79
    else if (randomNum >= 34 && randomNum <= 40) randomHairColor = Random(7) + 80; // Range 80-86
    else if (randomNum >= 41 && randomNum <= 79) randomHairColor = Random(39) + 96; // Range 96-134
    else if (randomNum >= 80 && randomNum <= 99) randomHairColor = Random(20) + 148; // Range 148-167

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Fire Genasi skin color channel
int CheckFireGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 4 && nSkinColor <= 7) ||
           (nSkinColor >= 12 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 22 && nSkinColor <= 23) ||
           (nSkinColor >= 27 && nSkinColor <= 31) ||
           (nSkinColor >= 42 && nSkinColor <= 47) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor == 63) ||
           (nSkinColor >= 64 && nSkinColor <= 67) ||
           (nSkinColor >= 73 && nSkinColor <= 75) ||
           (nSkinColor == 79) ||
           (nSkinColor == 81) ||
           (nSkinColor >= 83 && nSkinColor <= 103) ||
           (nSkinColor >= 112 && nSkinColor <= 119) ||
           (nSkinColor >= 123 && nSkinColor <= 127) ||
           (nSkinColor >= 131 && nSkinColor <= 135) ||
           (nSkinColor >= 156 && nSkinColor <= 162) ||
           (nSkinColor >= 167 && nSkinColor <= 171) ||
           (nSkinColor == 173);
}

//:: Checks for valid Fire Genasi hair color channel
int CheckFireGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 1 && nHairColor <= 7) ||
           (nHairColor == 15) ||
           (nHairColor == 23) ||
           (nHairColor == 31) ||
           (nHairColor == 47) ||
           (nHairColor >= 48 && nHairColor <= 49) ||
           (nHairColor >= 51 && nHairColor <= 55) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor == 63) ||
           (nHairColor >= 64 && nHairColor <= 65) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor == 79) ||
           (nHairColor >= 84 && nHairColor <= 103) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 118 && nHairColor <= 119) ||
           (nHairColor >= 123 && nHairColor <= 127) ||
           (nHairColor >= 131 && nHairColor <= 135) ||
           (nHairColor >= 155 && nHairColor <= 159) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor == 171) ||
           (nHairColor >= 173 && nHairColor <= 175);
}

// Function to randomly set oPC's skin color channel for Fire Genasi
void RndFireGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(87);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 3) randomSkinColor = Random(4) + 4; // Range 4-7
    else if (randomNum >= 4 && randomNum <= 7) randomSkinColor = Random(4) + 12; // Range 12-15
    else if (randomNum >= 8 && randomNum <= 9) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum >= 10 && randomNum <= 11) randomSkinColor = Random(2) + 22; // Range 22-23
    else if (randomNum >= 12 && randomNum <= 16) randomSkinColor = Random(5) + 27; // Range 27-31
    else if (randomNum >= 17 && randomNum <= 22) randomSkinColor = Random(6) + 42; // Range 42-47
    else if (randomNum == 23) randomSkinColor = 57;
    else if (randomNum == 24) randomSkinColor = 60;
    else if (randomNum == 25) randomSkinColor = 63;
    else if (randomNum >= 26 && randomNum <= 29) randomSkinColor = Random(4) + 64; // Range 64-67
    else if (randomNum >= 30 && randomNum <= 32) randomSkinColor = Random(3) + 73; // Range 73-75
    else if (randomNum == 33) randomSkinColor = 79;
    else if (randomNum == 34) randomSkinColor = 81;
    else if (randomNum >= 35 && randomNum <= 55) randomSkinColor = Random(21) + 83; // Range 83-103
    else if (randomNum >= 56 && randomNum <= 63) randomSkinColor = Random(8) + 112; // Range 112-119
    else if (randomNum >= 64 && randomNum <= 68) randomSkinColor = Random(5) + 123; // Range 123-127
    else if (randomNum >= 69 && randomNum <= 73) randomSkinColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 74 && randomNum <= 80) randomSkinColor = Random(7) + 156; // Range 156-162
    else if (randomNum >= 81 && randomNum <= 85) randomSkinColor = Random(5) + 167; // Range 167-171
    else if (randomNum == 86) randomSkinColor = 173;

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Fire Genasi
void RndFireGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(73);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 6) randomHairColor = Random(7) + 1; // Range 1-7
    else if (randomNum == 7) randomHairColor = 15;
    else if (randomNum == 8) randomHairColor = 23;
    else if (randomNum == 9) randomHairColor = 31;
    else if (randomNum == 10) randomHairColor = 47;
    else if (randomNum >= 11 && randomNum <= 12) randomHairColor = Random(2) + 48; // Range 48-49
    else if (randomNum >= 13 && randomNum <= 17) randomHairColor = Random(5) + 51; // Range 51-55
    else if (randomNum == 18) randomHairColor = 57;
    else if (randomNum == 19) randomHairColor = 60;
    else if (randomNum == 20) randomHairColor = 63;
    else if (randomNum >= 21 && randomNum <= 22) randomHairColor = Random(2) + 64; // Range 64-65
    else if (randomNum >= 23 && randomNum <= 24) randomHairColor = Random(2) + 74; // Range 74-75
    else if (randomNum == 25) randomHairColor = 79;
    else if (randomNum >= 26 && randomNum <= 45) randomHairColor = Random(20) + 84; // Range 84-103
    else if (randomNum >= 46 && randomNum <= 49) randomHairColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 50 && randomNum <= 51) randomHairColor = Random(2) + 118; // Range 118-119
    else if (randomNum >= 52 && randomNum <= 56) randomHairColor = Random(5) + 123; // Range 123-127
    else if (randomNum >= 57 && randomNum <= 61) randomHairColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 62 && randomNum <= 66) randomHairColor = Random(5) + 155; // Range 155-159
    else if (randomNum == 67) randomHairColor = 165;
    else if (randomNum == 68) randomHairColor = 167;
    else if (randomNum == 69) randomHairColor = 171;
    else if (randomNum >= 70 && randomNum <= 72) randomHairColor = Random(3) + 173; // Range 173-175

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Water Genasi hair color channel
int CheckWaterGenasiSkin(object oPC)
{
    //:: Get the hair color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the Skin color channel is within any of the specified ranges
    return (nSkinColor >= 16 && nSkinColor <= 23) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor >= 32 && nSkinColor <= 43) ||
           (nSkinColor >= 48 && nSkinColor <= 53) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 69 && nSkinColor <= 86) ||
           (nSkinColor >= 104 && nSkinColor <= 114) ||
           (nSkinColor >= 124 && nSkinColor <= 127) ||
           (nSkinColor >= 132 && nSkinColor <= 143) ||
           (nSkinColor >= 148 && nSkinColor <= 153) ||
           (nSkinColor == 165) ||
           (nSkinColor >= 169 && nSkinColor <= 171);
}

//:: Checks for valid Water Genasi hair color channel
int CheckWaterGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 47) ||
           (nHairColor >= 67 && nHairColor <= 86) ||
           (nHairColor >= 104 && nHairColor <= 115) ||
           (nHairColor >= 121 && nHairColor <= 127) ||
           (nHairColor >= 132 && nHairColor <= 143) ||
           (nHairColor >= 148 && nHairColor <= 153) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor >= 169 && nHairColor <= 171) ||
           (nHairColor == 173);
}

//:: Function to randomly set oPC's skin color channel for Water Genasi
void RndWaterGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(86);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(8) + 16; // Range 16-23
    else if (randomNum >= 8 && randomNum <= 11) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum >= 12 && randomNum <= 23) randomSkinColor = Random(12) + 32; // Range 32-43
    else if (randomNum >= 24 && randomNum <= 29) randomSkinColor = Random(6) + 48; // Range 48-53
    else if (randomNum == 30) randomSkinColor = 60;
    else if (randomNum >= 31 && randomNum <= 48) randomSkinColor = Random(18) + 69; // Range 69-86
    else if (randomNum >= 49 && randomNum <= 59) randomSkinColor = Random(11) + 104; // Range 104-114
    else if (randomNum >= 60 && randomNum <= 63) randomSkinColor = Random(4) + 124; // Range 124-127
    else if (randomNum >= 64 && randomNum <= 75) randomSkinColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 76 && randomNum <= 81) randomSkinColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 82) randomSkinColor = 165;
    else if (randomNum >= 83 && randomNum <= 85) randomSkinColor = Random(3) + 169; // Range 169-171    

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

//:: Function to randomly set oPC's hair color channel for Water Genasi
void RndWaterGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(95);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 31) randomHairColor = Random(32) + 16; // Range 16-47
    else if (randomNum >= 32 && randomNum <= 51) randomHairColor = Random(20) + 67; // Range 67-86
    else if (randomNum >= 52 && randomNum <= 63) randomHairColor = Random(12) + 104; // Range 104-115
    else if (randomNum >= 64 && randomNum <= 70) randomHairColor = Random(7) + 121; // Range 121-127
    else if (randomNum >= 71 && randomNum <= 82) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 83 && randomNum <= 88) randomHairColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 89) randomHairColor = 165;
    else if (randomNum == 90) randomHairColor = 167;
    else if (randomNum >= 91 && randomNum <= 93) randomHairColor = Random(3) + 169; // Range 169-171
    else if (randomNum == 94) randomHairColor = 173;

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Magma Genasi skin color channel
int CheckMagmaGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor == 7) ||
           (nSkinColor >= 13 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor >= 41 && nSkinColor <= 45) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 63 && nSkinColor <= 65) ||
           (nSkinColor >= 87 && nSkinColor <= 91) ||
           (nSkinColor >= 96 && nSkinColor <= 103) ||
           (nSkinColor >= 112 && nSkinColor <= 115) ||
           (nSkinColor >= 118 && nSkinColor <= 119) ||
           (nSkinColor >= 122 && nSkinColor <= 127) ||
           (nSkinColor >= 131 && nSkinColor <= 135) ||
           (nSkinColor >= 158 && nSkinColor <= 159) ||
           (nSkinColor == 165) ||
           (nSkinColor == 167) ||
           (nSkinColor == 171) ||
           (nSkinColor == 173);
}

// Function to check for valid hair color channel for Magma Genasi
int CheckMagmaGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 3 && nHairColor <= 7) ||
           (nHairColor == 15) ||
           (nHairColor >= 22 && nHairColor <= 23) ||
           (nHairColor == 27) ||
           (nHairColor >= 30 && nHairColor <= 31) ||
           (nHairColor == 47) ||
           (nHairColor == 51) ||
           (nHairColor == 53) ||
           (nHairColor == 55) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor >= 63 && nHairColor <= 65) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 103) ||
           (nHairColor >= 111 && nHairColor <= 115) ||
           (nHairColor == 119) ||
           (nHairColor >= 126 && nHairColor <= 127) ||
           (nHairColor >= 133 && nHairColor <= 135) ||
           (nHairColor >= 158 && nHairColor <= 159) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor == 171) ||
           (nHairColor == 173);
}

// Function to randomly set oPC's skin color channel for Magma Genasi
void RndMagmaGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(58);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum == 0) randomSkinColor = 7;
    else if (randomNum >= 1 && randomNum <= 3) randomSkinColor = Random(3) + 13; // Range 13-15
    else if (randomNum >= 4 && randomNum <= 5) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum >= 6 && randomNum <= 9) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum >= 10 && randomNum <= 14) randomSkinColor = Random(5) + 41; // Range 41-45
    else if (randomNum == 15) randomSkinColor = 57;
    else if (randomNum == 16) randomSkinColor = 60;
    else if (randomNum >= 17 && randomNum <= 19) randomSkinColor = Random(3) + 63; // Range 63-65
    else if (randomNum >= 20 && randomNum <= 24) randomSkinColor = Random(5) + 87; // Range 87-91
    else if (randomNum >= 25 && randomNum <= 32) randomSkinColor = Random(8) + 96; // Range 96-103
    else if (randomNum >= 33 && randomNum <= 36) randomSkinColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 37 && randomNum <= 38) randomSkinColor = Random(2) + 118; // Range 118-119
    else if (randomNum >= 39 && randomNum <= 44) randomSkinColor = Random(6) + 122; // Range 122-127
    else if (randomNum >= 45 && randomNum <= 49) randomSkinColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 50 && randomNum <= 51) randomSkinColor = Random(2) + 158; // Range 158-159
    else if (randomNum == 52) randomSkinColor = 165;
    else if (randomNum == 53) randomSkinColor = 167;
    else if (randomNum == 54) randomSkinColor = 171;
    else if (randomNum == 55) randomSkinColor = 173;

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Magma Genasi
void RndMagmaGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(64);

    // Map the random number to a hair color value
    int randomHairColor;

		 if (randomNum >= 0 && randomNum <= 4) randomHairColor = Random(5) + 3; // Range 3-7
    else if (randomNum == 5) randomHairColor =  15; // Single value
    else if (randomNum >= 6 && randomNum <= 7) randomHairColor =  Random(2) + 22; // Range 22-23
    else if (randomNum == 8) randomHairColor =  27; // Single value
    else if (randomNum >= 9 && randomNum <= 10) randomHairColor =  Random(2) + 30; // Range 30-31
    else if (randomNum == 11) randomHairColor =  47; // Single value
    else if (randomNum == 12) randomHairColor =  51; // Single value
    else if (randomNum == 13) randomHairColor =  53; // Single value
    else if (randomNum == 14) randomHairColor =  55; // Single value
    else if (randomNum == 15) randomHairColor =  57; // Single value
    else if (randomNum == 16) randomHairColor =  60; // Single value
    else if (randomNum >= 17 && randomNum <= 19) randomHairColor =  Random(3) + 63; // Range 63-65
    else if (randomNum >= 20 && randomNum <= 21) randomHairColor =  Random(2) + 74; // Range 74-75
    else if (randomNum >= 22 && randomNum <= 46) randomHairColor =  Random(20) + 84; // Range 84-103
    else if (randomNum >= 47 && randomNum <= 51) randomHairColor =  Random(5) + 111; // Range 111-115
    else if (randomNum == 52) randomHairColor =  119; // Single value
    else if (randomNum >= 53 && randomNum <= 54) randomHairColor =  Random(2) + 126; // Range 126-127
    else if (randomNum >= 55 && randomNum <= 57) randomHairColor =  Random(3) + 133; // Range 133-135
    else if (randomNum >= 58 && randomNum <= 59) randomHairColor =  Random(2) + 158; // Range 158-159
    else if (randomNum == 60) randomHairColor =  165; // Single value
    else if (randomNum == 61) randomHairColor =  167; // Single value
    else if (randomNum == 62) randomHairColor =  171; // Single value
    else if (randomNum == 63) randomHairColor =  173; // Single value   

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Rain Genasi skin color channel
int CheckRainGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 16 && nSkinColor <= 23) ||
           (nSkinColor == 28) ||
           (nSkinColor >= 40 && nSkinColor <= 42) ||
           (nSkinColor >= 48 && nSkinColor <= 51) ||
           (nSkinColor >= 76 && nSkinColor <= 79) ||
           (nSkinColor >= 81 && nSkinColor <= 84) ||
           (nSkinColor >= 112 && nSkinColor <= 114) ||
           (nSkinColor >= 121 && nSkinColor <= 126) ||
           (nSkinColor >= 132 && nSkinColor <= 143) ||
           (nSkinColor >= 148 && nSkinColor <= 151) ||
           (nSkinColor == 165) ||
           (nSkinColor >= 167 && nSkinColor <= 169) ||
           (nSkinColor == 171);
}

// Function to check for valid hair color channel for Rain Genasi
int CheckRainGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 35) ||
           (nHairColor >= 69 && nHairColor <= 86) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 124 && nHairColor <= 127) ||
           (nHairColor >= 132 && nHairColor <= 143) ||
           (nHairColor >= 148 && nHairColor <= 153) ||
           (nHairColor == 165) ||
           (nHairColor >= 167 && nHairColor <= 169) ||
           (nHairColor == 171);
}

// Function to randomly set oPC's skin color channel for Rain Genasi
void RndRainGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(52);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(8) + 16; // Range 16-23
    else if (randomNum == 8) randomSkinColor = 28; // Single value
    else if (randomNum >= 9 && randomNum <= 11) randomSkinColor = Random(3) + 40; // Range 40-42
    else if (randomNum >= 12 && randomNum <= 15) randomSkinColor = Random(4) + 48; // Range 48-51
    else if (randomNum >= 16 && randomNum <= 19) randomSkinColor = Random(4) + 76; // Range 76-79
    else if (randomNum >= 20 && randomNum <= 23) randomSkinColor = Random(4) + 81; // Range 81-84
    else if (randomNum >= 24 && randomNum <= 26) randomSkinColor = Random(3) + 112; // Range 112-114
    else if (randomNum >= 27 && randomNum <= 32) randomSkinColor = Random(6) + 121; // Range 121-126
    else if (randomNum >= 33 && randomNum <= 44) randomSkinColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 45 && randomNum <= 48) randomSkinColor = Random(4) + 148; // Range 148-151
    else if (randomNum == 49) randomSkinColor = 165; // Single value
    else if (randomNum >= 50 && randomNum <= 51) randomSkinColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 52) randomSkinColor = 171; // Single value
    else randomSkinColor = 0; // Default case

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Rain Genasi
void RndRainGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(68);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 19) randomHairColor = Random(20) + 16; // Range 16-35
    else if (randomNum >= 20 && randomNum <= 37) randomHairColor = Random(18) + 69; // Range 69-86
    else if (randomNum >= 38 && randomNum <= 41) randomHairColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 42 && randomNum <= 45) randomHairColor = Random(4) + 124; // Range 124-127
    else if (randomNum >= 46 && randomNum <= 57) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 58 && randomNum <= 63) randomHairColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 64) randomHairColor = 165; // Single value
    else if (randomNum >= 65 && randomNum <= 67) randomHairColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 68) randomHairColor = 171; // Single value
    else randomHairColor = 0; // Default case

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

//:: Checks for valid Silt Genasi skin color channel
int CheckSiltGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 39 && nSkinColor <= 47) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 66 && nSkinColor <= 67) ||
           (nSkinColor >= 74 && nSkinColor <= 75) ||
           (nSkinColor >= 84 && nSkinColor <= 86) ||
           (nSkinColor >= 92 && nSkinColor <= 95) ||
           (nSkinColor >= 108 && nSkinColor <= 111) ||
           (nSkinColor >= 112 && nSkinColor <= 115) ||
           (nSkinColor >= 120 && nSkinColor <= 127) ||
           (nSkinColor >= 154 && nSkinColor <= 155) ||
           (nSkinColor == 172) ||
           (nSkinColor == 174);
}

// Function to check for valid hair color channel for Silt Genasi
int CheckSiltGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 7 && nHairColor <= 11) ||
           (nHairColor >= 24 && nHairColor <= 31) ||
           (nHairColor >= 40 && nHairColor <= 43) ||
           (nHairColor >= 54 && nHairColor <= 55) ||
           (nHairColor == 58) ||
           (nHairColor == 60) ||
           (nHairColor >= 66 && nHairColor <= 67) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 86) ||
           (nHairColor >= 92 && nHairColor <= 95) ||
           (nHairColor >= 104 && nHairColor <= 111) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 120 && nHairColor <= 127) ||
           (nHairColor >= 130 && nHairColor <= 131) ||
           (nHairColor >= 154 && nHairColor <= 157) ||
           (nHairColor == 167) ||
           (nHairColor == 172);
}

// Function to randomly set oPC's skin color channel for Silt Genasi
void RndSiltGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(58);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(9) + 39; // Range 39-47
    else if (randomNum == 8) randomSkinColor = 60; // Range 60
    else if (randomNum >= 9 && randomNum <= 10) randomSkinColor = Random(2) + 66; // Range 66-67
    else if (randomNum >= 11 && randomNum <= 12) randomSkinColor = Random(2) + 74; // Range 74-75
    else if (randomNum >= 13 && randomNum <= 15) randomSkinColor = Random(3) + 84; // Range 84-86
    else if (randomNum >= 16 && randomNum <= 19) randomSkinColor = Random(4) + 92; // Range 92-95
    else if (randomNum >= 20 && randomNum <= 23) randomSkinColor = Random(4) + 108; // Range 108-111
    else if (randomNum >= 24 && randomNum <= 29) randomSkinColor = Random(6) + 112; // Range 112-115
    else if (randomNum >= 30 && randomNum <= 41) randomSkinColor = Random(12) + 120; // Range 120-127
    else if (randomNum >= 42 && randomNum <= 55) randomSkinColor = Random(14) + 154; // Range 154-157
    else if (randomNum == 56) randomSkinColor = 172; // Single value
    else if (randomNum == 57) randomSkinColor = 174; // Single value

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Silt Genasi
void RndSiltGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(80);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 4) randomHairColor = Random(5) + 7; // Range 7-11
    else if (randomNum >= 5 && randomNum <= 18) randomHairColor = Random(8) + 24; // Range 24-31
    else if (randomNum >= 19 && randomNum <= 20) randomHairColor = Random(2) + 40; // Range 40-43
    else if (randomNum >= 21 && randomNum <= 22) randomHairColor = Random(2) + 54; // Range 54-55
    else if (randomNum == 23) randomHairColor = 58; // Single value
    else if (randomNum == 24) randomHairColor = 60; // Single value
    else if (randomNum >= 25 && randomNum <= 26) randomHairColor = Random(2) + 66; // Range 66-67
    else if (randomNum >= 27 && randomNum <= 28) randomHairColor = Random(2) + 74; // Range 74-75
    else if (randomNum >= 29 && randomNum <= 31) randomHairColor = Random(3) + 84; // Range 84-86
    else if (randomNum >= 32 && randomNum <= 35) randomHairColor = Random(4) + 92; // Range 92-95
    else if (randomNum >= 36 && randomNum <= 47) randomHairColor = Random(12) + 104; // Range 104-111
    else if (randomNum >= 48 && randomNum <= 59) randomHairColor = Random(12) + 112; // Range 112-115
    else if (randomNum >= 60 && randomNum <= 71) randomHairColor = Random(12) + 120; // Range 120-127
    else if (randomNum >= 72 && randomNum <= 73) randomHairColor = Random(2) + 130; // Range 130-131
    else if (randomNum >= 74 && randomNum <= 77) randomHairColor = Random(4) + 154; // Range 154-157
    else if (randomNum == 78) randomHairColor = 167; // Single value
    else if (randomNum == 79) randomHairColor = 172; // Single value

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}

// Function to check for valid skin color channel for Sun Genasi
int CheckSunGenasiSkin(object oPC)
{
    // Get the skin color channel
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    // Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 0 && nSkinColor <= 14) ||
           (nSkinColor >= 24 && nSkinColor <= 27) ||
           (nSkinColor >= 54 && nSkinColor <= 55) ||
           (nSkinColor >= 57 && nSkinColor <= 59) ||
           (nSkinColor == 66) ||
           (nSkinColor >= 87 && nSkinColor <= 95) ||
           (nSkinColor >= 116 && nSkinColor <= 117) ||
           (nSkinColor >= 128 && nSkinColor <= 129) ||
           (nSkinColor >= 154 && nSkinColor <= 159) ||
           (nSkinColor == 167) ||
           (nSkinColor == 172) ||
           (nSkinColor == 175);
}

// Function to check for valid hair color channel for Sun Genasi
int CheckSunGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 8 && nHairColor <= 15) ||
           (nHairColor >= 42 && nHairColor <= 47) ||
           (nHairColor == 56) ||
           (nHairColor == 58) ||
           (nHairColor >= 65 && nHairColor <= 67) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 95) ||
           (nHairColor >= 100 && nHairColor <= 103) ||
           (nHairColor >= 154 && nHairColor <= 163) ||
           (nHairColor == 167) ||
           (nHairColor == 172) ||
           (nHairColor >= 173 && nHairColor <= 175);
}

// Function to randomly set oPC's skin color channel for Sun Genasi
void RndSunGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(43);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 14) randomSkinColor = Random(15); // Range 0-14
    else if (randomNum >= 15 && randomNum <= 18) randomSkinColor = Random(4) + 24; // Range 24-27
    else if (randomNum >= 19 && randomNum <= 20) randomSkinColor = Random(2) + 54; // Range 54-55
    else if (randomNum >= 21 && randomNum <= 23) randomSkinColor = Random(2) + 57; // Range 57-59
    else if (randomNum == 24) randomSkinColor = 66; // Range 66
    else if (randomNum >= 25 && randomNum <= 32) randomSkinColor = Random(9) + 87; // Range 87-95
    else if (randomNum >= 33 && randomNum <= 34) randomSkinColor = Random(2) + 116; // Range 116-117
    else if (randomNum >= 35 && randomNum <= 36) randomSkinColor = Random(2) + 128; // Range 128-129
    else if (randomNum >= 37 && randomNum <= 40) randomSkinColor = Random(5) + 154; // Range 154-159
    else if (randomNum == 41) randomSkinColor = 167; // Single value
    else if (randomNum == 42) randomSkinColor = 172; // Single value
    else if (randomNum == 43) randomSkinColor = 175; // Single value

    // Set the random skin color value for oPC
    SetColor(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Sun Genasi
void RndSunGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(73);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 6) randomHairColor = Random(8) + 16; // Range 16-23
    else if (randomNum == 7) randomHairColor = 28; // Single value
    else if (randomNum >= 8 && randomNum <= 10) randomHairColor = Random(3) + 40; // Range 40-42
    else if (randomNum >= 11 && randomNum <= 14) randomHairColor = Random(4) + 48; // Range 48-51
    else if (randomNum >= 15 && randomNum <= 18) randomHairColor = Random(4) + 76; // Range 76-79
    else if (randomNum >= 19 && randomNum <= 21) randomHairColor = Random(4) + 81; // Range 81-84
    else if (randomNum >= 22 && randomNum <= 24) randomHairColor = Random(3) + 112; // Range 112-114
    else if (randomNum >= 25 && randomNum <= 30) randomHairColor = Random(6) + 121; // Range 121-126
    else if (randomNum >= 31 && randomNum <= 42) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 43 && randomNum <= 46) randomHairColor = Random(4) + 148; // Range 148-151
    else if (randomNum == 47) randomHairColor = 165; // Single value
    else if (randomNum >= 48 && randomNum <= 50) randomHairColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 51) randomHairColor = 171; // Single value
    else if (randomNum == 52) randomHairColor = 173; // Single value
    else if (randomNum >= 53 && randomNum <= 61) randomHairColor = Random(9) + 174; // Range 174-182
    else if (randomNum >= 62 && randomNum <= 64) randomHairColor = Random(3) + 187; // Range 187-189
    else if (randomNum == 65) randomHairColor = 194; // Single value
    else if (randomNum == 66) randomHairColor = 195; // Single value
    else if (randomNum >= 67 && randomNum <= 70) randomHairColor = Random(4) + 199; // Range 199-202
    else if (randomNum == 71) randomHairColor = 208; // Single value
    else if (randomNum == 72) randomHairColor = 213; // Single value
    else if (randomNum == 73) randomHairColor = 215; // Single value

    // Set the random hair color value for oPC
    SetColor(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}
