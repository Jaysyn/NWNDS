//:: ds_enf_genasi.nss
/*	
*	Checks that PC Genasis are using an appropriate
*	skin & hair color for their racialtype & set
*	them to an appropriate color if not.
*
*/
/* #include "prc_color_const" */
#include "prc_inc_racial"

/*
33	RACIAL_TYPE_MAGMA_GEN
34	RACIAL_TYPE_RAIN_GEN
35	RACIAL_TYPE_SILT_GEN
36	RACIAL_TYPE_SUN_GEN
199	RACIAL_TYPE_AIR_GEN
200	RACIAL_TYPE_EARTH_GEN
202	RACIAL_TYPE_FIRE_GEN
205	RACIAL_TYPE_WATER_GEN
*/

//:: Check & fix Air Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_AIR_GEN  && CheckAirGenasiSkin(oPC) != TRUE)
	{
		RndAirGenasiSkin(oPC);
	}
	
//:: Check & fix Air Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_AIR_GEN  && CheckAirGenasiHair(oPC) != TRUE)
	{
		RndAirGenasiHair(oPC);
	}

//:: Check & fix Earth Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_EARTH_GEN  && CheckEarthGenasiSkin(oPC) != TRUE)
	{
		RndEarthGenasiSkin(oPC);
	}
	
//:: Check & fix Earth Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_EARTH_GEN  && CheckEarthGenasiHair(oPC) != TRUE)
	{
		RndEarthGenasiHair(oPC);
	}
	
//:: Check & fix Fire Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_FIRE_GEN  && CheckFireGenasiSkin(oPC) != TRUE)
	{
		RndFireGenasiSkin(oPC);
	}
	
//:: Check & fix Fire Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_FIRE_GEN  && CheckFireGenasiHair(oPC) != TRUE)
	{
		RndFireGenasiHair(oPC);
	}	
	
//:: Check & fix Water Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_WATER_GEN  && CheckWaterGenasiSkin(oPC) != TRUE)
	{
		RndWaterGenasiSkin(oPC);
	}
	
//:: Check & fix Water Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_WATER_GEN  && CheckWaterGenasiHair(oPC) != TRUE)
	{
		RndWaterGenasiHair(oPC);
	}		
	
//:: Check & fix Magma Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_MAGMA_GEN  && CheckMagmaGenasiSkin(oPC) != TRUE)
	{
		RndMagmaGenasiSkin(oPC);
	}
	
//:: Check & fix Magma Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_MAGMA_GEN  && CheckMagmaGenasiHair(oPC) != TRUE)
	{
		RndMagmaGenasiHair(oPC);
	}		
		
//:: Check & fix Rain Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_RAIN_GEN  && CheckRainGenasiSkin(oPC) != TRUE)
	{
		RndRainGenasiSkin(oPC);
	}
	
//:: Check & fix Rain Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_RAIN_GEN  && CheckRainGenasiHair(oPC) != TRUE)
	{
		RndRainGenasiHair(oPC);
	}	

//:: Check & fix Silt Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SILT_GEN  && CheckSiltGenasiSkin(oPC) != TRUE)
	{
		RndSiltGenasiSkin(oPC);
	}
	
//:: Check & fix Silt Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SILT_GEN  && CheckSiltGenasiHair(oPC) != TRUE)
	{
		RndSiltGenasiHair(oPC);
	}

//:: Check & fix Sun Genasi Skin Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SUN_GEN  && CheckSunGenasiSkin(oPC) != TRUE)
	{
		RndSunGenasiSkin(oPC);
	}
	
//:: Check & fix Sun Genasi Hair Color Channel
    if ( GetRacialType(oPC) == RACIAL_TYPE_SUN_GEN  && CheckSunGenasiHair(oPC) != TRUE)
	{
		RndSunGenasiHair(oPC);
	}

	

//:: Skin color reference 
/*
	001 = DARK_VANILLA
	002 = CAMEO
	003 = TAN
	004 = BRANDY_ROSE
	005 = KURUMIZOME_BROWN
	006 = PRUSSIAN_RED
	007 = TOAST
	008 = RUSSET
	009 = AKAROA
	010 = ECRU
	011 = CORKBOARD
	012 = CLOUDY_CINNAMON
	013 = CLAMSHELL
	014 = OLD_ROSE
	015 = CORAL_TREE
	016 = ALAEA
	
	017 = COTTON_SEED
	018 = CLOUDY
	019 = TAPA
	020 = DOVE_GRAY
	021 = HEATHER
	022 = BALI_HAI
	023 = SLATE_GRAY
	024 = DEEP_PAYNES_GRAY
	025 = TAHUNA_SANDS
	026 = STRAW
	027 = DONKEY_BROWN
	028 = BURLAP
	029 = SHUTTLE_GRAY
	030 = COAL_MINE
	031 = ABBEY
	032 = MAKO
	
	033 = THISTLE_GREEN
	034 = OLIVINE
	035 = VINTAGE
	036 = MEADOWLAND
	037 = SPRING_RAIN
	038 = BAY_LEAF
	039 = GLADE_GREEN
	040 = CACTUS
	041 = NOBEL_GRAY
	042 = DUSTY_GRAY
	043 = INDUSTRIAL
	044 = RHINE_CASTLE
	045 = ROSE_VALE
	046 = CARMINE
	047 = MAGENTA
	048 = ASTER_PURPLE
	
	049 = DULL_BLUE_VIOLET
	050 = HELVETIA_BLUE
	051 = TYROLITE_GREEN
	052 = CAPRI_BLUE
	053 = PEACOCK_GREEN
	054 = COSSACK_GREEN
	055 = CALLISTE_GREEN
	056 = WARBLER_GREEN
	057 = SILVER
	058 = QUAKER_DRAB
	059 = MASSICOT_YELLOW
	060 = WAFER
	061 = TUNDORA
	062 = AZO_BLUE
	063 = WHITE
	064 = BLACK
	
	065 = CONGO_BROWN
	066 = MARS_VIOLET
	067 = OLD_COPPER
	068 = DARK_OLIVE
	069 = CHALET_GREEN
	070 = DARK_GRAY_OLIVE
	071 = DARK_VIOLET
	072 = DARK_SLATE
	073 = DARK_PURPLE
	074 = OLD_LAVENDER
	075 = CAMEO_BROWN
	076 = FLINT
	077 = TERRA_VERTE
	078 = SLATE_OLIVE
	079 = BAYOUX_BLUE
	080 = VIOLET_SLATE
	
	081 = VERDIGRIS
	082 = SIAM_GREY
	083 = BLUISH_VIOLET
	084 = MID_GRAY
	085 = DUSKY_DRAB
	086 = GRAYISH_BROWN
	087 = VINACEOUS_SLATE
	088 = TOBACCO
	089 = VERMILLION
	090 = BURNT_UMBER
	091 = CEDAR_STAFF
	092 = SMOKED_PAPRIKA
	093 = TUSSOCK
	094 = LUXOR_GOLD
	095 = PESTO
	096 = BREEN
	
	097 = CONTESSA
	098 = CANDIED_APPLE
	099 = UMEMURASAKI_PURPLE
	100 = TAWNY_PORT
	101 = ANTIQUE_BRASS
	102 = SANTA_FE
	103 = SEPIA
	104 = METALLIC_COPPER
	105 = VERDANT_HAVEN
	106 = WAKATAKE_GREEN
	107 = PINE
	108 = WOODLAND
	109 = PLYMOUTH_GREEN
	110 = VETIVER_GREEN
	111 = JADE_GREEN
	112 = RAW_UMBER
	
	113 = OLIVE_GRAY
	114 = LIMED_ASH
	115 = FUSCOUS_GRAY
	116 = TUNGSTEN
	117 = SORRELL_BROWN
	118 = SANDALWOOD
	119 = NATAL_BROWN
	120 = MOROCCO_BROWN
	121 = INDIAN_KHAKI
	122 = ROSYBROWN
	123 = YELLOWISH_OLIVE
	124 = MUMMY_BROWN
	125 = PALE_MOUSE_GRAY
	126 = LIGHT_QUAKER_DRAB
	127 = DEEP_QUAKER_DRAB
	128 = TAUPE_BROWN
	
	129 = VANILLA
	130 = SANDRIFT
	131 = BUFF_BROWN
	132 = SHINGLE_FAWN
	133 = PLUMBEOUS
	134 = DEEP_VARLEYS_GRAY
	135 = SLATE_VIOLET
	136 = SLATE_BLACK
	137 = LIGHT_VIOLET_BUE
	138 = ULTRAMARINE_ASH
	139 = KASHMIR_BLUE
	140 = HORTENSE_BLUE
	141 = CALAMINE_BLUE
	142 = BERYL_GREEN
	143 = SMALT_BLUE
	144 = DUSKY_GREEN_BLUE
	
	145 = ORCHID
	146 = PHLOX_PURPLE
	147 = VIOLET_PURPLE
	148 = HYACINTH_VIOLET
	149 = LIGHT_NEROPALIN_BLUE
	150 = COLUMBIA_BLUE
	151 = GLAUCOUS_BLUE
	152 = DARK_CINNABAR_GREEN
	153 = LIGHT_ORIENTAL_GREEN
	154 = WINTER_GREEN
	155 = PALE_GREENISH_YELLOW
	156 = LIGHT_CRESS_GREEN
	157 = AVELLANEOUS
	158 = COCOA_PECAN
	159 = ORANGE_VINACEOUS
	160 = HYDRANGEA_RED
	
	161 = EUPATORIUM_PURPLE
	162 = VINACEOUS_PURPLE
	163 = LIGHT_RUSSET
	164 = MATHEWS_PURPLE
	165 = MAUVETTE
	166 = DARK_MADDER_BLUE
	167 = MATTE_WHITE
	168 = MATTE_BLACK
	169 = LIGHT_MINERAL_GRAY
	170 = DUSKY_BLUE_GREEN
	171 = PLUMMY
	172 = BLACK_ORCHID
	173 = DARK_CITRINE
	174 = ROSY_SANDSTONE
	175 = MOCHA
	176 = GOLD
*/

//:: Hair Color Reference
/*	
	001 = AMBER_BROWN
	002 = CHESTNUT
	003 = MAROON
	004 = CHOCOLATE
	005 = SPECTRUM_RED
	006 = CARMINE
	007 = CLARET_BROWN
	008 = HEATH_BROWN
	009 = SEAFOAM_YELLOW
	010 = REED_YELLOW
	011 = DARK_OLIVE_BUFF
	012 = BUFFY_BROWN
	013 = ARMY_BROWN
	014 = ARGUS_BROWN
	015 = BRACKEN
	016 = BROWN_BLACK
	
	017 = PALLID_BLUE
	018 = PALE_RUSSIAN_BLUE
	019 = LIGHT_SLATE_GRAY
	020 = SLATE
	021 = DEEP_DUTCH_BLUE
	022 = GRAVEL_GREY_BLUE
	023 = BLUISH_SLATE_BLACK
	024 = DARK_SLATE_GRAY	
	025 = CHICORY_BLUE
	026 = MIDNIGHT_BLUE
	027 = DEEP_PLUMBAGO_BLUE
	028 = PLUM_PURPLE
	029 = FORGET_ME_NOT_BLUE
	030 = PASSIONATE_BLUE
	031 = SILVER_CHALICE
	032 = CHARCOAL
	
	033 = NILE_BLUE
	034 = ANTHRACENE_GREEN
	035 = BLUISH_GLAUCOUS
	036 = DUSKY_SLATE_BLUE
	037 = VARISCITE_GREEN
	038 = DUSKY_DULL_GREEN
	039 = COURT_GRAY
	040 = DULL_BLACKISH_GREEN
	041 = OLIVINE
	042 = DARK_OLIVE_GREEN
	043 = ASH_GRAY
	044 = OLIVACEOUS_BLACK
	045 = OLIVE_BUFF
	046 = MEDAL_BRONZE
	047 = PALLID_MOUSE_GRAY
	048 = BROWNISH_GRAY
	
	049 = DEEP_OLIVE_BUFF
	050 = ANTIQUE_BRASS
	051 = SILVER_PINK
	052 = TAUPE_BROWN
	053 = VINACEOUS
	054 = PERSIAN_PLUM
	055 = PALE_PURPLE_DRAB
	056 = DARK_LIVID_PURPLE
	057 = CEREBRAL_GREY
	058 = DOVE_GRAY
	059 = MASSICOT_YELLOW
	060 = DESERT_SAND
	061 = DAVYS_GRAY
	062 = MIRROR
	064 = GLOSS_WHITE
	064 = GLOSS_BLACK
	
	065 = NEUTRAL_RED
	066 = DARK_MINERAL_RED
	067 = COYOTE_BROWN
	068 = UMBER
	069 = HUNTER_GREEN
	070 = DARK_GRAYISH_OLIVE
	071 = DARK_VIOLET
	072 = DEEP_SLATE_VIOLET
	073 = DULL_DARK_PURPLE
	074 = DARK_SLATE_VIOLET
	075 = MARS_BROWN
	076 = DARK_PURPLE_DRAB
	077 = DARK_RUSSIAN_GREEN
	078 = DARK_NEUTRAL_GRAY
	079 = INDIGO_BLUE
	080 = TITANIUM_GRAY
	
	081 = WARPLOCK_BRONZE
	082 = CHAETURA_DRAB
	083 = DUSK_BLUE
	084 = GRAPHITE_GRAY
	085 = FUSCOUS_GRAY
	086 = BONE_BROWN
	087 = ESPRESSO
	088 = VANDYKE_BROWN
	089 = DEEP_REDDISH_ORANGE
	090 = COPPER_BROWN
	091 = BURNT_UMBER
	092 = OX_BLOOD_RED
	093 = PYRITE_YELLOW
	094 = BUCKTHORN_BROWN
	095 = ANTIQUE_BROWN
	096 = PEARL_GOLD
	
	097 = LIGHT_PINK
	098 = ANTIQUE_PINK
	099 = ACAJOU_RED
	100 = VIOLET_CARMINE
	101 = BEIGE_RED
	102 = PECAN_BROWN
	103 = POMPEIAN_RED
	104 = SADDLE_BROWN
	105 = FOLIAGE
	106 = MOSS
	107 = FOREST_GREEN
	108 = LEAF_GREEN
	109 = CORYDALIS_GREEN
	110 = TEA_GREEN
	111 = JADE_GREEN
	112 = WOODLAND
	
	113 = OLIVE_GRAY
	114 = MOUSE_GRAY
	115 = DARK_GRAYISH_BROWN
	116 = ANILINE_BLACK
	117 = TAN
	118 = TAUPE
	119 = NATAL_BROWN
	120 = MAHOGANY_BROWN
	121 = BEIGE
	122 = VETIVER_GREEN
	123 = GOLD_FUSION
	124 = DARK_OLIVE
	125 = PALE_MOUSE_GRAY
	126 = LIGHT_QUAKER_DRAB
	127 = DARK_VINACEOUS_DRAB
	128 = MASALA_BROWN
	
	129 = PALE_OLIVE_BUFF
	130 = SAGE
	131 = BEAVER
	132 = COFFEE
	133 = PLUMBEOUS
	134 = VARLEYS_GRAY
	135 = DARK_SLATE_VIOLET
	136 = BLACK_GREEN
	137 = PASTEL_TURQUOISE
	138 = COMMELINA_BLUE
	139 = DARK_DIVA_BLUE
	140 = HORTENSE_BLUE
	141 = PALE_METHYL_BLUE
	142 = BERYL_GREEN
	143 = LIGHT_TERRE_VERTE
	144 = MAGNETIC_GREEN
	
	145 = LIGHT_MALLOW_PURPLE
	146 = MALLOW_PURPLE
	147 = PHLOX_PURPLE
	148 = HYACINTH_VIOLET
	149 = LIGHT_SQUILL_BLUE
	150 = COLUMBIA_BLUE
	151 = GLAUCOUS_BLUE
	152 = DARK_CINNABAR_GREEN
	153 = LIGHT_ORIENTAL_GREEN
	154 = WINTER_GREEN
	155 = KILDARE_GREEN
	156 = DULL_CITRINE
	157 = LIGHT_CRESS_GREEN
	158 = AVELLANEOUS
	159 = TAHINI_BROWN
	160 = DEEP_CORINTHIAN_RED
	
	161 = ARGYLE_PURPLE
	162 = LIGHT_PERILLA_PURPLE
	163 = PINKISH_VINACEOUS
	164 = MATHEWS_PURPLE
	165 = MAUVETTE
	166 = DARK_MADDER_BLUE
	167 = MATTE_WHITE
	168 = MATTE_BLACK
	169 = PALE_GULL_GRAY
	170 = NICKEL_GREEN
	171 = MADDER_VIOLET
	172 = DEEP_MADDER_BLUE
	173 = DARK_CITRINE
	174 = MAHOGANY
	175 = LIGHT_BROWNISH_OLIVE
	176 = GOLD
	
		
*/

	
/*	Air Genasi Colors
	
	Skin Color Channels
	20-23
	40
	50-51
	132-133
	136-137
	140-141
	148-149
	164
	166
	
CHANNEL_SKIN_DOVE_GRAY
CHANNEL_SKIN_HEATHER
CHANNEL_SKIN_BALI_HAI
CHANNEL_SKIN_SLATE_GRAY
CHANNEL_SKIN_CACTUS
CHANNEL_SKIN_CALLISTE_GREEN
CHANNEL_SKIN_WARBLER_GREEN
CHANNEL_SKIN_PLUMBEOUS
CHANNEL_SKIN_DEEP_VARLEYS_GRAY
CHANNEL_SKIN_SLATE_VIOLET
CHANNEL_SKIN_SLATE_BLACK
CHANNEL_SKIN_MATHEWS_PURPLE
CHANNEL_SKIN_MAUVETTE
CHANNEL_SKIN_LIGHT_RUSSET
CHANNEL_SKIN_MATTE_WHITE
CHANNEL_SKIN_ROSY_SANDSTONE
CHANNEL_SKIN_MOCHA

	
	Hair Color Channels
	16-35
	44
	46
	54
	56
	57
	70
	77-79
	82-84
	128-150
	163-171
	
CHANNEL_HAIR_BROWN_BLACK
CHANNEL_HAIR_PALLID_BLUE
CHANNEL_HAIR_PALE_RUSSIAN_BLUE
CHANNEL_HAIR_LIGHT_SLATE_GRAY
CHANNEL_HAIR_SLATE
CHANNEL_HAIR_DEEP_DUTCH_BLUE
CHANNEL_HAIR_GRAVEL_GREY_BLUE
CHANNEL_HAIR_BLUISH_SLATE_BLACK
CHANNEL_HAIR_DARK_SLATE_GRAY
CHANNEL_HAIR_CHICORY_BLUE
CHANNEL_HAIR_MIDNIGHT_BLUE
CHANNEL_HAIR_DEEP_PLUMBAGO_BLUE
CHANNEL_HAIR_PLUM_PURPLE
CHANNEL_HAIR_FORGET_ME_NOT_BLUE
CHANNEL_HAIR_PASSIONATE_BLUE
CHANNEL_HAIR_SILVER_CHALICE
CHANNEL_HAIR_CHARCOAL
CHANNEL_HAIR_NILE_BLUE
CHANNEL_HAIR_ANTHRACENE_GREEN
CHANNEL_HAIR_BLUISH_GLAUCOUS
CHANNEL_HAIR_DUSKY_SLATE_BLUE
CHANNEL_HAIR_VARISCITE_GREEN
CHANNEL_HAIR_DUSKY_DULL_GREEN
CHANNEL_HAIR_COURT_GRAY
CHANNEL_HAIR_DULL_BLACKISH_GREEN
CHANNEL_HAIR_OLIVINE
CHANNEL_HAIR_DARK_OLIVE_GREEN
CHANNEL_HAIR_ASH_GRAY
CHANNEL_HAIR_OLIVACEOUS_BLACK
CHANNEL_HAIR_OLIVE_BUFF
CHANNEL_HAIR_MEDAL_BRONZE
CHANNEL_HAIR_PALLID_MOUSE_GRAY
CHANNEL_HAIR_BROWNISH_GRAY
CHANNEL_HAIR_DEEP_OLIVE_BUFF
CHANNEL_HAIR_ANTIQUE_BRASS
CHANNEL_HAIR_SILVER_PINK
CHANNEL_HAIR_TAUPE_BROWN
CHANNEL_HAIR_VINACEOUS
CHANNEL_HAIR_PERSIAN_PLUM
CHANNEL_HAIR_PALE_PURPLE_DRAB
CHANNEL_HAIR_DARK_LIVID_PURPLE
CHANNEL_HAIR_CEREBRAL_GREY
CHANNEL_HAIR_DOVE_GRAY
CHANNEL_HAIR_MASSICOT_YELLOW
CHANNEL_HAIR_DESERT_SAND
CHANNEL_HAIR_DAVYS_GRAY
CHANNEL_HAIR_MIRROR
CHANNEL_HAIR_GLOSS_WHITE
CHANNEL_HAIR_GLOSS_BLACK
CHANNEL_HAIR_NEUTRAL_RED
CHANNEL_HAIR_DARK_MINERAL_RED
CHANNEL_HAIR_COYOTE_BROWN
CHANNEL_HAIR_UMBER
CHANNEL_HAIR_HUNTER_GREEN
CHANNEL_HAIR_DARK_GRAYISH_OLIVE
CHANNEL_HAIR_DARK_VIOLET
CHANNEL_HAIR_DEEP_SLATE_VIOLET
CHANNEL_HAIR_DULL_DARK_PURPLE
CHANNEL_HAIR_SLATE_VIOLET
CHANNEL_HAIR_MARS_BROWN
CHANNEL_HAIR_DARK_PURPLE_DRAB
CHANNEL_HAIR_DARK_RUSSIAN_GREEN
CHANNEL_HAIR_DARK_NEUTRAL_GRAY
CHANNEL_HAIR_INDIGO_BLUE
CHANNEL_HAIR_TITANIUM_GRAY
CHANNEL_HAIR_WARPLOCK_BRONZE
CHANNEL_HAIR_CHAETURA_DRAB
CHANNEL_HAIR_DUSK_BLUE
CHANNEL_HAIR_GRAPHITE_GRAY
CHANNEL_HAIR_FUSCOUS_GRAY
CHANNEL_HAIR_BONE_BROWN
CHANNEL_HAIR_ESPRESSO
CHANNEL_HAIR_VANDYKE_BROWN
CHANNEL_HAIR_DEEP_REDDISH_ORANGE
CHANNEL_HAIR_COPPER_BROWN
CHANNEL_HAIR_BURNT_UMBER
CHANNEL_HAIR_OX_BLOOD_RED
CHANNEL_HAIR_PYRITE_YELLOW
CHANNEL_HAIR_BUCKTHORN_BROWN
CHANNEL_HAIR_ANTIQUE_BROWN
CHANNEL_HAIR_PEARL_GOLD
CHANNEL_HAIR_LIGHT_PINK
CHANNEL_HAIR_ANTIQUE_PINK
CHANNEL_HAIR_ACAJOU_RED
CHANNEL_HAIR_VIOLET_CARMINE
CHANNEL_HAIR_BEIGE_RED
CHANNEL_HAIR_PECAN_BROWN
CHANNEL_HAIR_POMPEIAN_RED
CHANNEL_HAIR_SADDLE_BROWN
CHANNEL_HAIR_FOLIAGE
CHANNEL_HAIR_MOSS
CHANNEL_HAIR_FOREST_GREEN
CHANNEL_HAIR_LEAF_GREEN
CHANNEL_HAIR_CORYDALIS_GREEN
CHANNEL_HAIR_TEA_GREEN
CHANNEL_HAIR_JADE_GREEN
CHANNEL_HAIR_WOODLAND
CHANNEL_HAIR_OLIVE_GRAY
CHANNEL_HAIR_MOUSE_GRAY
CHANNEL_HAIR_DARK_GRAYISH_BROWN
CHANNEL_HAIR_ANILINE_BLACK
CHANNEL_HAIR_TAN
CHANNEL_HAIR_TAUPE
CHANNEL_HAIR_NATAL_BROWN
CHANNEL_HAIR_MAHOGANY_BROWN
CHANNEL_HAIR_BEIGE
CHANNEL_HAIR_VETIVER_GREEN
CHANNEL_HAIR_GOLD_FUSION
CHANNEL_HAIR_NICKEL_GREEN
CHANNEL_HAIR_MADDER_VIOLET
	

*/	

//:: Checks for valid Air Genasi skin color channel
int CheckAirGenasiSkin(object oPC)
{
	//:: Get the skin color channel
    int nSkinColor 	= GetColorChannel(oPC, COLOR_CHANNEL_SKIN);
	
	//:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 20 && nSkinColor <= 23) ||
           (nSkinColor == 40) ||
           (nSkinColor >= 50 && nSkinColor <= 51) ||
           (nSkinColor >= 132 && nSkinColor <= 133) ||
           (nSkinColor >= 136 && nSkinColor <= 137) ||
           (nSkinColor >= 140 && nSkinColor <= 141) ||
           (nSkinColor >= 148 && nSkinColor <= 149) ||
           (nSkinColor == 164) ||
           (nSkinColor == 166);
}

//:: Checks for valid Air Genasi hair color channel
int CheckAirGenasiHair(object oPC)
{
	//:: Get the hair color channel
    int nHairColor 	= GetColorChannel(oPC, COLOR_CHANNEL_HAIR);
	
	//:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 35) ||
           (nHairColor == 44) ||
           (nHairColor == 46) ||
           (nHairColor == 54) ||
           (nHairColor == 56) ||
           (nHairColor == 57) ||
           (nHairColor == 70) ||
           (nHairColor >= 77 && nHairColor <= 79) ||
           (nHairColor >= 82 && nHairColor <= 84) ||
           (nHairColor >= 128 && nHairColor <= 150) ||
           (nHairColor >= 163 && nHairColor <= 171);
}

// Function to randomly set oPC's skin color channel for Air Genasi
void RndAirGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(9);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum == 0) randomSkinColor = 20 + Random(4); // Range 20-23
    else if (randomNum == 1) randomSkinColor = 40;
    else if (randomNum == 2) randomSkinColor = 50 + Random(2); // Range 50-51
    else if (randomNum == 3) randomSkinColor = 132 + Random(2); // Range 132-133
    else if (randomNum == 4) randomSkinColor = 136 + Random(2); // Range 136-137
    else if (randomNum == 5) randomSkinColor = 140 + Random(2); // Range 140-141
    else if (randomNum == 6) randomSkinColor = 148 + Random(2); // Range 148-149
    else if (randomNum == 7) randomSkinColor = 164;
    else if (randomNum == 8) randomSkinColor = 166;

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Air Genasi
void RndAirGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(11);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum == 0) randomHairColor = Random(20) + 16; // Range 16-35
    else if (randomNum == 1) randomHairColor = 44; // Single value
    else if (randomNum == 2) randomHairColor = 46; // Single value
    else if (randomNum == 3) randomHairColor = 54; // Single value
    else if (randomNum == 4) randomHairColor = 56; // Single value
    else if (randomNum == 5) randomHairColor = 57; // Single value
    else if (randomNum == 6) randomHairColor = 70; // Single value
    else if (randomNum == 7) randomHairColor = Random(3) + 77; // Range 77-79
    else if (randomNum == 8) randomHairColor = Random(3) + 82; // Range 82-84
    else if (randomNum == 9) randomHairColor = Random(23) + 128; // Range 128-150
    else if (randomNum == 10) randomHairColor = Random(9) + 163; // Range 163-171

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/*	Earth Genasi Colors

	Skin Color Channels
	3-7
	11
	14-15
	18-19
	21-23
	28-31
	35
	37-39
	42-43
	57
	60
	63
	74-79
	80-87
	103-127
	130-135
	155-157
	167-174
	
CHANNEL_SKIN_TAN
CHANNEL_SKIN_BRANDY_ROSE
CHANNEL_SKIN_KURUMIZOME_BROWN
CHANNEL_SKIN_PRUSSIAN_RED
CHANNEL_SKIN_TOAST
CHANNEL_SKIN_CORKBOARD
CHANNEL_SKIN_OLD_ROSE
CHANNEL_SKIN_CORAL_TREE
CHANNEL_SKIN_CLOUDY
CHANNEL_SKIN_TAPA
CHANNEL_SKIN_DOVE_GRAY
CHANNEL_SKIN_HEATHER
CHANNEL_SKIN_BALI_HAI
CHANNEL_SKIN_SLATE_GRAY
CHANNEL_SKIN_DEEP_PAYNES_GRAY
CHANNEL_SKIN_TAHUNA_SANDS
CHANNEL_SKIN_STRAW
CHANNEL_SKIN_ABBEY
CHANNEL_SKIN_VINTAGE
CHANNEL_SKIN_MEADOWLAND
CHANNEL_SKIN_SPRING_RAIN
CHANNEL_SKIN_BAY_LEAF
CHANNEL_SKIN_GLADE_GREEN
CHANNEL_SKIN_NOBEL_GRAY
CHANNEL_SKIN_DUSTY_GRAY
CHANNEL_SKIN_INDUSTRIAL
CHANNEL_SKIN_ROSE_VALE
CHANNEL_SKIN_CARMINE
CHANNEL_SKIN_MAGENTA
CHANNEL_SKIN_ASTER_PURPLE
CHANNEL_SKIN_DULL_BLUE_VIOLET
CHANNEL_SKIN_HELVETIA_BLUE
CHANNEL_SKIN_TYROLITE_GREEN
CHANNEL_SKIN_CAPRI_BLUE
CHANNEL_SKIN_PEACOCK_GREEN
CHANNEL_SKIN_COSSACK_GREEN
CHANNEL_SKIN_CALLISTE_GREEN
CHANNEL_SKIN_WARBLER_GREEN
CHANNEL_SKIN_SILVER
CHANNEL_SKIN_QUAKER_DRAB
CHANNEL_SKIN_MASSICOT_YELLOW
CHANNEL_SKIN_SEPIA
CHANNEL_SKIN_METALLIC_COPPER
CHANNEL_SKIN_VERDANT_HAVEN
CHANNEL_SKIN_WAKATAKE_GREEN
CHANNEL_SKIN_PINE
CHANNEL_SKIN_WOODLAND
CHANNEL_SKIN_PLYMOUTH_GREEN
CHANNEL_SKIN_VETIVER_GREEN
CHANNEL_SKIN_JADE_GREEN
CHANNEL_SKIN_RAW_UMBER
CHANNEL_SKIN_OLIVE_GRAY
CHANNEL_SKIN_LIMED_ASH
CHANNEL_SKIN_FUSCOUS_GRAY
CHANNEL_SKIN_TUNGSTEN
CHANNEL_SKIN_SORRELL_BROWN
CHANNEL_SKIN_SANDALWOOD
CHANNEL_SKIN_NATAL_BROWN
CHANNEL_SKIN_MOROCCO_BROWN
CHANNEL_SKIN_ROSYBROWN
CHANNEL_SKIN_YELLOWISH_OLIVE
CHANNEL_SKIN_MUMMY_BROWN
CHANNEL_SKIN_PALE_MOUSE_GRAY
CHANNEL_SKIN_LIGHT_QUAKER_DRAB
CHANNEL_SKIN_DEEP_QUAKER_DRAB
CHANNEL_SKIN_PALE_GREENISH_YELLOW
CHANNEL_SKIN_LIGHT_CRESS_GREEN
CHANNEL_SKIN_AVELLANEOUS
CHANNEL_SKIN_PLUMMY
CHANNEL_SKIN_BLACK_ORCHID
CHANNEL_SKIN_DARK_CITRINE
CHANNEL_SKIN_ROSY_SANDSTONE

	Hair Color Channels
	
0-3
7
13-15	
19
23
27-31
35
37-39
41
43
47
49
57
60
63
69-71
74-79
80-86
96-135
148-159
165-174
	
CHANNEL_HAIR_AMBER_BROWN
CHANNEL_HAIR_CHESTNUT
CHANNEL_HAIR_MAROON
CHANNEL_HAIR_CHOCOLATE
CHANNEL_HAIR_HEATH_BROWN
CHANNEL_HAIR_ARGUS_BROWN
CHANNEL_HAIR_BRACKEN
CHANNEL_HAIR_BROWN_BLACK
CHANNEL_HAIR_SLATE
CHANNEL_HAIR_DARK_SLATE_GRAY
CHANNEL_HAIR_PLUM_PURPLE
CHANNEL_HAIR_FORGET_ME_NOT_BLUE
CHANNEL_HAIR_PASSIONATE_BLUE
CHANNEL_HAIR_SILVER_CHALICE
CHANNEL_HAIR_CHARCOAL
CHANNEL_HAIR_DUSKY_SLATE_BLUE
CHANNEL_HAIR_VARISCITE_GREEN
CHANNEL_HAIR_DUSKY_DULL_GREEN
CHANNEL_HAIR_COURT_GRAY
CHANNEL_HAIR_DULL_BLACKISH_GREEN
CHANNEL_HAIR_OLIVINE
CHANNEL_HAIR_BROWNISH_GRAY
CHANNEL_HAIR_ANTIQUE_BRASS
CHANNEL_HAIR_DOVE_GRAY
CHANNEL_HAIR_DAVYS_GRAY
CHANNEL_HAIR_GLOSS_BLACK
CHANNEL_HAIR_INDIGO_BLUE
CHANNEL_HAIR_TITANIUM_GRAY
CHANNEL_HAIR_WARPLOCK_BRONZE
CHANNEL_HAIR_MARS_BROWN
CHANNEL_HAIR_DARK_PURPLE_DRAB
CHANNEL_HAIR_DARK_RUSSIAN_GREEN
CHANNEL_HAIR_DARK_NEUTRAL_GRAY
CHANNEL_HAIR_INDIGO_BLUE
CHANNEL_HAIR_TITANIUM_GRAY
CHANNEL_HAIR_WARPLOCK_BRONZE
CHANNEL_HAIR_CHAETURA_DRAB
CHANNEL_HAIR_DUSK_BLUE
CHANNEL_HAIR_GRAPHITE_GRAY
CHANNEL_HAIR_FUSCOUS_GRAY
CHANNEL_HAIR_BONE_BROWN
CHANNEL_HAIR_ESPRESSO
CHANNEL_HAIR_VANDYKE_BROWN
CHANNEL_HAIR_LIGHT_PINK
CHANNEL_HAIR_ANTIQUE_PINK
CHANNEL_HAIR_ACAJOU_RED
CHANNEL_HAIR_VIOLET_CARMINE
CHANNEL_HAIR_BEIGE_RED
CHANNEL_HAIR_PECAN_BROWN
CHANNEL_HAIR_POMPEIAN_RED
CHANNEL_HAIR_SADDLE_BROWN
CHANNEL_HAIR_FOLIAGE
CHANNEL_HAIR_MOSS
CHANNEL_HAIR_FOREST_GREEN
CHANNEL_HAIR_LEAF_GREEN
CHANNEL_HAIR_CORYDALIS_GREEN
CHANNEL_HAIR_TEA_GREEN
CHANNEL_HAIR_JADE_GREEN
CHANNEL_HAIR_WOODLAND
CHANNEL_HAIR_OLIVE_GRAY
CHANNEL_HAIR_MOUSE_GRAY
CHANNEL_HAIR_DARK_GRAYISH_BROWN
CHANNEL_HAIR_ANILINE_BLACK
CHANNEL_HAIR_TAN
CHANNEL_HAIR_TAUPE
CHANNEL_HAIR_NATAL_BROWN
CHANNEL_HAIR_MAHOGANY_BROWN
CHANNEL_HAIR_BEIGE
CHANNEL_HAIR_VETIVER_GREEN
CHANNEL_HAIR_GOLD_FUSION
CHANNEL_HAIR_DARK_OLIVE
CHANNEL_HAIR_LIGHT_SQUILL_BLUE
CHANNEL_HAIR_COLUMBIA_BLUE
CHANNEL_HAIR_GLAUCOUS_BLUE
CHANNEL_HAIR_DARK_CINNABAR_GREEN
CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN
CHANNEL_HAIR_WINTER_GREEN
CHANNEL_HAIR_KILDARE_GREEN
CHANNEL_HAIR_DULL_CITRINE
CHANNEL_HAIR_LIGHT_CRESS_GREEN
CHANNEL_HAIR_AVELLANEOUS
CHANNEL_HAIR_TAHINI_BROWN
CHANNEL_HAIR_DEEP_CORINTHIAN_RED
CHANNEL_HAIR_ARGYLE_PURPLE
CHANNEL_HAIR_LIGHT_PERILLA_PURPLE
CHANNEL_HAIR_PINKISH_VINACEOUS
CHANNEL_HAIR_MATHEWS_PURPLE
CHANNEL_HAIR_DARK_MADDER_BLUE
CHANNEL_HAIR_MATTE_WHITE
CHANNEL_HAIR_MATTE_BLACK
CHANNEL_HAIR_PALE_GULL_GRAY
CHANNEL_HAIR_NICKEL_GREEN
CHANNEL_HAIR_MADDER_VIOLET
CHANNEL_HAIR_DEEP_MADDER_BLUE
CHANNEL_HAIR_DARK_CITRINE
CHANNEL_HAIR_MAHOGANY
CHANNEL_HAIR_LIGHT_BROWNISH_OLIVE
	
*/	
	
//:: Checks for valid Earth Genasi skin color channel
int CheckEarthGenasiSkin(object oPC)
{
	//:: Get the skin color channel
    int nSkinColor 	= GetColorChannel(oPC, COLOR_CHANNEL_SKIN);
	
	//:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 3 && nSkinColor <= 7) ||
           (nSkinColor == 11) ||
           (nSkinColor >= 14 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 21 && nSkinColor <= 23) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor == 35) ||
           (nSkinColor >= 37 && nSkinColor <= 39) ||
           (nSkinColor >= 42 && nSkinColor <= 43) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor == 63) ||
           (nSkinColor >= 74 && nSkinColor <= 79) ||
           (nSkinColor >= 80 && nSkinColor <= 87) ||
           (nSkinColor >= 103 && nSkinColor <= 127) ||
           (nSkinColor >= 130 && nSkinColor <= 135) ||
           (nSkinColor >= 155 && nSkinColor <= 157) ||
           (nSkinColor >= 167 && nSkinColor <= 174);
}

//:: Checks for valid Earth Genasi hair color channel
int CheckEarthGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 0 && nHairColor <= 3) ||
           (nHairColor == 7) ||
           (nHairColor >= 13 && nHairColor <= 15) ||
           (nHairColor == 19) ||
           (nHairColor == 23) ||
           (nHairColor >= 27 && nHairColor <= 31) ||
           (nHairColor == 35) ||
           (nHairColor >= 37 && nHairColor <= 39) ||
           (nHairColor == 41) ||
           (nHairColor == 43) ||
           (nHairColor == 47) ||
           (nHairColor == 49) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor == 63) ||
           (nHairColor >= 69 && nHairColor <= 71) ||
           (nHairColor >= 74 && nHairColor <= 79) ||
           (nHairColor >= 80 && nHairColor <= 86) ||
           (nHairColor >= 96 && nHairColor <= 135) ||
           (nHairColor >= 148 && nHairColor <= 159) ||
           (nHairColor >= 165 && nHairColor <= 174);
}

// Function to randomly set oPC's skin color channel for Earth Genasi
void RndEarthGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(18);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum == 0) randomSkinColor = Random(5) + 3; // Range 3-7
    else if (randomNum == 1) randomSkinColor = 11; // Range 11
    else if (randomNum == 2) randomSkinColor = Random(2) + 14; // Range 14-15
    else if (randomNum == 3) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum == 4) randomSkinColor = Random(3) + 21; // Range 21-23
    else if (randomNum == 5) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum == 6) randomSkinColor = 35; // Range 35
    else if (randomNum == 7) randomSkinColor = Random(3) + 37; // Range 37-39
    else if (randomNum == 8) randomSkinColor = Random(2) + 42; // Range 42-43
    else if (randomNum == 9) randomSkinColor = 57; // Range 57
    else if (randomNum == 10) randomSkinColor = 60; // Range 60
    else if (randomNum == 11) randomSkinColor = 63; // Range 63
    else if (randomNum == 12) randomSkinColor = Random(6) + 74; // Range 74-79
    else if (randomNum == 13) randomSkinColor = Random(8) + 80; // Range 80-87
    else if (randomNum == 14) randomSkinColor = Random(25) + 103; // Range 103-127
    else if (randomNum == 15) randomSkinColor = Random(6) + 130; // Range 130-135
    else if (randomNum == 16) randomSkinColor = Random(3) + 155; // Range 155-157
    else if (randomNum == 17) randomSkinColor = Random(8) + 167; // Range 167-174

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Earth Genasi
void RndEarthGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(99);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 3) randomHairColor = Random(4); // Range 0-3
    else if (randomNum == 4) randomHairColor = 7;
    else if (randomNum >= 5 && randomNum <= 7) randomHairColor = Random(3) + 13; // Range 13-15
    else if (randomNum == 8) randomHairColor = 19;
    else if (randomNum == 9) randomHairColor = 23;
    else if (randomNum >= 10 && randomNum <= 14) randomHairColor = Random(5) + 27; // Range 27-31
    else if (randomNum == 15) randomHairColor = 35;
    else if (randomNum >= 16 && randomNum <= 18) randomHairColor = Random(3) + 37; // Range 37-39
    else if (randomNum == 19) randomHairColor = 41; 
    else if (randomNum == 20) randomHairColor = 43; 
    else if (randomNum == 21) randomHairColor = 47;
    else if (randomNum == 22) randomHairColor = 49;
    else if (randomNum == 23) randomHairColor = 57;
    else if (randomNum == 24) randomHairColor = 60;
    else if (randomNum == 25) randomHairColor = 63;
    else if (randomNum >= 26 && randomNum <= 28) randomHairColor = Random(3) + 69; // Range 69-71
    else if (randomNum >= 29 && randomNum <= 33) randomHairColor = Random(5) + 74; // Range 74-79
    else if (randomNum >= 34 && randomNum <= 40) randomHairColor = Random(7) + 80; // Range 80-86
    else if (randomNum >= 41 && randomNum <= 79) randomHairColor = Random(39) + 96; // Range 96-134
    else if (randomNum >= 80 && randomNum <= 99) randomHairColor = Random(20) + 148; // Range 148-167

    return randomHairColor;

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/*	Fire Genasi Colors

	Skin color channels
	4-7
	12-15
	18-19
	22-23
	27-31
	42-47
	57
	60
	63
	64-67
	73-75
	79
	81
	83-103
	112-119
	123-127
	131-135
	156-162
	167-171
	173
	
	CHANNEL_SKIN_KURUMIZOME_BROWN
	CHANNEL_SKIN_PRUSSIAN_RED
	CHANNEL_SKIN_TOAST
	CHANNEL_SKIN_RUSSET
	CHANNEL_SKIN_CLAMSHELL
	CHANNEL_SKIN_OLD_ROSE
	CHANNEL_SKIN_CORAL_TREE
	CHANNEL_SKIN_ALAEA
	CHANNEL_SKIN_TAPA
	CHANNEL_SKIN_DOVE_GRAY
	CHANNEL_SKIN_BALI_HAI
	CHANNEL_SKIN_SLATE_GRAY
	CHANNEL_SKIN_BURLAP
	CHANNEL_SKIN_SHUTTLE_GRAY
	CHANNEL_SKIN_COAL_MINE
	CHANNEL_SKIN_ABBEY
	CHANNEL_SKIN_MAKO
	CHANNEL_SKIN_MAGENTA
	CHANNEL_SKIN_ASTER_PURPLE
	CHANNEL_SKIN_DULL_BLUE_VIOLET
	CHANNEL_SKIN_HELVETIA_BLUE
	CHANNEL_SKIN_TYROLITE_GREEN
	CHANNEL_SKIN_CAPRI_BLUE
	CHANNEL_SKIN_QUAKER_DRAB
	CHANNEL_SKIN_DUSTY_GRAY
	CHANNEL_SKIN_WHITE
	CHANNEL_SKIN_CONGO_BROWN
	CHANNEL_SKIN_MARS_VIOLET
	CHANNEL_SKIN_OLD_COPPER
	CHANNEL_SKIN_DARK_OLIVE
	CHANNEL_SKIN_CAMEO_BROWN
	CHANNEL_SKIN_FLINT
	CHANNEL_SKIN_TERRA_VERTE
	CHANNEL_SKIN_SLATE_OLIVE
	CHANNEL_SKIN_SIAM_GREY
	CHANNEL_SKIN_BLUISH_VIOLET
	CHANNEL_SKIN_MID_GRAY
	CHANNEL_SKIN_DUSKY_DRAB
	CHANNEL_SKIN_GRAYISH_BROWN
	CHANNEL_SKIN_VINACEOUS_SLATE
	CHANNEL_SKIN_TOBACCO
	CHANNEL_SKIN_VERMILLION
	CHANNEL_SKIN_BURNT_UMBER
	CHANNEL_SKIN_CEDAR_STAFF
	CHANNEL_SKIN_SMOKED_PAPRIKA
	CHANNEL_SKIN_TUSSOCK
	CHANNEL_SKIN_LUXOR_GOLD
	CHANNEL_SKIN_PESTO
	CHANNEL_SKIN_BREEN
	CHANNEL_SKIN_CONTESSA
	CHANNEL_SKIN_CANDIED_APPLE
	CHANNEL_SKIN_UMEMURASAKI_PURPLE
	CHANNEL_SKIN_TAWNY_PORT
	CHANNEL_SKIN_ANTIQUE_BRASS
	CHANNEL_SKIN_SANDRIFT
	CHANNEL_SKIN_BUFF_BROWN
	CHANNEL_SKIN_SHINGLE_FAWN
	CHANNEL_SKIN_PLUMBEOUS
	CHANNEL_SKIN_DEEP_VARLEYS_GRAY
	CHANNEL_SKIN_SLATE_VIOLET
	CHANNEL_SKIN_SLATE_BLACK
	CHANNEL_SKIN_LIGHT_VIOLET_BLUE
	CHANNEL_SKIN_ULTRAMARINE_ASH
	CHANNEL_SKIN_KASHMIR_BLUE
	CHANNEL_SKIN_HORTENSE_BLUE
	CHANNEL_SKIN_CALAMINE_BLUE
	CHANNEL_SKIN_BERYL_GREEN
	CHANNEL_SKIN_SMALT_BLUE
	CHANNEL_SKIN_DUSKY_GREEN_BLUE
	CHANNEL_SKIN_ORCHID
	CHANNEL_SKIN_PHLOX_PURPLE
	CHANNEL_SKIN_VIOLET_PURPLE
	CHANNEL_SKIN_LIGHT_RUSSET
	CHANNEL_SKIN_MATHEWS_PURPLE
	CHANNEL_SKIN_MAUVETTE
	CHANNEL_SKIN_DARK_MADDER_BLUE
	CHANNEL_SKIN_MATTE_WHITE
	CHANNEL_SKIN_MATTE_BLACK
	CHANNEL_SKIN_LIGHT_MINERAL_GRAY
	CHANNEL_SKIN_DUSKY_BLUE_GREEN
	CHANNEL_SKIN_PLUMMY
	CHANNEL_SKIN_BLACK_ORCHID
	CHANNEL_SKIN_DARK_CITRINE
	CHANNEL_SKIN_ROSY_SANDSTONE
		
	
	Hair color channels
	
	1-7
	15
	23
	31
	47
	48-49
	51-55
	57
	60
	63
	64-65
	74-75
	79
	84-103
	112-115
	118-119
	123-127
	131-135
	155-159
	165
	167
	171
	173-175
	
	CHANNEL_HAIR_CHESTNUT
	CHANNEL_HAIR_MAROON
	CHANNEL_HAIR_CHOCOLATE
	CHANNEL_HAIR_SPECTRUM_RED
	CHANNEL_HAIR_CARMINE_RED
	CHANNEL_HAIR_CLARET_BROWN
	CHANNEL_HAIR_HEATH_BROWN
	CHANNEL_HAIR_BRACKEN
	CHANNEL_HAIR_DARK_SLATE_GRAY
	CHANNEL_HAIR_NILE_BLUE
	CHANNEL_HAIR_SILVER_CHALICE
	CHANNEL_HAIR_CHARCOAL
	CHANNEL_HAIR_NILE_BLUE
	CHANNEL_HAIR_ANTHRACENE_GREEN
	CHANNEL_HAIR_BLUISH_GLAUCOUS
	CHANNEL_HAIR_DUSKY_SLATE_BLUE
	CHANNEL_HAIR_VARISCITE_GREEN
	CHANNEL_HAIR_DUSKY_DULL_GREEN
	CHANNEL_HAIR_COURT_GRAY
	CHANNEL_HAIR_MIRROR
	CHANNEL_HAIR_GLOSS_BLACK
	CHANNEL_HAIR_NEUTRAL_RED
	CHANNEL_HAIR_DARK_MINERAL_RED
	CHANNEL_HAIR_MARS_BROWN
	CHANNEL_HAIR_DARK_PURPLE_DRAB
	CHANNEL_HAIR_INDIGO_BLUE
	CHANNEL_HAIR_GLAUCOUS_BLUE
	CHANNEL_HAIR_DARK_CINNABAR_GREEN
	CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN
	CHANNEL_HAIR_WINTER_GREEN
	CHANNEL_HAIR_KILDARE_GREEN
	CHANNEL_HAIR_DULL_CITRINE
	CHANNEL_HAIR_LIGHT_CRESS_GREEN
	CHANNEL_HAIR_AVELLANEOUS
	CHANNEL_HAIR_TAHINI_BROWN
	CHANNEL_HAIR_DEEP_CORINTHIAN_RED
	CHANNEL_HAIR_ARGYLE_PURPLE
	CHANNEL_HAIR_LIGHT_PERILLA_PURPLE
	CHANNEL_HAIR_PINKISH_VINACEOUS
	CHANNEL_HAIR_MATHEWS_PURPLE
	CHANNEL_HAIR_MAUVETTE
	CHANNEL_HAIR_DARK_MADDER_BLUE
	CHANNEL_HAIR_MATTE_WHITE
	CHANNEL_HAIR_MATTE_BLACK
	CHANNEL_HAIR_OLIVE_GRAY
	CHANNEL_HAIR_MOUSE_GRAY
	CHANNEL_HAIR_DARK_GRAYISH_BROWN
	CHANNEL_HAIR_ANILINE_BLACK
	CHANNEL_HAIR_TAN
	CHANNEL_HAIR_TAUPE
	CHANNEL_HAIR_PALE_MOUSE_GRAY
	CHANNEL_HAIR_LIGHT_QUAKER_DRAB
	CHANNEL_HAIR_DARK_VINACEOUS_DRAB
	CHANNEL_HAIR_MASALA_BROWN
	CHANNEL_HAIR_PALE_OLIVE_BUFF
	CHANNEL_HAIR_SAGE
	CHANNEL_HAIR_BEAVER
	CHANNEL_HAIR_COFFEE
	CHANNEL_HAIR_PLUMBEOUS
	CHANNEL_HAIR_VARLEYS_GRAY
	CHANNEL_HAIR_DARK_SLATE_VIOLET
	CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN
	CHANNEL_HAIR_WINTER_GREEN
	CHANNEL_HAIR_PALE_GREENISH_YELLOW
	CHANNEL_HAIR_LIGHT_CRESS_GREEN
	CHANNEL_HAIR_MATTE_WHITE
	CHANNEL_HAIR_LIGHT_MINERAL_GRAY
	CHANNEL_HAIR_BLACK_ORCHID
	CHANNEL_HAIR_ROSY_SANDSTONE
	CHANNEL_HAIR_MOCHA
	CHANNEL_HAIR_GOLD
	
*/

//:: Checks for valid Fire Genasi skin color channel
int CheckFireGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 4 && nSkinColor <= 7) ||
           (nSkinColor >= 12 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 22 && nSkinColor <= 23) ||
           (nSkinColor >= 27 && nSkinColor <= 31) ||
           (nSkinColor >= 42 && nSkinColor <= 47) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor == 63) ||
           (nSkinColor >= 64 && nSkinColor <= 67) ||
           (nSkinColor >= 73 && nSkinColor <= 75) ||
           (nSkinColor == 79) ||
           (nSkinColor == 81) ||
           (nSkinColor >= 83 && nSkinColor <= 103) ||
           (nSkinColor >= 112 && nSkinColor <= 119) ||
           (nSkinColor >= 123 && nSkinColor <= 127) ||
           (nSkinColor >= 131 && nSkinColor <= 135) ||
           (nSkinColor >= 156 && nSkinColor <= 162) ||
           (nSkinColor >= 167 && nSkinColor <= 171) ||
           (nSkinColor == 173);
}

//:: Checks for valid Fire Genasi hair color channel
int CheckFireGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 1 && nHairColor <= 7) ||
           (nHairColor == 15) ||
           (nHairColor == 23) ||
           (nHairColor == 31) ||
           (nHairColor == 47) ||
           (nHairColor >= 48 && nHairColor <= 49) ||
           (nHairColor >= 51 && nHairColor <= 55) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor == 63) ||
           (nHairColor >= 64 && nHairColor <= 65) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor == 79) ||
           (nHairColor >= 84 && nHairColor <= 103) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 118 && nHairColor <= 119) ||
           (nHairColor >= 123 && nHairColor <= 127) ||
           (nHairColor >= 131 && nHairColor <= 135) ||
           (nHairColor >= 155 && nHairColor <= 159) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor == 171) ||
           (nHairColor >= 173 && nHairColor <= 175);
}

// Function to randomly set oPC's skin color channel for Fire Genasi
void RndFireGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(87);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 3) randomSkinColor = Random(4) + 4; // Range 4-7
    else if (randomNum >= 4 && randomNum <= 7) randomSkinColor = Random(4) + 12; // Range 12-15
    else if (randomNum >= 8 && randomNum <= 9) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum >= 10 && randomNum <= 11) randomSkinColor = Random(2) + 22; // Range 22-23
    else if (randomNum >= 12 && randomNum <= 16) randomSkinColor = Random(5) + 27; // Range 27-31
    else if (randomNum >= 17 && randomNum <= 22) randomSkinColor = Random(6) + 42; // Range 42-47
    else if (randomNum == 23) randomSkinColor = 57;
    else if (randomNum == 24) randomSkinColor = 60;
    else if (randomNum == 25) randomSkinColor = 63;
    else if (randomNum >= 26 && randomNum <= 29) randomSkinColor = Random(4) + 64; // Range 64-67
    else if (randomNum >= 30 && randomNum <= 32) randomSkinColor = Random(3) + 73; // Range 73-75
    else if (randomNum == 33) randomSkinColor = 79;
    else if (randomNum == 34) randomSkinColor = 81;
    else if (randomNum >= 35 && randomNum <= 55) randomSkinColor = Random(21) + 83; // Range 83-103
    else if (randomNum >= 56 && randomNum <= 63) randomSkinColor = Random(8) + 112; // Range 112-119
    else if (randomNum >= 64 && randomNum <= 68) randomSkinColor = Random(5) + 123; // Range 123-127
    else if (randomNum >= 69 && randomNum <= 73) randomSkinColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 74 && randomNum <= 80) randomSkinColor = Random(7) + 156; // Range 156-162
    else if (randomNum >= 81 && randomNum <= 85) randomSkinColor = Random(5) + 167; // Range 167-171
    else if (randomNum == 86) randomSkinColor = 173;

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Fire Genasi
void RndFireGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(73);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 6) randomHairColor = Random(7) + 1; // Range 1-7
    else if (randomNum == 7) randomHairColor = 15;
    else if (randomNum == 8) randomHairColor = 23;
    else if (randomNum == 9) randomHairColor = 31;
    else if (randomNum == 10) randomHairColor = 47;
    else if (randomNum >= 11 && randomNum <= 12) randomHairColor = Random(2) + 48; // Range 48-49
    else if (randomNum >= 13 && randomNum <= 17) randomHairColor = Random(5) + 51; // Range 51-55
    else if (randomNum == 18) randomHairColor = 57;
    else if (randomNum == 19) randomHairColor = 60;
    else if (randomNum == 20) randomHairColor = 63;
    else if (randomNum >= 21 && randomNum <= 22) randomHairColor = Random(2) + 64; // Range 64-65
    else if (randomNum >= 23 && randomNum <= 24) randomHairColor = Random(2) + 74; // Range 74-75
    else if (randomNum == 25) randomHairColor = 79;
    else if (randomNum >= 26 && randomNum <= 45) randomHairColor = Random(20) + 84; // Range 84-103
    else if (randomNum >= 46 && randomNum <= 49) randomHairColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 50 && randomNum <= 51) randomHairColor = Random(2) + 118; // Range 118-119
    else if (randomNum >= 52 && randomNum <= 56) randomHairColor = Random(5) + 123; // Range 123-127
    else if (randomNum >= 57 && randomNum <= 61) randomHairColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 62 && randomNum <= 66) randomHairColor = Random(5) + 155; // Range 155-159
    else if (randomNum == 67) randomHairColor = 165;
    else if (randomNum == 68) randomHairColor = 167;
    else if (randomNum == 69) randomHairColor = 171;
    else if (randomNum >= 70 && randomNum <= 72) randomHairColor = Random(3) + 173; // Range 173-175

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/*	Water Genasi Colors

	Skin color channels
	16-23
	28-31
	32-43
	48-53
	60
	69-86
	104-114
	124-127
	132-143
	148-153
	165
	169-171
	
	CHANNEL_SKIN_COTTON_SEED
	CHANNEL_SKIN_CLOUDY
	CHANNEL_SKIN_TAPA
	CHANNEL_SKIN_DOVE_GRAY
	CHANNEL_SKIN_HEATHER
	CHANNEL_SKIN_BALI_HAI
	CHANNEL_SKIN_SLATE_GRAY
	CHANNEL_SKIN_DEEP_PAYNES_GRAY
	CHANNEL_SKIN_SHUTTLE_GRAY
	CHANNEL_SKIN_COAL_MINE
	CHANNEL_SKIN_ABBEY
	CHANNEL_SKIN_MAKO
	CHANNEL_SKIN_THISTLE_GREEN
	CHANNEL_SKIN_OLIVINE
	CHANNEL_SKIN_VINTAGE
	CHANNEL_SKIN_MEADOWLAND
	CHANNEL_SKIN_SPRING_RAIN
	CHANNEL_SKIN_BAY_LEAF
	CHANNEL_SKIN_GLADE_GREEN
	CHANNEL_SKIN_CACTUS
	CHANNEL_SKIN_NOBEL_GRAY
	CHANNEL_SKIN_DUSTY_GRAY
	CHANNEL_SKIN_INDUSTRIAL
	CHANNEL_SKIN_RHINE_CASTLE
	CHANNEL_SKIN_ROSE_VALE
	CHANNEL_SKIN_CARMINE
	CHANNEL_SKIN_MAGENTA
	CHANNEL_SKIN_ASTER_PURPLE
	CHANNEL_SKIN_DULL_BLUE_VIOLET
	CHANNEL_SKIN_HELVETIA_BLUE
	CHANNEL_SKIN_WHITE
	CHANNEL_SKIN_CHALET_GREEN
	CHANNEL_SKIN_DARK_GRAY_OLIVE
	CHANNEL_SKIN_DARK_VIOLET
	CHANNEL_SKIN_DARK_SLATE
	CHANNEL_SKIN_DARK_PURPLE
	CHANNEL_SKIN_OLD_LAVENDER
	CHANNEL_SKIN_CAMEO_BROWN
	CHANNEL_SKIN_FLINT
	CHANNEL_SKIN_TERRA_VERTE
	CHANNEL_SKIN_SLATE_OLIVE
	CHANNEL_SKIN_BAYOUX_BLUE
	CHANNEL_SKIN_VIOLET_SLATE
	CHANNEL_SKIN_VERDIGRIS
	CHANNEL_SKIN_SIAM_GREY
	CHANNEL_SKIN_BLUISH_VIOLET
	CHANNEL_SKIN_MID_GRAY
	CHANNEL_SKIN_DUSKY_DRAB
	CHANNEL_SKIN_GRAYISH_BROWN
	CHANNEL_SKIN_VINACEOUS_SLATE
	CHANNEL_SKIN_TOBACCO
	CHANNEL_SKIN_VERMILLION
	CHANNEL_SKIN_BURNT_UMBER
	CHANNEL_SKIN_CEDAR_STAFF
	CHANNEL_SKIN_SMOKED_PAPRIKA
	CHANNEL_SKIN_TUSSOCK
	CHANNEL_SKIN_LUXOR_GOLD
	CHANNEL_SKIN_PESTO
	CHANNEL_SKIN_BREEN
	CHANNEL_SKIN_CONTESSA
	CHANNEL_SKIN_CANDIED_APPLE
	CHANNEL_SKIN_UMEMURASAKI_PURPLE
	CHANNEL_SKIN_TAWNY_PORT
	CHANNEL_SKIN_ANTIQUE_BRASS
	CHANNEL_SKIN_SANTA_FE
	CHANNEL_SKIN_SEPIA
	CHANNEL_SKIN_METALLIC_COPPER
	CHANNEL_SKIN_RAW_UMBER
	CHANNEL_SKIN_OLIVE_GRAY
	CHANNEL_SKIN_LIMED_ASH
	CHANNEL_SKIN_FUSCOUS_GRAY
	CHANNEL_SKIN_TUNGSTEN
	CHANNEL_SKIN_SORRELL_BROWN
	CHANNEL_SKIN_SANDALWOOD
	CHANNEL_SKIN_NATAL_BROWN
	CHANNEL_SKIN_MOROCCO_BROWN
	CHANNEL_SKIN_INDIAN_KHAKI
	CHANNEL_SKIN_ROSYBROWN
	CHANNEL_SKIN_YELLOWISH_OLIVE
	CHANNEL_SKIN_MUMMY_BROWN
	CHANNEL_SKIN_PALE_MOUSE_GRAY
	CHANNEL_SKIN_LIGHT_QUAKER_DRAB
	CHANNEL_SKIN_DEEP_QUAKER_DRAB
	CHANNEL_SKIN_TAUPE_BROWN
	CHANNEL_SKIN_VANILLA
	CHANNEL_SKIN_SANDRIFT
	CHANNEL_SKIN_BUFF_BROWN
	CHANNEL_SKIN_SHINGLE_FAWN
	CHANNEL_SKIN_PLUMBEOUS
	CHANNEL_SKIN_DEEP_VARLEYS_GRAY
	CHANNEL_SKIN_SLATE_VIOLET
	CHANNEL_SKIN_SLATE_BLACK
	CHANNEL_SKIN_LIGHT_VIOLET_BLUE
	CHANNEL_SKIN_ULTRAMARINE_ASH
	CHANNEL_SKIN_KASHMIR_BLUE
	CHANNEL_SKIN_HORTENSE_BLUE
	CHANNEL_SKIN_CALAMINE_BLUE
	CHANNEL_SKIN_BERYL_GREEN
	CHANNEL_SKIN_SMALT_BLUE
	CHANNEL_SKIN_DUSKY_GREEN_BLUE
	CHANNEL_SKIN_ORCHID
	CHANNEL_SKIN_PHLOX_PURPLE
	CHANNEL_SKIN_VIOLET_PURPLE
	CHANNEL_SKIN_HYACINTH_VIOLET
	CHANNEL_SKIN_LIGHT_NEROPALIN_BLUE
	CHANNEL_SKIN_COLUMBIA_BLUE
	CHANNEL_SKIN_GLAUCOUS_BLUE
	CHANNEL_SKIN_DARK_CINNABAR_GREEN
	CHANNEL_SKIN_LIGHT_ORIENTAL_GREEN
	CHANNEL_SKIN_WINTER_GREEN
	CHANNEL_SKIN_PALE_GREENISH_YELLOW
	CHANNEL_SKIN_MATTE_WHITE
	CHANNEL_SKIN_LIGHT_MINERAL_GRAY
	CHANNEL_SKIN_DUSKY_BLUE_GREEN
	CHANNEL_SKIN_PLUMMY
	
	
	
	Hair color channels
	
	16-47
	67-86
	104-115
	121-127
	132-143
	148-153
	165
	167
	169-171
	173

	CHANNEL_HAIR_BRANDY_ROSE
	CHANNEL_HAIR_KURUMIZOME_BROWN
	CHANNEL_HAIR_PRUSSIAN_RED
	CHANNEL_HAIR_TOAST
	CHANNEL_HAIR_RUSSET
	CHANNEL_HAIR_AKAROA
	CHANNEL_HAIR_ECRU
	CHANNEL_HAIR_CORKBOARD
	CHANNEL_HAIR_CLOUDY_CINNAMON
	CHANNEL_HAIR_CLAMSHELL
	CHANNEL_HAIR_OLD_ROSE
	CHANNEL_HAIR_CORAL_TREE
	CHANNEL_HAIR_ALAEA
	CHANNEL_HAIR_COTTON_SEED
	CHANNEL_HAIR_CLOUDY
	CHANNEL_HAIR_TAPA
	CHANNEL_HAIR_DOVE_GRAY
	CHANNEL_HAIR_HEATHER
	CHANNEL_HAIR_BALI_HAI
	CHANNEL_HAIR_SLATE_GRAY
	CHANNEL_HAIR_DEEP_PAYNES_GRAY
	CHANNEL_HAIR_TAHUNA_SANDS
	CHANNEL_HAIR_STRAW
	CHANNEL_HAIR_DONKEY_BROWN
	CHANNEL_HAIR_BURLAP
	CHANNEL_HAIR_SHUTTLE_GRAY
	CHANNEL_HAIR_COAL_MINE
	CHANNEL_HAIR_ABBEY
	CHANNEL_HAIR_MAKO
	CHANNEL_HAIR_THISTLE_GREEN
	CHANNEL_HAIR_OLIVINE
	CHANNEL_HAIR_VINTAGE
	CHANNEL_HAIR_MEADOWLAND
	CHANNEL_HAIR_SPRING_RAIN
	CHANNEL_HAIR_BAY_LEAF
	CHANNEL_HAIR_GLADE_GREEN
	CHANNEL_HAIR_CACTUS
	CHANNEL_HAIR_NOBEL_GRAY
	CHANNEL_HAIR_DUSTY_GRAY
	CHANNEL_HAIR_INDUSTRIAL
	CHANNEL_HAIR_RHINE_CASTLE
	CHANNEL_HAIR_ROSE_VALE
	CHANNEL_HAIR_CARMINE
	CHANNEL_HAIR_MAGENTA
	CHANNEL_HAIR_ASTER_PURPLE
	CHANNEL_HAIR_DULL_BLUE_VIOLET
	CHANNEL_HAIR_HELVETIA_BLUE
	CHANNEL_HAIR_TYROLITE_GREEN
	CHANNEL_HAIR_CAPRI_BLUE
	CHANNEL_HAIR_PEACOCK_GREEN
	CHANNEL_HAIR_COSSACK_GREEN
	CHANNEL_HAIR_CALLISTE_GREEN
	CHANNEL_HAIR_WARBLER_GREEN
	CHANNEL_HAIR_SILVER
	CHANNEL_HAIR_QUAKER_DRAB
	CHANNEL_HAIR_MASSICOT_YELLOW
	CHANNEL_HAIR_WAFER
	CHANNEL_HAIR_TUNDORA
	CHANNEL_HAIR_AZO_BLUE
	CHANNEL_HAIR_WHITE
	CHANNEL_HAIR_BLACK
	CHANNEL_HAIR_CONGO_BROWN
	CHANNEL_HAIR_MARS_VIOLET
	CHANNEL_HAIR_OLD_COPPER
	CHANNEL_HAIR_DARK_OLIVE
	CHANNEL_HAIR_CHALET_GREEN
	CHANNEL_HAIR_DARK_GRAY_OLIVE
	CHANNEL_HAIR_DARK_VIOLET
	CHANNEL_HAIR_DARK_SLATE
	CHANNEL_HAIR_DARK_PURPLE
	CHANNEL_HAIR_OLD_LAVENDER
	CHANNEL_HAIR_CAMEO_BROWN
	CHANNEL_HAIR_FLINT
	CHANNEL_HAIR_TERRA_VERTE
	CHANNEL_HAIR_SLATE_OLIVE
	CHANNEL_HAIR_BAYOUX_BLUE
	CHANNEL_HAIR_VIOLET_SLATE
	CHANNEL_HAIR_VERDIGRIS
	CHANNEL_HAIR_SIAM_GREY
	CHANNEL_HAIR_BLUISH_VIOLET
	CHANNEL_HAIR_MID_GRAY
	CHANNEL_HAIR_DUSKY_DRAB
	CHANNEL_HAIR_GRAYISH_BROWN
	CHANNEL_HAIR_VINACEOUS_SLATE
	CHANNEL_HAIR_TOBACCO
	CHANNEL_HAIR_VERMILLION
	CHANNEL_HAIR_BURNT_UMBER
	CHANNEL_HAIR_CEDAR_STAFF
	CHANNEL_HAIR_SMOKED_PAPRIKA
	CHANNEL_HAIR_TUSSOCK
	CHANNEL_HAIR_LUXOR_GOLD
	CHANNEL_HAIR_PESTO
	CHANNEL_HAIR_BREEN
	CHANNEL_HAIR_CONTESSA
	CHANNEL_HAIR_CANDIED_APPLE
	CHANNEL_HAIR_UMEMURASAKI_PURPLE
	CHANNEL_HAIR_TAWNY_PORT
	CHANNEL_HAIR_ANTIQUE_BRASS
	CHANNEL_HAIR_SANTA_FE
	CHANNEL_HAIR_SEPIA
	CHANNEL_HAIR_METALLIC_COPPER
	CHANNEL_HAIR_VERDANT_HAVEN
	CHANNEL_HAIR_WAKATAKE_GREEN
	CHANNEL_HAIR_PINE
	CHANNEL_HAIR_WOODLAND
	CHANNEL_HAIR_PLYMOUTH_GREEN
	CHANNEL_HAIR_VETIVER_GREEN
	CHANNEL_HAIR_JADE_GREEN
	CHANNEL_HAIR_RAW_UMBER
	CHANNEL_HAIR_OLIVE_GRAY
	CHANNEL_HAIR_LIMED_ASH
	CHANNEL_HAIR_FUSCOUS_GRAY
	CHANNEL_HAIR_TUNGSTEN
	CHANNEL_HAIR_SORRELL_BROWN
	CHANNEL_HAIR_SANDALWOOD
	CHANNEL_HAIR_NATAL_BROWN
	CHANNEL_HAIR_MOROCCO_BROWN
	CHANNEL_HAIR_INDIAN_KHAKI
	CHANNEL_HAIR_ROSYBROWN
	CHANNEL_HAIR_YELLOWISH_OLIVE
	CHANNEL_HAIR_MUMMY_BROWN
	CHANNEL_HAIR_PALE_MOUSE_GRAY
	CHANNEL_HAIR_LIGHT_QUAKER_DRAB
	CHANNEL_HAIR_DEEP_QUAKER_DRAB
	CHANNEL_HAIR_TAUPE_BROWN
	CHANNEL_HAIR_VANILLA
	CHANNEL_HAIR_SANDRIFT
	CHANNEL_HAIR_BUFF_BROWN
	CHANNEL_HAIR_SHINGLE_FAWN
	CHANNEL_HAIR_PLUMBEOUS
	CHANNEL_HAIR_DEEP_VARLEYS_GRAY
	CHANNEL_HAIR_SLATE_VIOLET
	CHANNEL_HAIR_SLATE_BLACK
	CHANNEL_HAIR_LIGHT_VIOLET_BLUE
	CHANNEL_HAIR_ULTRAMARINE_ASH
	CHANNEL_HAIR_KASHMIR_BLUE
	CHANNEL_HAIR_HORTENSE_BLUE
	CHANNEL_HAIR_CALAMINE_BLUE
	CHANNEL_HAIR_BERYL_GREEN
	CHANNEL_HAIR_SMALT_BLUE
	CHANNEL_HAIR_DUSKY_GREEN_BLUE
	CHANNEL_HAIR_ORCHID
	CHANNEL_HAIR_PHLOX_PURPLE
	CHANNEL_HAIR_VIOLET_PURPLE
	CHANNEL_HAIR_HYACINTH_VIOLET
	CHANNEL_HAIR_LIGHT_NEROPALIN_BLUE
	CHANNEL_HAIR_COLUMBIA_BLUE
	CHANNEL_HAIR_GLAUCOUS_BLUE
	CHANNEL_HAIR_DARK_CINNABAR_GREEN
	CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN
	CHANNEL_HAIR_WINTER_GREEN
	CHANNEL_HAIR_PALE_GREENISH_YELLOW
	CHANNEL_HAIR_MATTE_WHITE
	CHANNEL_HAIR_LIGHT_MINERAL_GRAY
	CHANNEL_HAIR_DUSKY_BLUE_GREEN
	CHANNEL_HAIR_PLUMMY	
	
	

*/

//:: Checks for valid Water Genasi hair color channel
int CheckWaterGenasiSkin(object oPC)
{
    //:: Get the hair color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the Skin color channel is within any of the specified ranges
    return (nSkinColor >= 16 && nSkinColor <= 23) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor >= 32 && nSkinColor <= 43) ||
           (nSkinColor >= 48 && nSkinColor <= 53) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 69 && nSkinColor <= 86) ||
           (nSkinColor >= 104 && nSkinColor <= 114) ||
           (nSkinColor >= 124 && nSkinColor <= 127) ||
           (nSkinColor >= 132 && nSkinColor <= 143) ||
           (nSkinColor >= 148 && nSkinColor <= 153) ||
           (nSkinColor == 165) ||
           (nSkinColor >= 169 && nSkinColor <= 171);
}

//:: Checks for valid Water Genasi hair color channel
int CheckWaterGenasiHair(object oPC)
{
    //:: Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 47) ||
           (nHairColor >= 67 && nHairColor <= 86) ||
           (nHairColor >= 104 && nHairColor <= 115) ||
           (nHairColor >= 121 && nHairColor <= 127) ||
           (nHairColor >= 132 && nHairColor <= 143) ||
           (nHairColor >= 148 && nHairColor <= 153) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor >= 169 && nHairColor <= 171) ||
           (nHairColor == 173);
}

// Function to randomly set oPC's skin color channel for Water Genasi
void RndWaterGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(86);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(8) + 16; // Range 16-23
    else if (randomNum >= 8 && randomNum <= 11) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum >= 12 && randomNum <= 23) randomSkinColor = Random(12) + 32; // Range 32-43
    else if (randomNum >= 24 && randomNum <= 29) randomSkinColor = Random(6) + 48; // Range 48-53
    else if (randomNum == 30) randomSkinColor = 60;
    else if (randomNum >= 31 && randomNum <= 48) randomSkinColor = Random(18) + 69; // Range 69-86
    else if (randomNum >= 49 && randomNum <= 59) randomSkinColor = Random(11) + 104; // Range 104-114
    else if (randomNum >= 60 && randomNum <= 63) randomSkinColor = Random(4) + 124; // Range 124-127
    else if (randomNum >= 64 && randomNum <= 75) randomSkinColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 76 && randomNum <= 81) randomSkinColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 82) randomSkinColor = 165;
    else if (randomNum >= 83 && randomNum <= 85) randomSkinColor = Random(3) + 169; // Range 169-171    

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Water Genasi
void RndWaterGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(95);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 31) randomHairColor = Random(32) + 16; // Range 16-47
    else if (randomNum >= 32 && randomNum <= 51) randomHairColor = Random(20) + 67; // Range 67-86
    else if (randomNum >= 52 && randomNum <= 63) randomHairColor = Random(12) + 104; // Range 104-115
    else if (randomNum >= 64 && randomNum <= 70) randomHairColor = Random(7) + 121; // Range 121-127
    else if (randomNum >= 71 && randomNum <= 82) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 83 && randomNum <= 88) randomHairColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 89) randomHairColor = 165;
    else if (randomNum == 90) randomHairColor = 167;
    else if (randomNum >= 91 && randomNum <= 93) randomHairColor = Random(3) + 169; // Range 169-171
    else if (randomNum == 94) randomHairColor = 173;

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/* 	Magma Genasi Colors

	Skin color channels	
	7
	13-15
	18-19
	28-31
	41-45
	57
	60
	63-65
	87-91
	96-103
	112-115
	118-119
	122-127
	131-135
	158-159
	165
	167
	171
	173
	
	CHANNEL_SKIN_TAN
	CHANNEL_SKIN_BRANDY_ROSE
	CHANNEL_SKIN_KURUMIZOME_BROWN
	CHANNEL_SKIN_TOAST
	CHANNEL_SKIN_RUSSET
	CHANNEL_SKIN_AKAROA
	CHANNEL_SKIN_ECRU
	CHANNEL_SKIN_CORKBOARD
	CHANNEL_SKIN_CLOUDY_CINNAMON
	CHANNEL_SKIN_CLAMSHELL
	CHANNEL_SKIN_OLD_ROSE
	CHANNEL_SKIN_CORAL_TREE
	CHANNEL_SKIN_ALAEA
	CHANNEL_SKIN_COTTON_SEED
	CHANNEL_SKIN_CLOUDY
	CHANNEL_SKIN_TAPA
	CHANNEL_SKIN_DOVE_GRAY
	CHANNEL_SKIN_HEATHER
	CHANNEL_SKIN_BALI_HAI
	CHANNEL_SKIN_SLATE_GRAY
	CHANNEL_SKIN_DEEP_PAYNES_GRAY
	CHANNEL_SKIN_TAHUNA_SANDS
	CHANNEL_SKIN_STRAW
	CHANNEL_SKIN_DONKEY_BROWN
	CHANNEL_SKIN_BURLAP
	CHANNEL_SKIN_SHUTTLE_GRAY
	CHANNEL_SKIN_COAL_MINE
	CHANNEL_SKIN_ABBEY
	CHANNEL_SKIN_MAKO
	CHANNEL_SKIN_THISTLE_GREEN
	CHANNEL_SKIN_OLIVINE
	CHANNEL_SKIN_VINTAGE
	CHANNEL_SKIN_MEADOWLAND
	CHANNEL_SKIN_SPRING_RAIN
	CHANNEL_SKIN_BAY_LEAF
	CHANNEL_SKIN_GLADE_GREEN
	CHANNEL_SKIN_CACTUS
	CHANNEL_SKIN_NOBEL_GRAY
	CHANNEL_SKIN_DUSTY_GRAY
	CHANNEL_SKIN_INDUSTRIAL
	CHANNEL_SKIN_RHINE_CASTLE
	CHANNEL_SKIN_ROSE_VALE
	CHANNEL_SKIN_CARMINE
	CHANNEL_SKIN_MAGENTA
	CHANNEL_SKIN_ASTER_PURPLE
	CHANNEL_SKIN_DULL_BLUE_VIOLET
	CHANNEL_SKIN_HELVETIA_BLUE
	CHANNEL_SKIN_TYROLITE_GREEN
	CHANNEL_SKIN_CAPRI_BLUE
	CHANNEL_SKIN_PEACOCK_GREEN
	CHANNEL_SKIN_COSSACK_GREEN
	CHANNEL_SKIN_CALLISTE_GREEN
	CHANNEL_SKIN_WARBLER_GREEN
	CHANNEL_SKIN_SILVER
	CHANNEL_SKIN_QUAKER_DRAB
	CHANNEL_SKIN_MASSICOT_YELLOW
	CHANNEL_SKIN_WAFER
	CHANNEL_SKIN_TUNDORA
	CHANNEL_SKIN_AZO_BLUE
	CHANNEL_SKIN_WHITE
	CHANNEL_SKIN_BLACK
	CHANNEL_SKIN_CONGO_BROWN
	CHANNEL_SKIN_MARS_VIOLET
	CHANNEL_SKIN_OLD_COPPER
	CHANNEL_SKIN_DARK_OLIVE
	CHANNEL_SKIN_CHALET_GREEN
	CHANNEL_SKIN_DARK_GRAY_OLIVE
	CHANNEL_SKIN_DARK_VIOLET
	CHANNEL_SKIN_DARK_SLATE
	CHANNEL_SKIN_DARK_PURPLE
	CHANNEL_SKIN_OLD_LAVENDER
	CHANNEL_SKIN_CAMEO_BROWN
	CHANNEL_SKIN_FLINT
	CHANNEL_SKIN_TERRA_VERTE
	CHANNEL_SKIN_SLATE_OLIVE
	CHANNEL_SKIN_BAYOUX_BLUE
	CHANNEL_SKIN_VIOLET_SLATE
	CHANNEL_SKIN_VERDIGRIS
	CHANNEL_SKIN_SIAM_GREY
	CHANNEL_SKIN_BLUISH_VIOLET
	CHANNEL_SKIN_MID_GRAY
	CHANNEL_SKIN_DUSKY_DRAB
	CHANNEL_SKIN_GRAYISH_BROWN
	CHANNEL_SKIN_VINACEOUS_SLATE
	CHANNEL_SKIN_TOBACCO
	CHANNEL_SKIN_VERMILLION
	CHANNEL_SKIN_BURNT_UMBER
	CHANNEL_SKIN_CEDAR_STAFF
	CHANNEL_SKIN_SMOKED_PAPRIKA
	CHANNEL_SKIN_TUSSOCK
	CHANNEL_SKIN_LUXOR_GOLD
	CHANNEL_SKIN_PESTO
	CHANNEL_SKIN_BREEN
	CHANNEL_SKIN_CONTESSA
	CHANNEL_SKIN_CANDIED_APPLE
	CHANNEL_SKIN_UMEMURASAKI_PURPLE
	CHANNEL_SKIN_TAWNY_PORT
	CHANNEL_SKIN_ANTIQUE_BRASS
	CHANNEL_SKIN_SANTA_FE
	CHANNEL_SKIN_SEPIA
	CHANNEL_SKIN_METALLIC_COPPER
	CHANNEL_SKIN_VERDANT_HAVEN
	CHANNEL_SKIN_WAKATAKE_GREEN
	CHANNEL_SKIN_PINE
	CHANNEL_SKIN_WOODLAND
	CHANNEL_SKIN_PLYMOUTH_GREEN
	CHANNEL_SKIN_VETIVER_GREEN
	CHANNEL_SKIN_JADE_GREEN
	CHANNEL_SKIN_RAW_UMBER
	CHANNEL_SKIN_OLIVE_GRAY
	CHANNEL_SKIN_LIMED_ASH
	CHANNEL_SKIN_FUSCOUS_GRAY
	CHANNEL_SKIN_TUNGSTEN
	CHANNEL_SKIN_SORRELL_BROWN
	CHANNEL_SKIN_SANDALWOOD
	CHANNEL_SKIN_NATAL_BROWN
	CHANNEL_SKIN_MOROCCO_BROWN
	CHANNEL_SKIN_INDIAN_KHAKI
	CHANNEL_SKIN_ROSYBROWN
	CHANNEL_SKIN_YELLOWISH_OLIVE
	CHANNEL_SKIN_MUMMY_BROWN
	CHANNEL_SKIN_PALE_MOUSE_GRAY
	CHANNEL_SKIN_LIGHT_QUAKER_DRAB
	CHANNEL_SKIN_DEEP_QUAKER_DRAB
	CHANNEL_SKIN_TAUPE_BROWN
	CHANNEL_SKIN_VANILLA
	CHANNEL_SKIN_SANDRIFT
	CHANNEL_SKIN_BUFF_BROWN
	CHANNEL_SKIN_SHINGLE_FAWN
	CHANNEL_SKIN_PLUMBEOUS
	CHANNEL_SKIN_DEEP_VARLEYS_GRAY
	CHANNEL_SKIN_SLATE_VIOLET
	CHANNEL_SKIN_LIGHT_VIOLET_BLUE
	CHANNEL_SKIN_ULTRAMARINE_ASH
	CHANNEL_SKIN_KASHMIR_BLUE
	CHANNEL_SKIN_HORTENSE_BLUE
	CHANNEL_SKIN_CALAMINE_BLUE
	CHANNEL_SKIN_BERYL_GREEN
	CHANNEL_SKIN_SMALT_BLUE
	CHANNEL_SKIN_DUSKY_GREEN_BLUE
	CHANNEL_SKIN_ORCHID
	CHANNEL_SKIN_PHLOX_PURPLE
	CHANNEL_SKIN_VIOLET_PURPLE
	CHANNEL_SKIN_HYACINTH_VIOLET
	CHANNEL_SKIN_LIGHT_NEROPALIN_BLUE
	CHANNEL_SKIN_COLUMBIA_BLUE
	CHANNEL_SKIN_GLAUCOUS_BLUE
	CHANNEL_SKIN_DARK_CINNABAR_GREEN
	CHANNEL_SKIN_LIGHT_ORIENTAL_GREEN
	CHANNEL_SKIN_WINTER_GREEN
	CHANNEL_SKIN_PALE_GREENISH_YELLOW
	CHANNEL_SKIN_LIGHT_CRESS_GREEN
	CHANNEL_SKIN_AVELLANEOUS
	CHANNEL_SKIN_COCOA_PECAN
	CHANNEL_SKIN_ORANGE_VINACEOUS
	CHANNEL_SKIN_HYDRANGEA_RED
	CHANNEL_SKIN_EUPATORIUM_PURPLE
	CHANNEL_SKIN_VINACEOUS_PURPLE
	CHANNEL_SKIN_LIGHT_RUSSET
	CHANNEL_SKIN_MATHEWS_PURPLE
	CHANNEL_SKIN_MAUVETTE
	CHANNEL_SKIN_DARK_MADDER_BLUE
	CHANNEL_SKIN_MATTE_WHITE
	CHANNEL_SKIN_MATTE_BLACK
	CHANNEL_SKIN_LIGHT_MINERAL_GRAY
	CHANNEL_SKIN_DUSKY_BLUE_GREEN
	CHANNEL_SKIN_PLUMMY


	Hair Color channels
	3-7
	15
	22-23
	27
	30-31
	47
	51
	53
	55
	57
	60
	63-65
	74-75
	84-103
	111-115
	119
	126-127
	133-135
	158-159
	165
	167
	171
	173
	
	CHANNEL_HAIR_CAMEO
	CHANNEL_HAIR_TAN
	CHANNEL_HAIR_BRANDY_ROSE
	CHANNEL_HAIR_RUSSET
	CHANNEL_HAIR_RUSSET
	CHANNEL_HAIR_AKAROA
	CHANNEL_HAIR_TOAST
	CHANNEL_HAIR_RUSSET
	CHANNEL_HAIR_AKAROA
	CHANNEL_HAIR_ECRU
	CHANNEL_HAIR_CORKBOARD
	CHANNEL_HAIR_CLOUDY_CINNAMON
	CHANNEL_HAIR_CLAMSHELL
	CHANNEL_HAIR_OLD_ROSE
	CHANNEL_HAIR_CORAL_TREE
	CHANNEL_HAIR_ALAEA
	CHANNEL_HAIR_COTTON_SEED
	CHANNEL_HAIR_CLOUDY
	CHANNEL_HAIR_TAPA
	CHANNEL_HAIR_DOVE_GRAY
	CHANNEL_HAIR_HEATHER
	CHANNEL_HAIR_BALI_HAI
	CHANNEL_HAIR_SLATE_GRAY
	CHANNEL_HAIR_DEEP_PAYNES_GRAY
	CHANNEL_HAIR_TAHUNA_SANDS
	CHANNEL_HAIR_STRAW
	CHANNEL_HAIR_DONKEY_BROWN
	CHANNEL_HAIR_BURLAP
	CHANNEL_HAIR_SHUTTLE_GRAY
	CHANNEL_HAIR_COAL_MINE
	CHANNEL_HAIR_ABBEY
	CHANNEL_HAIR_MAKO
	CHANNEL_HAIR_THISTLE_GREEN
	CHANNEL_HAIR_OLIVINE
	CHANNEL_HAIR_VINTAGE
	CHANNEL_HAIR_MEADOWLAND
	CHANNEL_HAIR_SPRING_RAIN
	CHANNEL_HAIR_BAY_LEAF
	CHANNEL_HAIR_GLADE_GREEN
	CHANNEL_HAIR_CACTUS
	CHANNEL_HAIR_NOBEL_GRAY
	CHANNEL_HAIR_DUSTY_GRAY
	CHANNEL_HAIR_INDUSTRIAL
	CHANNEL_HAIR_RHINE_CASTLE
	CHANNEL_HAIR_ROSE_VALE
	CHANNEL_HAIR_CARMINE
	CHANNEL_HAIR_MAGENTA
	CHANNEL_HAIR_ASTER_PURPLE
	CHANNEL_HAIR_DULL_BLUE_VIOLET
	CHANNEL_HAIR_HELVETIA_BLUE
	CHANNEL_HAIR_TYROLITE_GREEN
	CHANNEL_HAIR_CAPRI_BLUE
	CHANNEL_HAIR_PEACOCK_GREEN
	CHANNEL_HAIR_COSSACK_GREEN
	CHANNEL_HAIR_CALLISTE_GREEN
	CHANNEL_HAIR_WARBLER_GREEN
	CHANNEL_HAIR_SILVER
	CHANNEL_HAIR_QUAKER_DRAB
	CHANNEL_HAIR_MASSICOT_YELLOW
	CHANNEL_HAIR_WAFER
	CHANNEL_HAIR_TUNDORA
	CHANNEL_HAIR_AZO_BLUE
	CHANNEL_HAIR_WHITE
	CHANNEL_HAIR_BLACK
	CHANNEL_HAIR_CONGO_BROWN
	CHANNEL_HAIR_MARS_VIOLET
	CHANNEL_HAIR_OLD_COPPER
	CHANNEL_HAIR_DARK_OLIVE
	CHANNEL_HAIR_CHALET_GREEN
	CHANNEL_HAIR_DARK_GRAY_OLIVE
	CHANNEL_HAIR_DARK_VIOLET
	CHANNEL_HAIR_DARK_SLATE
	CHANNEL_HAIR_DARK_PURPLE
	CHANNEL_HAIR_OLD_LAVENDER
	CHANNEL_HAIR_CAMEO_BROWN
	CHANNEL_HAIR_FLINT
	CHANNEL_HAIR_TERRA_VERTE
	CHANNEL_HAIR_SLATE_OLIVE
	CHANNEL_HAIR_BAYOUX_BLUE
	CHANNEL_HAIR_VIOLET_SLATE
	CHANNEL_HAIR_VERDIGRIS
	CHANNEL_HAIR_SIAM_GREY
	CHANNEL_HAIR_BLUISH_VIOLET
	CHANNEL_HAIR_MID_GRAY
	CHANNEL_HAIR_DUSKY_DRAB
	CHANNEL_HAIR_GRAYISH_BROWN
	CHANNEL_HAIR_VINACEOUS_SLATE
	CHANNEL_HAIR_TOBACCO
	CHANNEL_HAIR_VERMILLION
	CHANNEL_HAIR_BURNT_UMBER
	CHANNEL_HAIR_CEDAR_STAFF
	CHANNEL_HAIR_SMOKED_PAPRIKA
	CHANNEL_HAIR_TUSSOCK
	CHANNEL_HAIR_LUXOR_GOLD
	CHANNEL_HAIR_PESTO
	CHANNEL_HAIR_BREEN
	CHANNEL_HAIR_CONTESSA
	CHANNEL_HAIR_CANDIED_APPLE
	CHANNEL_HAIR_UMEMURASAKI_PURPLE
	CHANNEL_HAIR_TAWNY_PORT
	CHANNEL_HAIR_ANTIQUE_BRASS
	CHANNEL_HAIR_SANTA_FE
	CHANNEL_HAIR_SEPIA
	CHANNEL_HAIR_METALLIC_COPPER
	CHANNEL_HAIR_VERDANT_HAVEN
	CHANNEL_HAIR_WAKATAKE_GREEN
	CHANNEL_HAIR_PINE
	CHANNEL_HAIR_WOODLAND
	CHANNEL_HAIR_PLYMOUTH_GREEN
	CHANNEL_HAIR_VETIVER_GREEN
	CHANNEL_HAIR_JADE_GREEN
	CHANNEL_HAIR_RAW_UMBER
	CHANNEL_HAIR_OLIVE_GRAY
	CHANNEL_HAIR_LIMED_ASH
	CHANNEL_HAIR_FUSCOUS_GRAY
	CHANNEL_HAIR_TUNGSTEN
	CHANNEL_HAIR_SORRELL_BROWN
	CHANNEL_HAIR_SANDALWOOD
	CHANNEL_HAIR_NATAL_BROWN
	CHANNEL_HAIR_MOROCCO_BROWN
	CHANNEL_HAIR_INDIAN_KHAKI
	CHANNEL_HAIR_ROSYBROWN
	CHANNEL_HAIR_YELLOWISH_OLIVE
	CHANNEL_HAIR_MUMMY_BROWN
	CHANNEL_HAIR_PALE_MOUSE_GRAY
	CHANNEL_HAIR_LIGHT_QUAKER_DRAB
	CHANNEL_HAIR_DEEP_QUAKER_DRAB
	CHANNEL_HAIR_TAUPE_BROWN
	CHANNEL_HAIR_VANILLA
	CHANNEL_HAIR_SANDRIFT
	CHANNEL_HAIR_BUFF_BROWN
	CHANNEL_HAIR_SHINGLE_FAWN
	CHANNEL_HAIR_PLUMBEOUS
	CHANNEL_HAIR_DEEP_VARLEYS_GRAY
	CHANNEL_HAIR_SLATE_VIOLET
	CHANNEL_HAIR_LIGHT_VIOLET_BLUE
	CHANNEL_HAIR_ULTRAMARINE_ASH
	CHANNEL_HAIR_KASHMIR_BLUE
	CHANNEL_HAIR_HORTENSE_BLUE
	CHANNEL_HAIR_CALAMINE_BLUE
	CHANNEL_HAIR_BERYL_GREEN
	CHANNEL_HAIR_SMALT_BLUE
	CHANNEL_HAIR_DUSKY_GREEN_BLUE
	CHANNEL_HAIR_ORCHID
	CHANNEL_HAIR_PHLOX_PURPLE
	CHANNEL_HAIR_VIOLET_PURPLE
	CHANNEL_HAIR_HYACINTH_VIOLET
	CHANNEL_HAIR_LIGHT_NEROPALIN_BLUE
	CHANNEL_HAIR_COLUMBIA_BLUE
	CHANNEL_HAIR_GLAUCOUS_BLUE
	CHANNEL_HAIR_DARK_CINNABAR_GREEN
	CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN
	CHANNEL_HAIR_WINTER_GREEN
	CHANNEL_HAIR_PALE_GREENISH_YELLOW
	CHANNEL_HAIR_LIGHT_CRESS_GREEN
	CHANNEL_HAIR_AVELLANEOUS
	CHANNEL_HAIR_COCOA_PECAN
	CHANNEL_HAIR_ORANGE_VINACEOUS
	CHANNEL_HAIR_HYDRANGEA_RED
	CHANNEL_HAIR_EUPATORIUM_PURPLE
	CHANNEL_HAIR_VINACEOUS_PURPLE
	CHANNEL_HAIR_LIGHT_RUSSET
	CHANNEL_HAIR_MATHEWS_PURPLE
	CHANNEL_HAIR_MAUVETTE
	CHANNEL_HAIR_DARK_MADDER_BLUE
	CHANNEL_HAIR_MATTE_WHITE
	CHANNEL_HAIR_MATTE_BLACK
	CHANNEL_HAIR_LIGHT_MINERAL_GRAY
	CHANNEL_HAIR_DUSKY_BLUE_GREEN
	CHANNEL_HAIR_PLUMMY
		
*/

//:: Checks for valid Magma Genasi skin color channel
int CheckMagmaGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor == 7) ||
           (nSkinColor >= 13 && nSkinColor <= 15) ||
           (nSkinColor >= 18 && nSkinColor <= 19) ||
           (nSkinColor >= 28 && nSkinColor <= 31) ||
           (nSkinColor >= 41 && nSkinColor <= 45) ||
           (nSkinColor == 57) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 63 && nSkinColor <= 65) ||
           (nSkinColor >= 87 && nSkinColor <= 91) ||
           (nSkinColor >= 96 && nSkinColor <= 103) ||
           (nSkinColor >= 112 && nSkinColor <= 115) ||
           (nSkinColor >= 118 && nSkinColor <= 119) ||
           (nSkinColor >= 122 && nSkinColor <= 127) ||
           (nSkinColor >= 131 && nSkinColor <= 135) ||
           (nSkinColor >= 158 && nSkinColor <= 159) ||
           (nSkinColor == 165) ||
           (nSkinColor == 167) ||
           (nSkinColor == 171) ||
           (nSkinColor == 173);
}

// Function to check for valid hair color channel for Magma Genasi
int CheckMagmaGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 3 && nHairColor <= 7) ||
           (nHairColor == 15) ||
           (nHairColor >= 22 && nHairColor <= 23) ||
           (nHairColor == 27) ||
           (nHairColor >= 30 && nHairColor <= 31) ||
           (nHairColor == 47) ||
           (nHairColor == 51) ||
           (nHairColor == 53) ||
           (nHairColor == 55) ||
           (nHairColor == 57) ||
           (nHairColor == 60) ||
           (nHairColor >= 63 && nHairColor <= 65) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 103) ||
           (nHairColor >= 111 && nHairColor <= 115) ||
           (nHairColor == 119) ||
           (nHairColor >= 126 && nHairColor <= 127) ||
           (nHairColor >= 133 && nHairColor <= 135) ||
           (nHairColor >= 158 && nHairColor <= 159) ||
           (nHairColor == 165) ||
           (nHairColor == 167) ||
           (nHairColor == 171) ||
           (nHairColor == 173);
}

// Function to randomly set oPC's skin color channel for Magma Genasi
void RndMagmaGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(58);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum == 0) randomSkinColor = 7;
    else if (randomNum >= 1 && randomNum <= 3) randomSkinColor = Random(3) + 13; // Range 13-15
    else if (randomNum >= 4 && randomNum <= 5) randomSkinColor = Random(2) + 18; // Range 18-19
    else if (randomNum >= 6 && randomNum <= 9) randomSkinColor = Random(4) + 28; // Range 28-31
    else if (randomNum >= 10 && randomNum <= 14) randomSkinColor = Random(5) + 41; // Range 41-45
    else if (randomNum == 15) randomSkinColor = 57;
    else if (randomNum == 16) randomSkinColor = 60;
    else if (randomNum >= 17 && randomNum <= 19) randomSkinColor = Random(3) + 63; // Range 63-65
    else if (randomNum >= 20 && randomNum <= 24) randomSkinColor = Random(5) + 87; // Range 87-91
    else if (randomNum >= 25 && randomNum <= 32) randomSkinColor = Random(8) + 96; // Range 96-103
    else if (randomNum >= 33 && randomNum <= 36) randomSkinColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 37 && randomNum <= 38) randomSkinColor = Random(2) + 118; // Range 118-119
    else if (randomNum >= 39 && randomNum <= 44) randomSkinColor = Random(6) + 122; // Range 122-127
    else if (randomNum >= 45 && randomNum <= 49) randomSkinColor = Random(5) + 131; // Range 131-135
    else if (randomNum >= 50 && randomNum <= 51) randomSkinColor = Random(2) + 158; // Range 158-159
    else if (randomNum == 52) randomSkinColor = 165;
    else if (randomNum == 53) randomSkinColor = 167;
    else if (randomNum == 54) randomSkinColor = 171;
    else if (randomNum == 55) randomSkinColor = 173;

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Magma Genasi
void RndMagmaGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(64);

    // Map the random number to a hair color value
    int randomHairColor;

		 if (randomNum >= 0 && randomNum <= 4) return Random(5) + 3; // Range 3-7
    else if (randomNum == 5) return 15; // Single value
    else if (randomNum >= 6 && randomNum <= 7) return Random(2) + 22; // Range 22-23
    else if (randomNum == 8) return 27; // Single value
    else if (randomNum >= 9 && randomNum <= 10) return Random(2) + 30; // Range 30-31
    else if (randomNum == 11) return 47; // Single value
    else if (randomNum == 12) return 51; // Single value
    else if (randomNum == 13) return 53; // Single value
    else if (randomNum == 14) return 55; // Single value
    else if (randomNum == 15) return 57; // Single value
    else if (randomNum == 16) return 60; // Single value
    else if (randomNum >= 17 && randomNum <= 19) return Random(3) + 63; // Range 63-65
    else if (randomNum >= 20 && randomNum <= 21) return Random(2) + 74; // Range 74-75
    else if (randomNum >= 22 && randomNum <= 46) return Random(20) + 84; // Range 84-103
    else if (randomNum >= 47 && randomNum <= 51) return Random(5) + 111; // Range 111-115
    else if (randomNum == 52) return 119; // Single value
    else if (randomNum >= 53 && randomNum <= 54) return Random(2) + 126; // Range 126-127
    else if (randomNum >= 55 && randomNum <= 57) return Random(3) + 133; // Range 133-135
    else if (randomNum >= 58 && randomNum <= 59) return Random(2) + 158; // Range 158-159
    else if (randomNum == 60) return 165; // Single value
    else if (randomNum == 61) return 167; // Single value
    else if (randomNum == 62) return 171; // Single value
    else if (randomNum == 63) return 173; // Single value
   

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/* 	Rain Genasi Colors

	Skin Color Channels
	16-23
	28
	40-42
	48-51
	76-79
	81-84
	112-114
	121-126
	132-143
	148-151
	165
	167-169
	171
	


	
	Hair Color Channels
	16-35
	69-86
	112-115
	124-127
	132-143
	148-153
	165
	167-169
	171	
	
*/	

//:: Checks for valid Rain Genasi skin color channel
int CheckRainGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 16 && nSkinColor <= 23) ||
           (nSkinColor == 28) ||
           (nSkinColor >= 40 && nSkinColor <= 42) ||
           (nSkinColor >= 48 && nSkinColor <= 51) ||
           (nSkinColor >= 76 && nSkinColor <= 79) ||
           (nSkinColor >= 81 && nSkinColor <= 84) ||
           (nSkinColor >= 112 && nSkinColor <= 114) ||
           (nSkinColor >= 121 && nSkinColor <= 126) ||
           (nSkinColor >= 132 && nSkinColor <= 143) ||
           (nSkinColor >= 148 && nSkinColor <= 151) ||
           (nSkinColor == 165) ||
           (nSkinColor >= 167 && nSkinColor <= 169) ||
           (nSkinColor == 171);
}

// Function to check for valid hair color channel for Rain Genasi
int CheckRainGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 16 && nHairColor <= 35) ||
           (nHairColor >= 69 && nHairColor <= 86) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 124 && nHairColor <= 127) ||
           (nHairColor >= 132 && nHairColor <= 143) ||
           (nHairColor >= 148 && nHairColor <= 153) ||
           (nHairColor == 165) ||
           (nHairColor >= 167 && nHairColor <= 169) ||
           (nHairColor == 171);
}

// Function to randomly set oPC's skin color channel for Rain Genasi
void RndRainGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(52);

    // Map the random number to a skin color value
    int randomSkinColor;

		 if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(8) + 16; // Range 16-23
    else if (randomNum == 8) randomSkinColor = 28; // Single value
    else if (randomNum >= 9 && randomNum <= 11) randomSkinColor = Random(3) + 40; // Range 40-42
    else if (randomNum >= 12 && randomNum <= 15) randomSkinColor = Random(4) + 48; // Range 48-51
    else if (randomNum >= 16 && randomNum <= 19) randomSkinColor = Random(4) + 76; // Range 76-79
    else if (randomNum >= 20 && randomNum <= 23) randomSkinColor = Random(4) + 81; // Range 81-84
    else if (randomNum >= 24 && randomNum <= 26) randomSkinColor = Random(3) + 112; // Range 112-114
    else if (randomNum >= 27 && randomNum <= 32) randomSkinColor = Random(6) + 121; // Range 121-126
    else if (randomNum >= 33 && randomNum <= 44) randomSkinColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 45 && randomNum <= 48) randomSkinColor = Random(4) + 148; // Range 148-151
    else if (randomNum == 49) randomSkinColor = 165; // Single value
    else if (randomNum >= 50 && randomNum <= 51) randomSkinColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 52) randomSkinColor = 171; // Single value
    else randomSkinColor = 0; // Default case

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Rain Genasi
void RndRainGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(68);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 19) randomHairColor = Random(20) + 16; // Range 16-35
    else if (randomNum >= 20 && randomNum <= 37) randomHairColor = Random(18) + 69; // Range 69-86
    else if (randomNum >= 38 && randomNum <= 41) randomHairColor = Random(4) + 112; // Range 112-115
    else if (randomNum >= 42 && randomNum <= 45) randomHairColor = Random(4) + 124; // Range 124-127
    else if (randomNum >= 46 && randomNum <= 57) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 58 && randomNum <= 63) randomHairColor = Random(6) + 148; // Range 148-153
    else if (randomNum == 64) randomHairColor = 165; // Single value
    else if (randomNum >= 65 && randomNum <= 67) randomHairColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 68) randomHairColor = 171; // Single value
    else randomHairColor = 0; // Default case

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/*	Silt Genasi Colors

	Skin Color Channels
	39-47
	60
	66-67
	74-75
	84-86
	92-95
	108-111
	112-115
	120-127
	154-155
	172
	174
	
	
	Hair Color Channels
	7-11
	24-31
	40-43
	54-55
	58
	60
	66-67
	74-75
	84-86
	92-95
	104-111
	112-115
	120-127
	130-131
	154-157
	167
	172

*/	

//:: Checks for valid Silt Genasi skin color channel
int CheckSiltGenasiSkin(object oPC)
{
    //:: Get the skin color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 39 && nSkinColor <= 47) ||
           (nSkinColor == 60) ||
           (nSkinColor >= 66 && nSkinColor <= 67) ||
           (nSkinColor >= 74 && nSkinColor <= 75) ||
           (nSkinColor >= 84 && nSkinColor <= 86) ||
           (nSkinColor >= 92 && nSkinColor <= 95) ||
           (nSkinColor >= 108 && nSkinColor <= 111) ||
           (nSkinColor >= 112 && nSkinColor <= 115) ||
           (nSkinColor >= 120 && nSkinColor <= 127) ||
           (nSkinColor >= 154 && nSkinColor <= 155) ||
           (nSkinColor == 172) ||
           (nSkinColor == 174);
}

// Function to check for valid hair color channel for Silt Genasi
int CheckSiltGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 7 && nHairColor <= 11) ||
           (nHairColor >= 24 && nHairColor <= 31) ||
           (nHairColor >= 40 && nHairColor <= 43) ||
           (nHairColor >= 54 && nHairColor <= 55) ||
           (nHairColor == 58) ||
           (nHairColor == 60) ||
           (nHairColor >= 66 && nHairColor <= 67) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 86) ||
           (nHairColor >= 92 && nHairColor <= 95) ||
           (nHairColor >= 104 && nHairColor <= 111) ||
           (nHairColor >= 112 && nHairColor <= 115) ||
           (nHairColor >= 120 && nHairColor <= 127) ||
           (nHairColor >= 130 && nHairColor <= 131) ||
           (nHairColor >= 154 && nHairColor <= 157) ||
           (nHairColor == 167) ||
           (nHairColor == 172);
}

// Function to randomly set oPC's skin color channel for Silt Genasi
void RndSiltGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(58);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 7) randomSkinColor = Random(9) + 39; // Range 39-47
    else if (randomNum == 8) randomSkinColor = 60; // Range 60
    else if (randomNum >= 9 && randomNum <= 10) randomSkinColor = Random(2) + 66; // Range 66-67
    else if (randomNum >= 11 && randomNum <= 12) randomSkinColor = Random(2) + 74; // Range 74-75
    else if (randomNum >= 13 && randomNum <= 15) randomSkinColor = Random(3) + 84; // Range 84-86
    else if (randomNum >= 16 && randomNum <= 19) randomSkinColor = Random(4) + 92; // Range 92-95
    else if (randomNum >= 20 && randomNum <= 23) randomSkinColor = Random(4) + 108; // Range 108-111
    else if (randomNum >= 24 && randomNum <= 29) randomSkinColor = Random(6) + 112; // Range 112-115
    else if (randomNum >= 30 && randomNum <= 41) randomSkinColor = Random(12) + 120; // Range 120-127
    else if (randomNum >= 42 && randomNum <= 55) randomSkinColor = Random(14) + 154; // Range 154-157
    else if (randomNum == 56) randomSkinColor = 172; // Single value
    else if (randomNum == 57) randomSkinColor = 174; // Single value

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Silt Genasi
void RndSiltGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(80);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 4) randomHairColor = Random(5) + 7; // Range 7-11
    else if (randomNum >= 5 && randomNum <= 18) randomHairColor = Random(8) + 24; // Range 24-31
    else if (randomNum >= 19 && randomNum <= 20) randomHairColor = Random(2) + 40; // Range 40-43
    else if (randomNum >= 21 && randomNum <= 22) randomHairColor = Random(2) + 54; // Range 54-55
    else if (randomNum == 23) randomHairColor = 58; // Single value
    else if (randomNum == 24) randomHairColor = 60; // Single value
    else if (randomNum >= 25 && randomNum <= 26) randomHairColor = Random(2) + 66; // Range 66-67
    else if (randomNum >= 27 && randomNum <= 28) randomHairColor = Random(2) + 74; // Range 74-75
    else if (randomNum >= 29 && randomNum <= 31) randomHairColor = Random(3) + 84; // Range 84-86
    else if (randomNum >= 32 && randomNum <= 35) randomHairColor = Random(4) + 92; // Range 92-95
    else if (randomNum >= 36 && randomNum <= 47) randomHairColor = Random(12) + 104; // Range 104-111
    else if (randomNum >= 48 && randomNum <= 59) randomHairColor = Random(12) + 112; // Range 112-115
    else if (randomNum >= 60 && randomNum <= 71) randomHairColor = Random(12) + 120; // Range 120-127
    else if (randomNum >= 72 && randomNum <= 73) randomHairColor = Random(2) + 130; // Range 130-131
    else if (randomNum >= 74 && randomNum <= 77) randomHairColor = Random(4) + 154; // Range 154-157
    else if (randomNum == 78) randomHairColor = 167; // Single value
    else if (randomNum == 79) randomHairColor = 172; // Single value

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}


/*	Sun Genasi Colors
	
	Skin Color Channels
	0-14
	24-27
	54-55
	57-59
	66
	87-95
	116-117
	128-129
	154-159
	167
	172
	175
	
	
	Hair Color Channels
	8-15
	42-47
	56
	58
	65-67
	74-75
	84-95
	100-103
	154-163
	167
	172
	173-175
		
	
*/


// Function to check for valid skin color channel for Sun Genasi
int CheckSunGenasiSkin(object oPC)
{
    // Get the skin color channel
    int nSkinColor = GetColorChannel(oPC, COLOR_CHANNEL_SKIN);

    // Check if the skin color channel is within any of the specified ranges
    return (nSkinColor >= 0 && nSkinColor <= 14) ||
           (nSkinColor >= 24 && nSkinColor <= 27) ||
           (nSkinColor >= 54 && nSkinColor <= 55) ||
           (nSkinColor >= 57 && nSkinColor <= 59) ||
           (nSkinColor == 66) ||
           (nSkinColor >= 87 && nSkinColor <= 95) ||
           (nSkinColor >= 116 && nSkinColor <= 117) ||
           (nSkinColor >= 128 && nSkinColor <= 129) ||
           (nSkinColor >= 154 && nSkinColor <= 159) ||
           (nSkinColor == 167) ||
           (nSkinColor == 172) ||
           (nSkinColor == 175);
}

// Function to check for valid hair color channel for Sun Genasi
int CheckSunGenasiHair(object oPC)
{
    // Get the hair color channel
    int nHairColor = GetColorChannel(oPC, COLOR_CHANNEL_HAIR);

    // Check if the hair color channel is within any of the specified ranges
    return (nHairColor >= 8 && nHairColor <= 15) ||
           (nHairColor >= 42 && nHairColor <= 47) ||
           (nHairColor == 56) ||
           (nHairColor == 58) ||
           (nHairColor >= 65 && nHairColor <= 67) ||
           (nHairColor >= 74 && nHairColor <= 75) ||
           (nHairColor >= 84 && nHairColor <= 95) ||
           (nHairColor >= 100 && nHairColor <= 103) ||
           (nHairColor >= 154 && nHairColor <= 163) ||
           (nHairColor == 167) ||
           (nHairColor == 172) ||
           (nHairColor >= 173 && nHairColor <= 175);
}

// Function to randomly set oPC's skin color channel for Sun Genasi
void RndSunGenasiSkin(object oPC)
{
    // Get a random number
    int randomNum = Random(43);

    // Map the random number to a skin color value
    int randomSkinColor;

    if (randomNum >= 0 && randomNum <= 14) randomSkinColor = Random(15); // Range 0-14
    else if (randomNum >= 15 && randomNum <= 18) randomSkinColor = Random(4) + 24; // Range 24-27
    else if (randomNum >= 19 && randomNum <= 20) randomSkinColor = Random(2) + 54; // Range 54-55
    else if (randomNum >= 21 && randomNum <= 23) randomSkinColor = Random(2) + 57; // Range 57-59
    else if (randomNum == 24) randomSkinColor = 66; // Range 66
    else if (randomNum >= 25 && randomNum <= 32) randomSkinColor = Random(9) + 87; // Range 87-95
    else if (randomNum >= 33 && randomNum <= 34) randomSkinColor = Random(2) + 116; // Range 116-117
    else if (randomNum >= 35 && randomNum <= 36) randomSkinColor = Random(2) + 128; // Range 128-129
    else if (randomNum >= 37 && randomNum <= 40) randomSkinColor = Random(5) + 154; // Range 154-159
    else if (randomNum == 41) randomSkinColor = 167; // Single value
    else if (randomNum == 42) randomSkinColor = 172; // Single value
    else if (randomNum == 43) randomSkinColor = 175; // Single value

    // Set the random skin color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_SKIN, randomSkinColor);
}

// Function to randomly set oPC's hair color channel for Sun Genasi
void RndSunGenasiHair(object oPC)
{
    // Get a random number
    int randomNum = Random(73);

    // Map the random number to a hair color value
    int randomHairColor;

    if (randomNum >= 0 && randomNum <= 6) randomHairColor = Random(8) + 16; // Range 16-23
    else if (randomNum == 7) randomHairColor = 28; // Single value
    else if (randomNum >= 8 && randomNum <= 10) randomHairColor = Random(3) + 40; // Range 40-42
    else if (randomNum >= 11 && randomNum <= 14) randomHairColor = Random(4) + 48; // Range 48-51
    else if (randomNum >= 15 && randomNum <= 18) randomHairColor = Random(4) + 76; // Range 76-79
    else if (randomNum >= 19 && randomNum <= 21) randomHairColor = Random(4) + 81; // Range 81-84
    else if (randomNum >= 22 && randomNum <= 24) randomHairColor = Random(3) + 112; // Range 112-114
    else if (randomNum >= 25 && randomNum <= 30) randomHairColor = Random(6) + 121; // Range 121-126
    else if (randomNum >= 31 && randomNum <= 42) randomHairColor = Random(12) + 132; // Range 132-143
    else if (randomNum >= 43 && randomNum <= 46) randomHairColor = Random(4) + 148; // Range 148-151
    else if (randomNum == 47) randomHairColor = 165; // Single value
    else if (randomNum >= 48 && randomNum <= 50) randomHairColor = Random(3) + 167; // Range 167-169
    else if (randomNum == 51) randomHairColor = 171; // Single value
    else if (randomNum == 52) randomHairColor = 173; // Single value
    else if (randomNum >= 53 && randomNum <= 61) randomHairColor = Random(9) + 174; // Range 174-182
    else if (randomNum >= 62 && randomNum <= 64) randomHairColor = Random(3) + 187; // Range 187-189
    else if (randomNum == 65) randomHairColor = 194; // Single value
    else if (randomNum == 66) randomHairColor = 195; // Single value
    else if (randomNum >= 67 && randomNum <= 70) randomHairColor = Random(4) + 199; // Range 199-202
    else if (randomNum == 71) randomHairColor = 208; // Single value
    else if (randomNum == 72) randomHairColor = 213; // Single value
    else if (randomNum == 73) randomHairColor = 215; // Single value

    // Set the random hair color value for oPC
    SetColorChannel(oPC, COLOR_CHANNEL_HAIR, randomHairColor);
}










//:: Function to check if oPC has valid Air Genasi skin color channels
int HasValidAirGenasiSkinColor(object oPC);

//:: Function to check if oPC has valid Air Genasi hair color channels
int HasValidAirGenasiHairColor(object oPC);

//:: Function to check if oPC has valid Earth Genasi skin color channels
int HasValidEarthGenasiSkinColor(object oPC);

//:: Function to check if oPC has valid Earth Genasi hair color channels
int HasValidEarthGenasiHairColor(object oPC);

//:: Function to check if oPC has valid Fire Genasi skin color channels
int HasValidFireGenasiSkinColor(object oPC);

//:: Function to check if oPC has valid Fire Genasi hair color channels
int HasValidFireGenasiHairColor(object oPC);

//:: Function to check if oPC has valid Water Genasi skin color channels
int HasValidWaterGenasiSkinColor(object oPC);

//:: Function to check if oPC has valid Water Genasi hair color channels
int HasValidWaterGenasiHairColor(object oPC);



//:: Function to check if oPC has valid Air Genasi skin color channels
int HasValidAirGenasiSkinColor(object oPC)
{
    //:: Get the skin color channel of the player character
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color is valid.
    if( nSkinColor == CHANNEL_SKIN_DOVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_HEATHER ||
		nSkinColor == CHANNEL_SKIN_BALI_HAI ||
		nSkinColor == CHANNEL_SKIN_SLATE_GRAY ||
		nSkinColor == CHANNEL_SKIN_CACTUS ||
		nSkinColor == CHANNEL_SKIN_CALLISTE_GREEN ||
		nSkinColor == CHANNEL_SKIN_WARBLER_GREEN ||
		nSkinColor == CHANNEL_SKIN_PLUMBEOUS ||
		nSkinColor == CHANNEL_SKIN_DEEP_VARLEYS_GRAY ||
		nSkinColor == CHANNEL_SKIN_SLATE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_SLATE_BLACK ||
		nSkinColor == CHANNEL_SKIN_MATHEWS_PURPLE ||
		nSkinColor == CHANNEL_SKIN_MAUVETTE ||
		nSkinColor == CHANNEL_SKIN_LIGHT_RUSSET ||
		nSkinColor == CHANNEL_SKIN_MATTE_WHITE ||
		nSkinColor == CHANNEL_SKIN_ROSY_SANDSTONE ||
		nSkinColor == CHANNEL_SKIN_MOCHA)
		{
        //:: Skin color is valid, return TRUE
			return TRUE;
		}

//:: Skin color is not within any valid range, return FALSE
	return FALSE;
}

//:: Function to check if oPC has valid Air Genasi hair color channels
int HasValidAirGenasiHairColor(object oPC)
{
    //:: Get the hair color channel of the player character
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color is within any of the valid ranges
    if 	(nHairColor == CHANNEL_HAIR_BROWN_BLACK ||
		nHairColor == CHANNEL_HAIR_PALLID_BLUE ||
		nHairColor == CHANNEL_HAIR_PALE_RUSSIAN_BLUE ||
		nHairColor == CHANNEL_HAIR_LIGHT_SLATE_GRAY ||
		nHairColor == CHANNEL_HAIR_SLATE ||
		nHairColor == CHANNEL_HAIR_DEEP_DUTCH_BLUE ||
		nHairColor == CHANNEL_HAIR_GRAVEL_GREY_BLUE ||
		nHairColor == CHANNEL_HAIR_BLUISH_SLATE_BLACK ||
		nHairColor == CHANNEL_HAIR_DARK_SLATE_GRAY ||
		nHairColor == CHANNEL_HAIR_CHICORY_BLUE ||
		nHairColor == CHANNEL_HAIR_MIDNIGHT_BLUE ||
		nHairColor == CHANNEL_HAIR_DEEP_PLUMBAGO_BLUE ||
		nHairColor == CHANNEL_HAIR_PLUM_PURPLE ||
		nHairColor == CHANNEL_HAIR_FORGET_ME_NOT_BLUE ||
		nHairColor == CHANNEL_HAIR_PASSIONATE_BLUE ||
		nHairColor == CHANNEL_HAIR_SILVER_CHALICE ||
		nHairColor == CHANNEL_HAIR_CHARCOAL ||
		nHairColor == CHANNEL_HAIR_NILE_BLUE ||
		nHairColor == CHANNEL_HAIR_ANTHRACENE_GREEN ||
		nHairColor == CHANNEL_HAIR_BLUISH_GLAUCOUS ||
		nHairColor == CHANNEL_HAIR_DUSKY_SLATE_BLUE ||
		nHairColor == CHANNEL_HAIR_VARISCITE_GREEN ||
		nHairColor == CHANNEL_HAIR_DUSKY_DULL_GREEN ||
		nHairColor == CHANNEL_HAIR_COURT_GRAY ||
		nHairColor == CHANNEL_HAIR_DULL_BLACKISH_GREEN ||
		nHairColor == CHANNEL_HAIR_OLIVINE ||
		nHairColor == CHANNEL_HAIR_DARK_OLIVE_GREEN ||
		nHairColor == CHANNEL_HAIR_ASH_GRAY ||
		nHairColor == CHANNEL_HAIR_OLIVACEOUS_BLACK ||
		nHairColor == CHANNEL_HAIR_OLIVE_BUFF ||
		nHairColor == CHANNEL_HAIR_MEDAL_BRONZE ||
		nHairColor == CHANNEL_HAIR_PALLID_MOUSE_GRAY ||
		nHairColor == CHANNEL_HAIR_BROWNISH_GRAY ||
		nHairColor == CHANNEL_HAIR_DEEP_OLIVE_BUFF ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_BRASS ||
		nHairColor == CHANNEL_HAIR_SILVER_PINK ||
		nHairColor == CHANNEL_HAIR_TAUPE_BROWN ||
		nHairColor == CHANNEL_HAIR_VINACEOUS ||
		nHairColor == CHANNEL_HAIR_PERSIAN_PLUM ||
		nHairColor == CHANNEL_HAIR_PALE_PURPLE_DRAB ||
		nHairColor == CHANNEL_HAIR_DARK_LIVID_PURPLE ||
		nHairColor == CHANNEL_HAIR_CEREBRAL_GREY ||
		nHairColor == CHANNEL_HAIR_DOVE_GRAY ||
		nHairColor == CHANNEL_HAIR_MASSICOT_YELLOW ||
		nHairColor == CHANNEL_HAIR_DESERT_SAND ||
		nHairColor == CHANNEL_HAIR_DAVYS_GRAY ||
		nHairColor == CHANNEL_HAIR_MIRROR ||
		nHairColor == CHANNEL_HAIR_GLOSS_WHITE ||
		nHairColor == CHANNEL_HAIR_GLOSS_BLACK ||
		nHairColor == CHANNEL_HAIR_NEUTRAL_RED ||
		nHairColor == CHANNEL_HAIR_DARK_MINERAL_RED ||
		nHairColor == CHANNEL_HAIR_COYOTE_BROWN ||
		nHairColor == CHANNEL_HAIR_UMBER ||
		nHairColor == CHANNEL_HAIR_HUNTER_GREEN ||
		nHairColor == CHANNEL_HAIR_DARK_GRAYISH_OLIVE ||
		nHairColor == CHANNEL_HAIR_DARK_VIOLET ||
		nHairColor == CHANNEL_HAIR_DEEP_SLATE_VIOLET ||
		nHairColor == CHANNEL_HAIR_DULL_DARK_PURPLE ||
		nHairColor == CHANNEL_HAIR_SLATE_VIOLET ||
		nHairColor == CHANNEL_HAIR_MARS_BROWN ||
		nHairColor == CHANNEL_HAIR_DARK_PURPLE_DRAB ||
		nHairColor == CHANNEL_HAIR_DARK_RUSSIAN_GREEN ||
		nHairColor == CHANNEL_HAIR_DARK_NEUTRAL_GRAY ||
		nHairColor == CHANNEL_HAIR_INDIGO_BLUE ||
		nHairColor == CHANNEL_HAIR_TITANIUM_GRAY ||
		nHairColor == CHANNEL_HAIR_WARPLOCK_BRONZE ||
		nHairColor == CHANNEL_HAIR_CHAETURA_DRAB ||
		nHairColor == CHANNEL_HAIR_DUSK_BLUE ||
		nHairColor == CHANNEL_HAIR_GRAPHITE_GRAY ||
		nHairColor == CHANNEL_HAIR_FUSCOUS_GRAY ||
		nHairColor == CHANNEL_HAIR_BONE_BROWN ||
		nHairColor == CHANNEL_HAIR_ESPRESSO ||
		nHairColor == CHANNEL_HAIR_VANDYKE_BROWN ||
		nHairColor == CHANNEL_HAIR_DEEP_REDDISH_ORANGE ||
		nHairColor == CHANNEL_HAIR_COPPER_BROWN ||
		nHairColor == CHANNEL_HAIR_BURNT_UMBER ||
		nHairColor == CHANNEL_HAIR_OX_BLOOD_RED ||
		nHairColor == CHANNEL_HAIR_PYRITE_YELLOW ||
		nHairColor == CHANNEL_HAIR_BUCKTHORN_BROWN ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_BROWN ||
		nHairColor == CHANNEL_HAIR_PEARL_GOLD ||
		nHairColor == CHANNEL_HAIR_LIGHT_PINK ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_PINK ||
		nHairColor == CHANNEL_HAIR_ACAJOU_RED ||
		nHairColor == CHANNEL_HAIR_VIOLET_CARMINE ||
		nHairColor == CHANNEL_HAIR_BEIGE_RED ||
		nHairColor == CHANNEL_HAIR_PECAN_BROWN ||
		nHairColor == CHANNEL_HAIR_POMPEIAN_RED ||
		nHairColor == CHANNEL_HAIR_SADDLE_BROWN ||
		nHairColor == CHANNEL_HAIR_FOLIAGE ||
		nHairColor == CHANNEL_HAIR_MOSS ||
		nHairColor == CHANNEL_HAIR_FOREST_GREEN ||
		nHairColor == CHANNEL_HAIR_LEAF_GREEN ||
		nHairColor == CHANNEL_HAIR_CORYDALIS_GREEN ||
		nHairColor == CHANNEL_HAIR_TEA_GREEN ||
		nHairColor == CHANNEL_HAIR_JADE_GREEN ||
		nHairColor == CHANNEL_HAIR_WOODLAND ||
		nHairColor == CHANNEL_HAIR_OLIVE_GRAY ||
		nHairColor == CHANNEL_HAIR_MOUSE_GRAY ||
		nHairColor == CHANNEL_HAIR_DARK_GRAYISH_BROWN ||
		nHairColor == CHANNEL_HAIR_ANILINE_BLACK ||
		nHairColor == CHANNEL_HAIR_TAN ||
		nHairColor == CHANNEL_HAIR_TAUPE ||
		nHairColor == CHANNEL_HAIR_NATAL_BROWN ||
		nHairColor == CHANNEL_HAIR_MAHOGANY_BROWN ||
		nHairColor == CHANNEL_HAIR_BEIGE ||
		nHairColor == CHANNEL_HAIR_VETIVER_GREEN ||
		nHairColor == CHANNEL_HAIR_GOLD_FUSION ||
		nHairColor == CHANNEL_HAIR_NICKEL_GREEN ||
		nHairColor == CHANNEL_HAIR_MADDER_VIOLET)
		{
        //:: Hair color is valid, return TRUE
			return TRUE;
		}

//:: Hair color is not within any valid range, return FALSE
    return FALSE;
}

//:: Function to check if oPC has valid Earth Genasi skin color channels
int HasValidEarthGenasiSkinColor(object oPC)
{
    //:: Get the skin color channel of the player character
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color is valid.
    if( nSkinColor == CHANNEL_SKIN_TAN ||
		nSkinColor == CHANNEL_SKIN_BRANDY_ROSE ||
		nSkinColor == CHANNEL_SKIN_KURUMIZOME_BROWN ||
		nSkinColor == CHANNEL_SKIN_PRUSSIAN_RED ||
		nSkinColor == CHANNEL_SKIN_TOAST ||
		nSkinColor == CHANNEL_SKIN_CORKBOARD ||
		nSkinColor == CHANNEL_SKIN_OLD_ROSE ||
		nSkinColor == CHANNEL_SKIN_CORAL_TREE ||
		nSkinColor == CHANNEL_SKIN_CLOUDY ||
		nSkinColor == CHANNEL_SKIN_TAPA ||
		nSkinColor == CHANNEL_SKIN_DOVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_HEATHER ||
		nSkinColor == CHANNEL_SKIN_BALI_HAI ||
		nSkinColor == CHANNEL_SKIN_SLATE_GRAY ||
		nSkinColor == CHANNEL_SKIN_DEEP_PAYNES_GRAY ||
		nSkinColor == CHANNEL_SKIN_TAHUNA_SANDS ||
		nSkinColor == CHANNEL_SKIN_STRAW ||
		nSkinColor == CHANNEL_SKIN_ABBEY ||
		nSkinColor == CHANNEL_SKIN_VINTAGE ||
		nSkinColor == CHANNEL_SKIN_MEADOWLAND ||
		nSkinColor == CHANNEL_SKIN_SPRING_RAIN ||
		nSkinColor == CHANNEL_SKIN_BAY_LEAF ||
		nSkinColor == CHANNEL_SKIN_GLADE_GREEN ||
		nSkinColor == CHANNEL_SKIN_NOBEL_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSTY_GRAY ||
		nSkinColor == CHANNEL_SKIN_INDUSTRIAL ||
		nSkinColor == CHANNEL_SKIN_ROSE_VALE ||
		nSkinColor == CHANNEL_SKIN_CARMINE ||
		nSkinColor == CHANNEL_SKIN_MAGENTA ||
		nSkinColor == CHANNEL_SKIN_ASTER_PURPLE ||
		nSkinColor == CHANNEL_SKIN_DULL_BLUE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_HELVETIA_BLUE ||
		nSkinColor == CHANNEL_SKIN_TYROLITE_GREEN ||
		nSkinColor == CHANNEL_SKIN_CAPRI_BLUE ||
		nSkinColor == CHANNEL_SKIN_PEACOCK_GREEN ||
		nSkinColor == CHANNEL_SKIN_COSSACK_GREEN ||
		nSkinColor == CHANNEL_SKIN_CALLISTE_GREEN ||
		nSkinColor == CHANNEL_SKIN_WARBLER_GREEN ||
		nSkinColor == CHANNEL_SKIN_SILVER ||
		nSkinColor == CHANNEL_SKIN_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_MASSICOT_YELLOW ||
		nSkinColor == CHANNEL_SKIN_SEPIA ||
		nSkinColor == CHANNEL_SKIN_METALLIC_COPPER ||
		nSkinColor == CHANNEL_SKIN_VERDANT_HAVEN ||
		nSkinColor == CHANNEL_SKIN_WAKATAKE_GREEN ||
		nSkinColor == CHANNEL_SKIN_PINE ||
		nSkinColor == CHANNEL_SKIN_WOODLAND ||
		nSkinColor == CHANNEL_SKIN_PLYMOUTH_GREEN ||
		nSkinColor == CHANNEL_SKIN_VETIVER_GREEN ||
		nSkinColor == CHANNEL_SKIN_JADE_GREEN ||
		nSkinColor == CHANNEL_SKIN_RAW_UMBER ||
		nSkinColor == CHANNEL_SKIN_OLIVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_LIMED_ASH ||
		nSkinColor == CHANNEL_SKIN_FUSCOUS_GRAY ||
		nSkinColor == CHANNEL_SKIN_TUNGSTEN ||
		nSkinColor == CHANNEL_SKIN_SORRELL_BROWN ||
		nSkinColor == CHANNEL_SKIN_SANDALWOOD ||
		nSkinColor == CHANNEL_SKIN_NATAL_BROWN ||
		nSkinColor == CHANNEL_SKIN_MOROCCO_BROWN ||
		nSkinColor == CHANNEL_SKIN_ROSYBROWN ||
		nSkinColor == CHANNEL_SKIN_YELLOWISH_OLIVE ||
		nSkinColor == CHANNEL_SKIN_MUMMY_BROWN ||
		nSkinColor == CHANNEL_SKIN_PALE_MOUSE_GRAY ||
		nSkinColor == CHANNEL_SKIN_LIGHT_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_DEEP_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_PALE_GREENISH_YELLOW ||
		nSkinColor == CHANNEL_SKIN_LIGHT_CRESS_GREEN ||
		nSkinColor == CHANNEL_SKIN_AVELLANEOUS ||
		nSkinColor == CHANNEL_SKIN_PLUMMY ||
		nSkinColor == CHANNEL_SKIN_BLACK_ORCHID ||
		nSkinColor == CHANNEL_SKIN_DARK_CITRINE ||
		nSkinColor == CHANNEL_SKIN_ROSY_SANDSTONE )
		{
        //:: Skin color is valid, return TRUE
			return TRUE;
		}

//:: Skin color is not within any valid range, return FALSE
	return FALSE;
}

//:: Function to check if oPC has valid Earth Genasi hair color channels
int HasValidEarthGenasiHairColor(object oPC)
{
    //:: Get the hair color channel of the player character
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color is within any of the valid ranges
    if (nHairColor == CHANNEL_HAIR_AMBER_BROWN ||
		nHairColor == CHANNEL_HAIR_CHESTNUT ||
		nHairColor == CHANNEL_HAIR_MAROON ||
		nHairColor == CHANNEL_HAIR_CHOCOLATE ||
		nHairColor == CHANNEL_HAIR_HEATH_BROWN ||
		nHairColor == CHANNEL_HAIR_ARGUS_BROWN ||
		nHairColor == CHANNEL_HAIR_BRACKEN ||
		nHairColor == CHANNEL_HAIR_BROWN_BLACK ||
		nHairColor == CHANNEL_HAIR_SLATE ||
		nHairColor == CHANNEL_HAIR_DARK_SLATE_GRAY ||
		nHairColor == CHANNEL_HAIR_PLUM_PURPLE ||
		nHairColor == CHANNEL_HAIR_FORGET_ME_NOT_BLUE ||
		nHairColor == CHANNEL_HAIR_PASSIONATE_BLUE ||
		nHairColor == CHANNEL_HAIR_SILVER_CHALICE ||
		nHairColor == CHANNEL_HAIR_CHARCOAL ||
		nHairColor == CHANNEL_HAIR_DUSKY_SLATE_BLUE ||
		nHairColor == CHANNEL_HAIR_VARISCITE_GREEN ||
		nHairColor == CHANNEL_HAIR_DUSKY_DULL_GREEN ||
		nHairColor == CHANNEL_HAIR_COURT_GRAY ||
		nHairColor == CHANNEL_HAIR_DULL_BLACKISH_GREEN ||
		nHairColor == CHANNEL_HAIR_OLIVINE ||
		nHairColor == CHANNEL_HAIR_BROWNISH_GRAY ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_BRASS ||
		nHairColor == CHANNEL_HAIR_DOVE_GRAY ||
		nHairColor == CHANNEL_HAIR_DAVYS_GRAY ||
		nHairColor == CHANNEL_HAIR_GLOSS_BLACK ||
		nHairColor == CHANNEL_HAIR_INDIGO_BLUE ||
		nHairColor == CHANNEL_HAIR_TITANIUM_GRAY ||
		nHairColor == CHANNEL_HAIR_WARPLOCK_BRONZE ||
		nHairColor == CHANNEL_HAIR_MARS_BROWN ||
		nHairColor == CHANNEL_HAIR_DARK_PURPLE_DRAB ||
		nHairColor == CHANNEL_HAIR_DARK_RUSSIAN_GREEN ||
		nHairColor == CHANNEL_HAIR_DARK_NEUTRAL_GRAY ||
		nHairColor == CHANNEL_HAIR_INDIGO_BLUE ||
		nHairColor == CHANNEL_HAIR_TITANIUM_GRAY ||
		nHairColor == CHANNEL_HAIR_WARPLOCK_BRONZE ||
		nHairColor == CHANNEL_HAIR_CHAETURA_DRAB ||
		nHairColor == CHANNEL_HAIR_DUSK_BLUE ||
		nHairColor == CHANNEL_HAIR_GRAPHITE_GRAY ||
		nHairColor == CHANNEL_HAIR_FUSCOUS_GRAY ||
		nHairColor == CHANNEL_HAIR_BONE_BROWN ||
		nHairColor == CHANNEL_HAIR_ESPRESSO ||
		nHairColor == CHANNEL_HAIR_VANDYKE_BROWN ||
		nHairColor == CHANNEL_HAIR_LIGHT_PINK ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_PINK ||
		nHairColor == CHANNEL_HAIR_ACAJOU_RED ||
		nHairColor == CHANNEL_HAIR_VIOLET_CARMINE ||
		nHairColor == CHANNEL_HAIR_BEIGE_RED ||
		nHairColor == CHANNEL_HAIR_PECAN_BROWN ||
		nHairColor == CHANNEL_HAIR_POMPEIAN_RED ||
		nHairColor == CHANNEL_HAIR_SADDLE_BROWN ||
		nHairColor == CHANNEL_HAIR_FOLIAGE ||
		nHairColor == CHANNEL_HAIR_MOSS ||
		nHairColor == CHANNEL_HAIR_FOREST_GREEN ||
		nHairColor == CHANNEL_HAIR_LEAF_GREEN ||
		nHairColor == CHANNEL_HAIR_CORYDALIS_GREEN ||
		nHairColor == CHANNEL_HAIR_TEA_GREEN ||
		nHairColor == CHANNEL_HAIR_JADE_GREEN ||
		nHairColor == CHANNEL_HAIR_WOODLAND ||
		nHairColor == CHANNEL_HAIR_OLIVE_GRAY ||
		nHairColor == CHANNEL_HAIR_MOUSE_GRAY ||
		nHairColor == CHANNEL_HAIR_DARK_GRAYISH_BROWN ||
		nHairColor == CHANNEL_HAIR_ANILINE_BLACK ||
		nHairColor == CHANNEL_HAIR_TAN ||
		nHairColor == CHANNEL_HAIR_TAUPE ||
		nHairColor == CHANNEL_HAIR_NATAL_BROWN ||
		nHairColor == CHANNEL_HAIR_MAHOGANY_BROWN ||
		nHairColor == CHANNEL_HAIR_BEIGE ||
		nHairColor == CHANNEL_HAIR_VETIVER_GREEN ||
		nHairColor == CHANNEL_HAIR_GOLD_FUSION ||
		nHairColor == CHANNEL_HAIR_DARK_OLIVE ||
		nHairColor == CHANNEL_HAIR_LIGHT_SQUILL_BLUE ||
		nHairColor == CHANNEL_HAIR_COLUMBIA_BLUE ||
		nHairColor == CHANNEL_HAIR_GLAUCOUS_BLUE ||
		nHairColor == CHANNEL_HAIR_DARK_CINNABAR_GREEN ||
		nHairColor == CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN ||
		nHairColor == CHANNEL_HAIR_WINTER_GREEN ||
		nHairColor == CHANNEL_HAIR_KILDARE_GREEN ||
		nHairColor == CHANNEL_HAIR_DULL_CITRINE ||
		nHairColor == CHANNEL_HAIR_LIGHT_CRESS_GREEN ||
		nHairColor == CHANNEL_HAIR_AVELLANEOUS ||
		nHairColor == CHANNEL_HAIR_TAHINI_BROWN ||
		nHairColor == CHANNEL_HAIR_DEEP_CORINTHIAN_RED ||
		nHairColor == CHANNEL_HAIR_ARGYLE_PURPLE ||
		nHairColor == CHANNEL_HAIR_LIGHT_PERILLA_PURPLE ||
		nHairColor == CHANNEL_HAIR_PINKISH_VINACEOUS ||
		nHairColor == CHANNEL_HAIR_MATHEWS_PURPLE ||
		nHairColor == CHANNEL_HAIR_DARK_MADDER_BLUE ||
		nHairColor == CHANNEL_HAIR_MATTE_WHITE ||
		nHairColor == CHANNEL_HAIR_MATTE_BLACK ||
		nHairColor == CHANNEL_HAIR_PALE_GULL_GRAY ||
		nHairColor == CHANNEL_HAIR_NICKEL_GREEN ||
		nHairColor == CHANNEL_HAIR_MADDER_VIOLET ||
		nHairColor == CHANNEL_HAIR_DEEP_MADDER_BLUE ||
		nHairColor == CHANNEL_HAIR_DARK_CITRINE ||
		nHairColor == CHANNEL_HAIR_MAHOGANY ||
		nHairColor == CHANNEL_HAIR_LIGHT_BROWNISH_OLIVE)
		{
        //:: Hair color is valid, return TRUE
			return TRUE;
		}

//:: Hair color is not within any valid range, return FALSE
    return FALSE;
}

//:: Function to check if oPC has valid Fire Genasi skin color channels
int HasValidFireGenasiSkinColor(object oPC)
{
    //:: Get the skin color channel of the player character
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color is valid.
    if( nSkinColor == CHANNEL_SKIN_KURUMIZOME_BROWN ||
		nSkinColor == CHANNEL_SKIN_PRUSSIAN_RED ||
		nSkinColor == CHANNEL_SKIN_TOAST ||
		nSkinColor == CHANNEL_SKIN_RUSSET ||
		nSkinColor == CHANNEL_SKIN_CLAMSHELL ||
		nSkinColor == CHANNEL_SKIN_OLD_ROSE ||
		nSkinColor == CHANNEL_SKIN_CORAL_TREE ||
		nSkinColor == CHANNEL_SKIN_ALAEA ||
		nSkinColor == CHANNEL_SKIN_TAPA ||
		nSkinColor == CHANNEL_SKIN_DOVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_BALI_HAI ||
		nSkinColor == CHANNEL_SKIN_SLATE_GRAY ||
		nSkinColor == CHANNEL_SKIN_BURLAP ||
		nSkinColor == CHANNEL_SKIN_SHUTTLE_GRAY ||
		nSkinColor == CHANNEL_SKIN_COAL_MINE ||
		nSkinColor == CHANNEL_SKIN_ABBEY ||
		nSkinColor == CHANNEL_SKIN_MAKO ||
		nSkinColor == CHANNEL_SKIN_MAGENTA ||
		nSkinColor == CHANNEL_SKIN_ASTER_PURPLE ||
		nSkinColor == CHANNEL_SKIN_DULL_BLUE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_HELVETIA_BLUE ||
		nSkinColor == CHANNEL_SKIN_TYROLITE_GREEN ||
		nSkinColor == CHANNEL_SKIN_CAPRI_BLUE ||
		nSkinColor == CHANNEL_SKIN_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_DUSTY_GRAY ||
		nSkinColor == CHANNEL_SKIN_WHITE ||
		nSkinColor == CHANNEL_SKIN_CONGO_BROWN ||
		nSkinColor == CHANNEL_SKIN_MARS_VIOLET ||
		nSkinColor == CHANNEL_SKIN_OLD_COPPER ||
		nSkinColor == CHANNEL_SKIN_DARK_OLIVE ||
		nSkinColor == CHANNEL_SKIN_CAMEO_BROWN ||
		nSkinColor == CHANNEL_SKIN_FLINT ||
		nSkinColor == CHANNEL_SKIN_TERRA_VERTE ||
		nSkinColor == CHANNEL_SKIN_SLATE_OLIVE ||
		nSkinColor == CHANNEL_SKIN_SIAM_GREY ||
		nSkinColor == CHANNEL_SKIN_BLUISH_VIOLET ||
		nSkinColor == CHANNEL_SKIN_MID_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSKY_DRAB ||
		nSkinColor == CHANNEL_SKIN_GRAYISH_BROWN ||
		nSkinColor == CHANNEL_SKIN_VINACEOUS_SLATE ||
		nSkinColor == CHANNEL_SKIN_TOBACCO ||
		nSkinColor == CHANNEL_SKIN_VERMILLION ||
		nSkinColor == CHANNEL_SKIN_BURNT_UMBER ||
		nSkinColor == CHANNEL_SKIN_CEDAR_STAFF ||
		nSkinColor == CHANNEL_SKIN_SMOKED_PAPRIKA ||
		nSkinColor == CHANNEL_SKIN_TUSSOCK ||
		nSkinColor == CHANNEL_SKIN_LUXOR_GOLD ||
		nSkinColor == CHANNEL_SKIN_PESTO ||
		nSkinColor == CHANNEL_SKIN_BREEN ||
		nSkinColor == CHANNEL_SKIN_CONTESSA ||
		nSkinColor == CHANNEL_SKIN_CANDIED_APPLE ||
		nSkinColor == CHANNEL_SKIN_UMEMURASAKI_PURPLE ||
		nSkinColor == CHANNEL_SKIN_TAWNY_PORT ||
		nSkinColor == CHANNEL_SKIN_ANTIQUE_BRASS ||
		nSkinColor == CHANNEL_SKIN_SANDRIFT ||
		nSkinColor == CHANNEL_SKIN_BUFF_BROWN ||
		nSkinColor == CHANNEL_SKIN_SHINGLE_FAWN ||
		nSkinColor == CHANNEL_SKIN_PLUMBEOUS ||
		nSkinColor == CHANNEL_SKIN_DEEP_VARLEYS_GRAY ||
		nSkinColor == CHANNEL_SKIN_SLATE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_SLATE_BLACK ||
		nSkinColor == CHANNEL_SKIN_LIGHT_VIOLET_BLUE ||
		nSkinColor == CHANNEL_SKIN_ULTRAMARINE_ASH ||
		nSkinColor == CHANNEL_SKIN_KASHMIR_BLUE ||
		nSkinColor == CHANNEL_SKIN_HORTENSE_BLUE ||
		nSkinColor == CHANNEL_SKIN_CALAMINE_BLUE ||
		nSkinColor == CHANNEL_SKIN_BERYL_GREEN ||
		nSkinColor == CHANNEL_SKIN_SMALT_BLUE ||
		nSkinColor == CHANNEL_SKIN_DUSKY_GREEN_BLUE ||
		nSkinColor == CHANNEL_SKIN_ORCHID ||
		nSkinColor == CHANNEL_SKIN_PHLOX_PURPLE ||
		nSkinColor == CHANNEL_SKIN_VIOLET_PURPLE ||
		nSkinColor == CHANNEL_SKIN_LIGHT_RUSSET ||
		nSkinColor == CHANNEL_SKIN_MATHEWS_PURPLE ||
		nSkinColor == CHANNEL_SKIN_MAUVETTE ||
		nSkinColor == CHANNEL_SKIN_DARK_MADDER_BLUE ||
		nSkinColor == CHANNEL_SKIN_MATTE_WHITE ||
		nSkinColor == CHANNEL_SKIN_MATTE_BLACK ||
		nSkinColor == CHANNEL_SKIN_LIGHT_MINERAL_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSKY_BLUE_GREEN ||
		nSkinColor == CHANNEL_SKIN_PLUMMY ||
		nSkinColor == CHANNEL_SKIN_BLACK_ORCHID ||
		nSkinColor == CHANNEL_SKIN_DARK_CITRINE )
		{
        //:: Skin color is valid, return TRUE
			return TRUE;
		}

//:: Skin color is not within any valid range, return FALSE
	return FALSE;
}

//:: Function to check if oPC has valid Fire Genasi hair color channels
int HasValidFireGenasiHairColor(object oPC)
{
    //:: Get the hair color channel of the player character
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color is within any of the valid ranges
    if (nHairColor == CHANNEL_HAIR_AMBER_BROWN ||
		nHairColor == CHANNEL_HAIR_CHESTNUT ||
		nHairColor == CHANNEL_HAIR_MAROON ||
		nHairColor == CHANNEL_HAIR_CHOCOLATE ||
		nHairColor == CHANNEL_HAIR_HEATH_BROWN ||
		nHairColor == CHANNEL_HAIR_ARGUS_BROWN ||
		nHairColor == CHANNEL_HAIR_BRACKEN ||
		nHairColor == CHANNEL_HAIR_BROWN_BLACK ||
		nHairColor == CHANNEL_HAIR_SLATE ||
		nHairColor == CHANNEL_HAIR_DARK_SLATE_GRAY ||
		nHairColor == CHANNEL_HAIR_PLUM_PURPLE ||
		nHairColor == CHANNEL_HAIR_FORGET_ME_NOT_BLUE ||
		nHairColor == CHANNEL_HAIR_PASSIONATE_BLUE ||
		nHairColor == CHANNEL_HAIR_SILVER_CHALICE ||
		nHairColor == CHANNEL_HAIR_CHARCOAL ||
		nHairColor == CHANNEL_HAIR_DUSKY_SLATE_BLUE ||
		nHairColor == CHANNEL_HAIR_VARISCITE_GREEN ||
		nHairColor == CHANNEL_HAIR_DUSKY_DULL_GREEN ||
		nHairColor == CHANNEL_HAIR_COURT_GRAY ||
		nHairColor == CHANNEL_HAIR_DULL_BLACKISH_GREEN ||
		nHairColor == CHANNEL_HAIR_OLIVINE ||
		nHairColor == CHANNEL_HAIR_BROWNISH_GRAY ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_BRASS ||
		nHairColor == CHANNEL_HAIR_DOVE_GRAY ||
		nHairColor == CHANNEL_HAIR_DAVYS_GRAY ||
		nHairColor == CHANNEL_HAIR_GLOSS_BLACK ||
		nHairColor == CHANNEL_HAIR_INDIGO_BLUE ||
		nHairColor == CHANNEL_HAIR_TITANIUM_GRAY ||
		nHairColor == CHANNEL_HAIR_WARPLOCK_BRONZE ||
		nHairColor == CHANNEL_HAIR_MARS_BROWN ||
		nHairColor == CHANNEL_HAIR_DARK_PURPLE_DRAB ||
		nHairColor == CHANNEL_HAIR_DARK_RUSSIAN_GREEN ||
		nHairColor == CHANNEL_HAIR_DARK_NEUTRAL_GRAY ||
		nHairColor == CHANNEL_HAIR_INDIGO_BLUE ||
		nHairColor == CHANNEL_HAIR_TITANIUM_GRAY ||
		nHairColor == CHANNEL_HAIR_WARPLOCK_BRONZE ||
		nHairColor == CHANNEL_HAIR_CHAETURA_DRAB ||
		nHairColor == CHANNEL_HAIR_DUSK_BLUE ||
		nHairColor == CHANNEL_HAIR_GRAPHITE_GRAY ||
		nHairColor == CHANNEL_HAIR_FUSCOUS_GRAY ||
		nHairColor == CHANNEL_HAIR_BONE_BROWN ||
		nHairColor == CHANNEL_HAIR_ESPRESSO ||
		nHairColor == CHANNEL_HAIR_VANDYKE_BROWN ||
		nHairColor == CHANNEL_HAIR_LIGHT_PINK ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_PINK ||
		nHairColor == CHANNEL_HAIR_ACAJOU_RED ||
		nHairColor == CHANNEL_HAIR_VIOLET_CARMINE ||
		nHairColor == CHANNEL_HAIR_BEIGE_RED ||
		nHairColor == CHANNEL_HAIR_PECAN_BROWN ||
		nHairColor == CHANNEL_HAIR_POMPEIAN_RED ||
		nHairColor == CHANNEL_HAIR_SADDLE_BROWN ||
		nHairColor == CHANNEL_HAIR_FOLIAGE ||
		nHairColor == CHANNEL_HAIR_MOSS ||
		nHairColor == CHANNEL_HAIR_FOREST_GREEN ||
		nHairColor == CHANNEL_HAIR_LEAF_GREEN ||
		nHairColor == CHANNEL_HAIR_CORYDALIS_GREEN ||
		nHairColor == CHANNEL_HAIR_TEA_GREEN ||
		nHairColor == CHANNEL_HAIR_JADE_GREEN ||
		nHairColor == CHANNEL_HAIR_WOODLAND ||
		nHairColor == CHANNEL_HAIR_OLIVE_GRAY ||
		nHairColor == CHANNEL_HAIR_MOUSE_GRAY ||
		nHairColor == CHANNEL_HAIR_DARK_GRAYISH_BROWN ||
		nHairColor == CHANNEL_HAIR_ANILINE_BLACK ||
		nHairColor == CHANNEL_HAIR_TAN ||
		nHairColor == CHANNEL_HAIR_TAUPE ||
		nHairColor == CHANNEL_HAIR_NATAL_BROWN ||
		nHairColor == CHANNEL_HAIR_MAHOGANY_BROWN ||
		nHairColor == CHANNEL_HAIR_BEIGE ||
		nHairColor == CHANNEL_HAIR_VETIVER_GREEN ||
		nHairColor == CHANNEL_HAIR_GOLD_FUSION ||
		nHairColor == CHANNEL_HAIR_DARK_OLIVE ||
		nHairColor == CHANNEL_HAIR_LIGHT_SQUILL_BLUE ||
		nHairColor == CHANNEL_HAIR_COLUMBIA_BLUE ||
		nHairColor == CHANNEL_HAIR_GLAUCOUS_BLUE ||
		nHairColor == CHANNEL_HAIR_DARK_CINNABAR_GREEN ||
		nHairColor == CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN ||
		nHairColor == CHANNEL_HAIR_WINTER_GREEN ||
		nHairColor == CHANNEL_HAIR_KILDARE_GREEN ||
		nHairColor == CHANNEL_HAIR_DULL_CITRINE ||
		nHairColor == CHANNEL_HAIR_LIGHT_CRESS_GREEN ||
		nHairColor == CHANNEL_HAIR_AVELLANEOUS ||
		nHairColor == CHANNEL_HAIR_TAHINI_BROWN ||
		nHairColor == CHANNEL_HAIR_DEEP_CORINTHIAN_RED ||
		nHairColor == CHANNEL_HAIR_ARGYLE_PURPLE ||
		nHairColor == CHANNEL_HAIR_LIGHT_PERILLA_PURPLE ||
		nHairColor == CHANNEL_HAIR_PINKISH_VINACEOUS ||
		nHairColor == CHANNEL_HAIR_MATHEWS_PURPLE ||
		nHairColor == CHANNEL_HAIR_DARK_MADDER_BLUE ||
		nHairColor == CHANNEL_HAIR_MATTE_WHITE ||
		nHairColor == CHANNEL_HAIR_MATTE_BLACK ||
		nHairColor == CHANNEL_HAIR_PALE_GULL_GRAY ||
		nHairColor == CHANNEL_HAIR_NICKEL_GREEN ||
		nHairColor == CHANNEL_HAIR_MADDER_VIOLET ||
		nHairColor == CHANNEL_HAIR_DEEP_MADDER_BLUE ||
		nHairColor == CHANNEL_HAIR_DARK_CITRINE ||
		nHairColor == CHANNEL_HAIR_MAHOGANY ||
		nHairColor == CHANNEL_HAIR_LIGHT_BROWNISH_OLIVE)
		{
        //:: Hair color is valid, return TRUE
			return TRUE;
		}

//:: Hair color is not within any valid range, return FALSE
    return FALSE;
}

//:: Function to check if oPC has valid Water Genasi skin color channels
int HasValidWaterGenasiSkinColor(object oPC)
{
    //:: Get the skin color channel of the player character
    int nSkinColor = GetColor(oPC, COLOR_CHANNEL_SKIN);

    //:: Check if the skin color is valid.
    if( nSkinColor == CHANNEL_SKIN_COTTON_SEED ||
		nSkinColor == CHANNEL_SKIN_CLOUDY ||
		nSkinColor == CHANNEL_SKIN_TAPA ||
		nSkinColor == CHANNEL_SKIN_DOVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_HEATHER ||
		nSkinColor == CHANNEL_SKIN_BALI_HAI ||
		nSkinColor == CHANNEL_SKIN_SLATE_GRAY ||
		nSkinColor == CHANNEL_SKIN_DEEP_PAYNES_GRAY ||
		nSkinColor == CHANNEL_SKIN_SHUTTLE_GRAY ||
		nSkinColor == CHANNEL_SKIN_COAL_MINE ||
		nSkinColor == CHANNEL_SKIN_ABBEY ||
		nSkinColor == CHANNEL_SKIN_MAKO ||
		nSkinColor == CHANNEL_SKIN_THISTLE_GREEN ||
		nSkinColor == CHANNEL_SKIN_OLIVINE ||
		nSkinColor == CHANNEL_SKIN_VINTAGE ||
		nSkinColor == CHANNEL_SKIN_MEADOWLAND ||
		nSkinColor == CHANNEL_SKIN_SPRING_RAIN ||
		nSkinColor == CHANNEL_SKIN_BAY_LEAF ||
		nSkinColor == CHANNEL_SKIN_GLADE_GREEN ||
		nSkinColor == CHANNEL_SKIN_CACTUS ||
		nSkinColor == CHANNEL_SKIN_NOBEL_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSTY_GRAY ||
		nSkinColor == CHANNEL_SKIN_INDUSTRIAL ||
		nSkinColor == CHANNEL_SKIN_RHINE_CASTLE ||
		nSkinColor == CHANNEL_SKIN_ROSE_VALE ||
		nSkinColor == CHANNEL_SKIN_CARMINE ||
		nSkinColor == CHANNEL_SKIN_MAGENTA ||
		nSkinColor == CHANNEL_SKIN_ASTER_PURPLE ||
		nSkinColor == CHANNEL_SKIN_DULL_BLUE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_HELVETIA_BLUE ||
		nSkinColor == CHANNEL_SKIN_WHITE ||
		nSkinColor == CHANNEL_SKIN_CHALET_GREEN ||
		nSkinColor == CHANNEL_SKIN_DARK_GRAY_OLIVE ||
		nSkinColor == CHANNEL_SKIN_DARK_VIOLET ||
		nSkinColor == CHANNEL_SKIN_DARK_SLATE ||
		nSkinColor == CHANNEL_SKIN_DARK_PURPLE ||
		nSkinColor == CHANNEL_SKIN_OLD_LAVENDER ||
		nSkinColor == CHANNEL_SKIN_CAMEO_BROWN ||
		nSkinColor == CHANNEL_SKIN_FLINT ||
		nSkinColor == CHANNEL_SKIN_TERRA_VERTE ||
		nSkinColor == CHANNEL_SKIN_SLATE_OLIVE ||
		nSkinColor == CHANNEL_SKIN_BAYOUX_BLUE ||
		nSkinColor == CHANNEL_SKIN_VIOLET_SLATE ||
		nSkinColor == CHANNEL_SKIN_VERDIGRIS ||
		nSkinColor == CHANNEL_SKIN_SIAM_GREY ||
		nSkinColor == CHANNEL_SKIN_BLUISH_VIOLET ||
		nSkinColor == CHANNEL_SKIN_MID_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSKY_DRAB ||
		nSkinColor == CHANNEL_SKIN_GRAYISH_BROWN ||
		nSkinColor == CHANNEL_SKIN_VINACEOUS_SLATE ||
		nSkinColor == CHANNEL_SKIN_TOBACCO ||
		nSkinColor == CHANNEL_SKIN_VERMILLION ||
		nSkinColor == CHANNEL_SKIN_BURNT_UMBER ||
		nSkinColor == CHANNEL_SKIN_CEDAR_STAFF ||
		nSkinColor == CHANNEL_SKIN_SMOKED_PAPRIKA ||
		nSkinColor == CHANNEL_SKIN_TUSSOCK ||
		nSkinColor == CHANNEL_SKIN_LUXOR_GOLD ||
		nSkinColor == CHANNEL_SKIN_PESTO ||
		nSkinColor == CHANNEL_SKIN_BREEN ||
		nSkinColor == CHANNEL_SKIN_CONTESSA ||
		nSkinColor == CHANNEL_SKIN_CANDIED_APPLE ||
		nSkinColor == CHANNEL_SKIN_UMEMURASAKI_PURPLE ||
		nSkinColor == CHANNEL_SKIN_TAWNY_PORT ||
		nSkinColor == CHANNEL_SKIN_ANTIQUE_BRASS ||
		nSkinColor == CHANNEL_SKIN_SANTA_FE ||
		nSkinColor == CHANNEL_SKIN_SEPIA ||
		nSkinColor == CHANNEL_SKIN_METALLIC_COPPER ||
		nSkinColor == CHANNEL_SKIN_RAW_UMBER ||
		nSkinColor == CHANNEL_SKIN_OLIVE_GRAY ||
		nSkinColor == CHANNEL_SKIN_LIMED_ASH ||
		nSkinColor == CHANNEL_SKIN_FUSCOUS_GRAY ||
		nSkinColor == CHANNEL_SKIN_TUNGSTEN ||
		nSkinColor == CHANNEL_SKIN_SORRELL_BROWN ||
		nSkinColor == CHANNEL_SKIN_SANDALWOOD ||
		nSkinColor == CHANNEL_SKIN_NATAL_BROWN ||
		nSkinColor == CHANNEL_SKIN_MOROCCO_BROWN ||
		nSkinColor == CHANNEL_SKIN_INDIAN_KHAKI ||
		nSkinColor == CHANNEL_SKIN_ROSYBROWN ||
		nSkinColor == CHANNEL_SKIN_YELLOWISH_OLIVE ||
		nSkinColor == CHANNEL_SKIN_MUMMY_BROWN ||
		nSkinColor == CHANNEL_SKIN_PALE_MOUSE_GRAY ||
		nSkinColor == CHANNEL_SKIN_LIGHT_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_DEEP_QUAKER_DRAB ||
		nSkinColor == CHANNEL_SKIN_TAUPE_BROWN ||
		nSkinColor == CHANNEL_SKIN_VANILLA ||
		nSkinColor == CHANNEL_SKIN_SANDRIFT ||
		nSkinColor == CHANNEL_SKIN_BUFF_BROWN ||
		nSkinColor == CHANNEL_SKIN_SHINGLE_FAWN ||
		nSkinColor == CHANNEL_SKIN_PLUMBEOUS ||
		nSkinColor == CHANNEL_SKIN_DEEP_VARLEYS_GRAY ||
		nSkinColor == CHANNEL_SKIN_SLATE_VIOLET ||
		nSkinColor == CHANNEL_SKIN_SLATE_BLACK ||
		nSkinColor == CHANNEL_SKIN_LIGHT_VIOLET_BLUE ||
		nSkinColor == CHANNEL_SKIN_ULTRAMARINE_ASH ||
		nSkinColor == CHANNEL_SKIN_KASHMIR_BLUE ||
		nSkinColor == CHANNEL_SKIN_HORTENSE_BLUE ||
		nSkinColor == CHANNEL_SKIN_CALAMINE_BLUE ||
		nSkinColor == CHANNEL_SKIN_BERYL_GREEN ||
		nSkinColor == CHANNEL_SKIN_SMALT_BLUE ||
		nSkinColor == CHANNEL_SKIN_DUSKY_GREEN_BLUE ||
		nSkinColor == CHANNEL_SKIN_ORCHID ||
		nSkinColor == CHANNEL_SKIN_PHLOX_PURPLE ||
		nSkinColor == CHANNEL_SKIN_VIOLET_PURPLE ||
		nSkinColor == CHANNEL_SKIN_HYACINTH_VIOLET ||
		nSkinColor == CHANNEL_SKIN_LIGHT_NEROPALIN_BLUE ||
		nSkinColor == CHANNEL_SKIN_COLUMBIA_BLUE ||
		nSkinColor == CHANNEL_SKIN_GLAUCOUS_BLUE ||
		nSkinColor == CHANNEL_SKIN_DARK_CINNABAR_GREEN ||
		nSkinColor == CHANNEL_SKIN_LIGHT_ORIENTAL_GREEN ||
		nSkinColor == CHANNEL_SKIN_WINTER_GREEN ||
		nSkinColor == CHANNEL_SKIN_PALE_GREENISH_YELLOW ||
		nSkinColor == CHANNEL_SKIN_MATTE_WHITE ||
		nSkinColor == CHANNEL_SKIN_LIGHT_MINERAL_GRAY ||
		nSkinColor == CHANNEL_SKIN_DUSKY_BLUE_GREEN ||
		nSkinColor == CHANNEL_SKIN_PLUMMY  )
		{
        //:: Skin color is valid, return TRUE
			return TRUE;
		}

//:: Skin color is not within any valid range, return FALSE
	return FALSE;
}

//:: Function to check if oPC has valid Water Genasi hair color channels
int HasValidWaterGenasiHairColor(object oPC)
{
    //:: Get the hair color channel of the player character
    int nHairColor = GetColor(oPC, COLOR_CHANNEL_HAIR);

    //:: Check if the hair color is within any of the valid ranges
    if (nHairColor == CHANNEL_HAIR_BRANDY_ROSE ||
		nHairColor == CHANNEL_HAIR_KURUMIZOME_BROWN ||
		nHairColor == CHANNEL_HAIR_PRUSSIAN_RED ||
		nHairColor == CHANNEL_HAIR_TOAST ||
		nHairColor == CHANNEL_HAIR_RUSSET ||
		nHairColor == CHANNEL_HAIR_AKAROA ||
		nHairColor == CHANNEL_HAIR_ECRU ||
		nHairColor == CHANNEL_HAIR_CORKBOARD ||
		nHairColor == CHANNEL_HAIR_CLOUDY_CINNAMON ||
		nHairColor == CHANNEL_HAIR_CLAMSHELL ||
		nHairColor == CHANNEL_HAIR_OLD_ROSE ||
		nHairColor == CHANNEL_HAIR_CORAL_TREE ||
		nHairColor == CHANNEL_HAIR_ALAEA ||
		nHairColor == CHANNEL_HAIR_COTTON_SEED ||
		nHairColor == CHANNEL_HAIR_CLOUDY ||
		nHairColor == CHANNEL_HAIR_TAPA ||
		nHairColor == CHANNEL_HAIR_DOVE_GRAY ||
		nHairColor == CHANNEL_HAIR_HEATHER ||
		nHairColor == CHANNEL_HAIR_BALI_HAI ||
		nHairColor == CHANNEL_HAIR_SLATE_GRAY ||
		nHairColor == CHANNEL_HAIR_DEEP_PAYNES_GRAY ||
		nHairColor == CHANNEL_HAIR_TAHUNA_SANDS ||
		nHairColor == CHANNEL_HAIR_STRAW ||
		nHairColor == CHANNEL_HAIR_DONKEY_BROWN ||
		nHairColor == CHANNEL_HAIR_BURLAP ||
		nHairColor == CHANNEL_HAIR_SHUTTLE_GRAY ||
		nHairColor == CHANNEL_HAIR_COAL_MINE ||
		nHairColor == CHANNEL_HAIR_ABBEY ||
		nHairColor == CHANNEL_HAIR_MAKO ||
		nHairColor == CHANNEL_HAIR_THISTLE_GREEN ||
		nHairColor == CHANNEL_HAIR_OLIVINE ||
		nHairColor == CHANNEL_HAIR_VINTAGE ||
		nHairColor == CHANNEL_HAIR_MEADOWLAND ||
		nHairColor == CHANNEL_HAIR_SPRING_RAIN ||
		nHairColor == CHANNEL_HAIR_BAY_LEAF ||
		nHairColor == CHANNEL_HAIR_GLADE_GREEN ||
		nHairColor == CHANNEL_HAIR_CACTUS ||
		nHairColor == CHANNEL_HAIR_NOBEL_GRAY ||
		nHairColor == CHANNEL_HAIR_DUSTY_GRAY ||
		nHairColor == CHANNEL_HAIR_INDUSTRIAL ||
		nHairColor == CHANNEL_HAIR_RHINE_CASTLE ||
		nHairColor == CHANNEL_HAIR_ROSE_VALE ||
		nHairColor == CHANNEL_HAIR_CARMINE ||
		nHairColor == CHANNEL_HAIR_MAGENTA ||
		nHairColor == CHANNEL_HAIR_ASTER_PURPLE ||
		nHairColor == CHANNEL_HAIR_DULL_BLUE_VIOLET ||
		nHairColor == CHANNEL_HAIR_HELVETIA_BLUE ||
		nHairColor == CHANNEL_HAIR_TYROLITE_GREEN ||
		nHairColor == CHANNEL_HAIR_CAPRI_BLUE ||
		nHairColor == CHANNEL_HAIR_PEACOCK_GREEN ||
		nHairColor == CHANNEL_HAIR_COSSACK_GREEN ||
		nHairColor == CHANNEL_HAIR_CALLISTE_GREEN ||
		nHairColor == CHANNEL_HAIR_WARBLER_GREEN ||
		nHairColor == CHANNEL_HAIR_SILVER ||
		nHairColor == CHANNEL_HAIR_QUAKER_DRAB ||
		nHairColor == CHANNEL_HAIR_MASSICOT_YELLOW ||
		nHairColor == CHANNEL_HAIR_WAFER ||
		nHairColor == CHANNEL_HAIR_TUNDORA ||
		nHairColor == CHANNEL_HAIR_AZO_BLUE ||
		nHairColor == CHANNEL_HAIR_WHITE ||
		nHairColor == CHANNEL_HAIR_BLACK ||
		nHairColor == CHANNEL_HAIR_CONGO_BROWN ||
		nHairColor == CHANNEL_HAIR_MARS_VIOLET ||
		nHairColor == CHANNEL_HAIR_OLD_COPPER ||
		nHairColor == CHANNEL_HAIR_DARK_OLIVE ||
		nHairColor == CHANNEL_HAIR_CHALET_GREEN ||
		nHairColor == CHANNEL_HAIR_DARK_GRAY_OLIVE ||
		nHairColor == CHANNEL_HAIR_DARK_VIOLET ||
		nHairColor == CHANNEL_HAIR_DARK_SLATE ||
		nHairColor == CHANNEL_HAIR_DARK_PURPLE ||
		nHairColor == CHANNEL_HAIR_OLD_LAVENDER ||
		nHairColor == CHANNEL_HAIR_CAMEO_BROWN ||
		nHairColor == CHANNEL_HAIR_FLINT ||
		nHairColor == CHANNEL_HAIR_TERRA_VERTE ||
		nHairColor == CHANNEL_HAIR_SLATE_OLIVE ||
		nHairColor == CHANNEL_HAIR_BAYOUX_BLUE ||
		nHairColor == CHANNEL_HAIR_VIOLET_SLATE ||
		nHairColor == CHANNEL_HAIR_VERDIGRIS ||
		nHairColor == CHANNEL_HAIR_SIAM_GREY ||
		nHairColor == CHANNEL_HAIR_BLUISH_VIOLET ||
		nHairColor == CHANNEL_HAIR_MID_GRAY ||
		nHairColor == CHANNEL_HAIR_DUSKY_DRAB ||
		nHairColor == CHANNEL_HAIR_GRAYISH_BROWN ||
		nHairColor == CHANNEL_HAIR_VINACEOUS_SLATE ||
		nHairColor == CHANNEL_HAIR_TOBACCO ||
		nHairColor == CHANNEL_HAIR_VERMILLION ||
		nHairColor == CHANNEL_HAIR_BURNT_UMBER ||
		nHairColor == CHANNEL_HAIR_CEDAR_STAFF ||
		nHairColor == CHANNEL_HAIR_SMOKED_PAPRIKA ||
		nHairColor == CHANNEL_HAIR_TUSSOCK ||
		nHairColor == CHANNEL_HAIR_LUXOR_GOLD ||
		nHairColor == CHANNEL_HAIR_PESTO ||
		nHairColor == CHANNEL_HAIR_BREEN ||
		nHairColor == CHANNEL_HAIR_CONTESSA ||
		nHairColor == CHANNEL_HAIR_CANDIED_APPLE ||
		nHairColor == CHANNEL_HAIR_UMEMURASAKI_PURPLE ||
		nHairColor == CHANNEL_HAIR_TAWNY_PORT ||
		nHairColor == CHANNEL_HAIR_ANTIQUE_BRASS ||
		nHairColor == CHANNEL_HAIR_SANTA_FE ||
		nHairColor == CHANNEL_HAIR_SEPIA ||
		nHairColor == CHANNEL_HAIR_METALLIC_COPPER ||
		nHairColor == CHANNEL_HAIR_VERDANT_HAVEN ||
		nHairColor == CHANNEL_HAIR_WAKATAKE_GREEN ||
		nHairColor == CHANNEL_HAIR_PINE ||
		nHairColor == CHANNEL_HAIR_WOODLAND ||
		nHairColor == CHANNEL_HAIR_PLYMOUTH_GREEN ||
		nHairColor == CHANNEL_HAIR_VETIVER_GREEN ||
		nHairColor == CHANNEL_HAIR_JADE_GREEN ||
		nHairColor == CHANNEL_HAIR_RAW_UMBER ||
		nHairColor == CHANNEL_HAIR_OLIVE_GRAY ||
		nHairColor == CHANNEL_HAIR_LIMED_ASH ||
		nHairColor == CHANNEL_HAIR_FUSCOUS_GRAY ||
		nHairColor == CHANNEL_HAIR_TUNGSTEN ||
		nHairColor == CHANNEL_HAIR_SORRELL_BROWN ||
		nHairColor == CHANNEL_HAIR_SANDALWOOD ||
		nHairColor == CHANNEL_HAIR_NATAL_BROWN ||
		nHairColor == CHANNEL_HAIR_MOROCCO_BROWN ||
		nHairColor == CHANNEL_HAIR_INDIAN_KHAKI ||
		nHairColor == CHANNEL_HAIR_ROSYBROWN ||
		nHairColor == CHANNEL_HAIR_YELLOWISH_OLIVE ||
		nHairColor == CHANNEL_HAIR_MUMMY_BROWN ||
		nHairColor == CHANNEL_HAIR_PALE_MOUSE_GRAY ||
		nHairColor == CHANNEL_HAIR_LIGHT_QUAKER_DRAB ||
		nHairColor == CHANNEL_HAIR_DEEP_QUAKER_DRAB ||
		nHairColor == CHANNEL_HAIR_TAUPE_BROWN ||
		nHairColor == CHANNEL_HAIR_VANILLA ||
		nHairColor == CHANNEL_HAIR_SANDRIFT ||
		nHairColor == CHANNEL_HAIR_BUFF_BROWN ||
		nHairColor == CHANNEL_HAIR_SHINGLE_FAWN ||
		nHairColor == CHANNEL_HAIR_PLUMBEOUS ||
		nHairColor == CHANNEL_HAIR_DEEP_VARLEYS_GRAY ||
		nHairColor == CHANNEL_HAIR_SLATE_VIOLET ||
		nHairColor == CHANNEL_HAIR_SLATE_BLACK ||
		nHairColor == CHANNEL_HAIR_LIGHT_VIOLET_BLUE ||
		nHairColor == CHANNEL_HAIR_ULTRAMARINE_ASH ||
		nHairColor == CHANNEL_HAIR_KASHMIR_BLUE ||
		nHairColor == CHANNEL_HAIR_HORTENSE_BLUE ||
		nHairColor == CHANNEL_HAIR_CALAMINE_BLUE ||
		nHairColor == CHANNEL_HAIR_BERYL_GREEN ||
		nHairColor == CHANNEL_HAIR_SMALT_BLUE ||
		nHairColor == CHANNEL_HAIR_DUSKY_GREEN_BLUE ||
		nHairColor == CHANNEL_HAIR_ORCHID ||
		nHairColor == CHANNEL_HAIR_PHLOX_PURPLE ||
		nHairColor == CHANNEL_HAIR_VIOLET_PURPLE ||
		nHairColor == CHANNEL_HAIR_HYACINTH_VIOLET ||
		nHairColor == CHANNEL_HAIR_LIGHT_NEROPALIN_BLUE ||
		nHairColor == CHANNEL_HAIR_COLUMBIA_BLUE ||
		nHairColor == CHANNEL_HAIR_GLAUCOUS_BLUE ||
		nHairColor == CHANNEL_HAIR_DARK_CINNABAR_GREEN ||
		nHairColor == CHANNEL_HAIR_LIGHT_ORIENTAL_GREEN ||
		nHairColor == CHANNEL_HAIR_WINTER_GREEN ||
		nHairColor == CHANNEL_HAIR_PALE_GREENISH_YELLOW ||
		nHairColor == CHANNEL_HAIR_MATTE_WHITE ||
		nHairColor == CHANNEL_HAIR_LIGHT_MINERAL_GRAY ||
		nHairColor == CHANNEL_HAIR_DUSKY_BLUE_GREEN ||
		nHairColor == CHANNEL_HAIR_PLUMMY )
		{
        //:: Hair color is valid, return TRUE
			return TRUE;
		}

//:: Hair color is not within any valid range, return FALSE
    return FALSE;
}





	
void main()	
{
	object oPC 		= OBJECT_SELF;
	
	int iSkinColor	= GetColor(oPC, COLOR_CHANNEL_SKIN);	
	int iHairColor 	= GetColor(oPC, COLOR_CHANNEL_HAIR);
	int iEyeColor	= GetColor(oPC, COLOR_CHANNEL_TATTOO_1);
	
	if( GetRacialType(oPC) == 199 /* RACIAL_TYPE_AIR_GEN */ ) // Air Genasi
        {
            SetColor ( oPC, COLOR_CHANNEL_SKIN, 020);

            if ( GetItemPossessedBy(oPC, "HLSLANG_199") == OBJECT_INVALID )
            {
                DelayCommand(1.0, FloatingTextStringOnCreature("Auran language token acquired.", oPC));
                CreateItemOnObject("HLSLANG_199", oPC);
            }
        }
}		