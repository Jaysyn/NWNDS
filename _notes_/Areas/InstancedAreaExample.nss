void main()
{
//:: Declare major variables
    object oPC          = GetPCSpeaker();
    object oBaseArea    = GetObjectByTag("IFM_PC_ROOMS");

    string sPCName      = GetName(oPC);
    string sBaseResRef  = GetResRef(oBaseArea);
    string sBaseName    = GetName(oBaseArea);
    string sNewTag      = sPCName+sBaseResRef;
    string sNewName     = sPCName+"'s Room at the Flying Monkey";

//:: Create instanced area from player's information
    CreateArea(sBaseResRef, sNewTag, sNewName);

//:: Get information about new area instance
    object oNewRoom     = GetObjectByTag(sNewTag);
    string sNewResRef   = GetResRef(oNewRoom);


    location lEntry     = GetLocation(GetObjectByTag("PC_ROOM_MAT"));


//:: Create entry waypoint in new area instance
    CreateObject(OBJECT_TYPE_WAYPOINT, "nw_waypoint001", lEntry, FALSE, "WP_"+sNewTag);

//:: Make sure the area is valid
    object oTarget;
    location lTarget;
    oTarget = GetWaypointByTag("WP_"+sNewTag);

    lTarget = GetLocation(oTarget);

    if (GetAreaFromLocation(lTarget)==OBJECT_INVALID)
        {
            SendMessageToPC(oPC, "Warning: Area does not exist!");
            return;
        }
//:: Move PC to instanced area.
    AssignCommand(oPC, ClearAllActions());

    AssignCommand(oPC, ActionJumpToLocation(lTarget));

}