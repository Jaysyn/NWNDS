Hulking insect-men standing as tall as 7 feet at the shoulder, tohr-kreen & thri-kreen are the least "human" of the player character races. Their survivability in the wilderness, combined with their cunning and intellect, have made the mantis warriors (as they are known to some races) the undisputed masters across large tracts of the Athasian wastes.

T'keech are uncommon; their green chitin indicates they once lived in verdant areas. While adapted to deserts as well as other kreen, T'keech are immune to chitin-rot and respiratory infections that affect other kreen in humid regions. T'keech tend to be neutral in regards to law and chaos. Preferred professions include priest, druid, and warrior. Most, hoever serve as laborers in the nations of Jeral, T'keech and J'ez. Small clutches of T'keech are found in each nation. They are rarely nomadic but some clutches have small, independant settlements near oases. T'keech prefer to live in scrub plains and near oases.

T'keech Ability Adjustments:  +2 Str, +4 Dex, +2 Wis, -4 Cha, -2 Int.

Favored Class (Psion): A multi-class T'keech's psion class does not count when determining whether they suffer an XP penalty for multiclassing.

Special Abilities:
- T'keech base land speed is 40 feet.
- Monstrous Humanoid: T'keech are not subject to spells or effects that affect humanoids only, such as charm person or dominate person.
- Medium: As Medium creatures, T'keech have no special bonuses or penalties due to their size.
- Darkvision: T'keech can see in total darkness.
- Leap (Ex): T'keech are natural jumpers, gaining a +30 racial bonus to all Jump checks.
- Natural Armor:  T'keech have a +2 natural armor bonus to AC due to their naturally tough and resistant chitin.
- Natural Weapons:  T'keech have two claws attacks for 1d4 damage & a bite attack for 1d4 damage.
- Sleep Immunity:  T'keech do not sleep, and are mmune to sleep spells and similar effects. Natural fatigue does not effect T'keech.  T'keech spellcasters and manifesters still require 8 hours of rest before preparing spells.
- Water Efficient: T'keech only require half the water consumption of other races.
- Deflect Arrows:  T'keech gain the benefit of the Deflect Arrows feat
- Racial Weapon Familiarity:  To T'keech, the chatkcha and gythka are treated as martial rather than exotic weapons. (This is implemented as free weapon proficencies.)
- T'keech have a +4 racial bonus on Hide checks in sandy or arid areas.
- Racial Hit Dice: A T'keech begins with 2 levels of monstrous humanoid.

Automatic Languages: Kreen. 

Bonus Languages: Common, Dwarven, Elven, Jozhal, Scrab, Tari, Tohr-kreen.

Level Adjustment: +2